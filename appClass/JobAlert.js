import React, { Component } from 'react';
import { Image, TouchableOpacity, StyleSheet, ScrollView,ActivityIndicator, FlatList,Alert,BackHandler, Platform, UIManager, LayoutAnimation } from 'react-native';
// import { Collapse, CollapseHeader, CollapseBody } from "accordion-collapse-react-native";
import { List, ListItem, Separator, Container, Header, Content, View, Form, Item, Input, Badge, Picker, Label, Button, Text, Icon, Card, Spinner, CardItem, Thumbnail, Left, Body, Right, Row } from 'native-base';
const { baseUrl } = require('./baseurl/Url');
import AsyncStorage from '@react-native-community/async-storage';



// const JobtypeFunction = ({arr}) => {
  
//     console.log('job type function ',arr);
//         var arrString = arr.split(',');
//     return <View style={{flexDirection:'row',flex : 1}}>
//         {arrString.map((item)=> {
//             return <Text style={{fontSize : 12,backgroundColor:'#e71f1f', flexWrap:'wrap', color:'#fff',margin: 3,padding :5}}>{item}</Text>;
//         })}  
//       </View>;
// }



class JobAlert extends React.Component {
  constructor(props) {
    super(props);

    this.state = {

      Email: '',
      MobileNumber: '',
      PhoneNumber: '',
      sortbySelected: '',
      emailerr: false,
      MobileNumbererr: false,
      PhoneNumbererr: false,
      expanded: false,
      isRefresh : true,
     
      sortByList: [
        { name: 'Interested', id: "1" },
        { name: 'Not Interested', id: "2" },
        { name: 'Let me think about it', id: "3" },
      ],
      data: []

    };

    if (Platform.OS === 'android') {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }
    this.getJobAlert = this.getJobAlert.bind(this);

  }





  // static navigationOptions = {
    
  //   tabBarIcon: ({ tintColor }) => (      
  //     <Image source={require('../assest/job_alert.png')} style={{width : 25 ,height: 25,tintColor  : tintColor}} />
  //   )
  // };

  // static navigationOptions = {
  //   header: null
  // };




  changeLayout = () => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    this.setState({ expanded: !this.state.expanded });
  }




   componentDidMount() {
        this.getJobAlert("");
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }


   componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
   }

   componentWillReceiveProps(nextProps) {
    if (nextProps.navigation.state.params.token) {
      console.log('recieve props');
      this.getJobAlert("");
    }
  }
   
   handleBackButton = () => {
    Alert.alert(
      'Gradhire',
      'Exiting the application?', [{
        text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel'
      }, {
        text: 'OK',
        onPress: () => BackHandler.exitApp()
      },], {
        cancelable: false
      }
    )
     return true;
   } 


  getJobAlert = async(sortby) => {
    try {
      const value = await AsyncStorage.getItem('UserId');

      if (value !== null) {
        //   // We have data!!
        //   console.log('user id : ',value);

        fetch(baseUrl + "/mobile/api/job_alert", {
          method: 'POST',
          headers: new Headers({
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          }),
          body: JSON.stringify({
            user_id: value,
            sortby : sortby
          })
        })
          .then(response => {
            response.json().then(respond => {
              console.log('response ', respond);
              if (respond.status === "1") {
                this.setState({ data: respond.data,isRefresh : false })
              }else {
                this.setState({ data: [],isRefresh : false  });       
              }
              console.log('state data : ', this.state.data);
            })
          })
          .catch(error => {
            console.error(error);
          });
      }

    } catch (error) {
      console.log('error in AsyncStorage data: ', error);
    }
  }



  
  _onPressButton = (index) => {
    const { navigate } = this.props.navigation;
    navigate('Companyjobdetails', { paramid: index,getJobAlert : this.getJobAlert,sortbySelected : this.state.sortbySelected});
  }


  onChangeSortBy(text) {
        this.getJobAlert(text);
        this.setState({ sortbySelected : text  });
  }

  render() {

    if(this.state.isRefresh){
      return <View style={{flex : 1,justifyContent : 'center'}}><ActivityIndicator size="large" color="#0000ff" /></View>;
}else {


          return (
            <Container style={styles.container}>
            <Image source={require('../assest/login_bg.png')} style={styles.backgroundImage} />
            <Content style={styles.loginForm}>

                <Form>

                <View picker  style={{ flex: 1,flexDirection: 'row'}}>
                  <Item picker style={styles.labunder}>
                    <Picker
                      mode="dropdown"
                      iosIcon={<Icon name="arrow-down" />}
                      style={{ paddingLeft: 0}}
                      placeholder="Select Sort By"
                      placeholderStyle={{ color: "#64a8e3", fontSize: 18 }}
                      placeholderIconColor="#0275d8"
                      selectedValue={this.state.sortbySelected}
                      onValueChange={(text) => { this.onChangeSortBy(text); }}
                    >
                      <Picker.Item label="Job Sort By" value="SortBy" />
                      {this.state.sortByList.map((item, index) => {
                        return (<Picker.Item key={index} label={item.name} value={item.id} />)
                      })}
                    </Picker>
                  </Item>
              </View>
              




                  {this.state.data.length == 0 ? <View><Text> Data not available  </Text></View> :
                        this.state.data.map((item, index) => {
                          return (

                            <TouchableOpacity key={index} onPress={() => { this._onPressButton(item.id); }}>
                            <Card  style={{padding:10}}>
                              <View style={{flex: 1, flexDirection: 'row' }}>
                                <View style={{ paddingRight: 10, paddingleft: 10 }} >
                                  { (item.image_url)?
                                    <Image resizeMode='contain' source={{uri : item.image_url}} style={{ width: 100,height : 100}} />
                                    : 
                                    <View style={{ justifyContent:"center", alignItems:"center" , borderWidth:1 , borderRadius: 100/2, backgroundColor : "white", width:100, height:100}}>
                                      <Icon name="ios-briefcase" style={{  fontSize: 50, color: "#333"}} />
                                    </View>
                                    
                                  }
                                </View>

                                <View style={{ flex:1, flexDirection: 'column' }}>
                                    <View style={{ flexDirection: 'row'}}>
                                      <Text  style={{fontSize:16, color:'#333',paddingRight: 10,fontWeight:'bold' }}>{item.job_title}</Text>
                                        {/* <JobtypeFunction arr={item.job_type} /> */}
                                    </View>

                                  <View style={{flexDirection: "row",paddingRight : 10}}>
                                    <Image source={require('../assest/clock_icon.png')} style={{width : 10,height: 8,padding : 8,marginTop: 5}} />
                                    <Text  style={styles.spanType}>{item.job_type}</Text>
                                  </View>

                                  <View style={{flexDirection: "row",paddingRight : 10}}>
                                    <Thumbnail resizeMode="contain" small square style={{ height: 20, width: 20 }} source={require('../assest/job.png')} />
                                    <Text style={{color:'#1f6fe7', fontWeight:'bold',paddingLeft: 5}}>{item.company_name}</Text>
                                  </View>
                              </View>

                              </View>

                              <View style={{ flex: 1, flexDirection: 'row', marginTop:5 }}>
                                <Thumbnail resizeMode="contain" small square style={{ width: 20, height: 20, }} source={require('../assest/address.png')} /> 
                                <Text style={{ color:'#5c5c5c'}}>{item.country}, {item.postcode}</Text>
                              </View>

                            </Card>
                            </TouchableOpacity>
                          )
                        })
                }

                </Form>
              </Content>
            </Container>
          )

    }
  }
}






const styles = StyleSheet.create({
  spanType:{ flexDirection:'row', flexWrap:'wrap',padding :5, fontSize:13,marginBottom:5,marginRight : 10},
  cardBl:{ padding:10},
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover', // or 'stretch'
  },
  loginForm: {
    flexGrow:1,
    position: 'absolute',  

    backgroundColor: 'transparent', width: '96%', height: '100%',

  },
  container: {

    flex: 1,
    backgroundColor: 'transparent',
    alignItems: 'center', justifyContent: 'center',



  },
  text: {
    fontSize: 17,
    color: 'black',
    padding: 10
  },

  btnText: {
    textAlign: 'center',
    color: 'white',
    fontSize: 20
  },

  btnTextHolder: {
    borderWidth: 1,
    borderColor: 'rgba(0,0,0,0.5)'
  },

  Btn: {
    padding: 10,
    backgroundColor: 'rgba(0,0,0,0.5)'
  },
  labunder:{flex: 1,  flexDirection: 'row',
  borderBottomWidth: 1,
  borderBottomColor: '#017bc2', marginBottom:5,}
});



export default JobAlert;
