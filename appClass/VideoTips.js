import React, { PureComponent } from 'react';
import {
  Button,View,Text,BackHandler, TouchableOpacity,Image
} from 'react-native';
import Video from "react-native-video";


class VideoTips extends PureComponent {
    static navigationOptions = {
      title: 'Video',
    };


    constructor(props){
      super(props);
    }

    componentDidMount() {
      BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
     }
     
     componentWillUnmount() {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
     }
     
     handleBackButton = () => {
          this.props.navigation.goBack();
       return true;
     } 


    render() {
         var {videoItem, question } = this.props.navigation.state.params;
            console.log('video player : ',this.props);
      return ( <View style={{flex : 1}}>
             <Video controls={true}
                    style={{ height: 300 }}
                    ref={(ref) => { this.video = ref }}
                    paused = {false}
                    source={{ uri:  'http://protechgrad.protechgenie.in/upload/videointerview_wb.mp4' }}
                    ignoreSilentSwitch={"ignore"}
                    // source={LightVideo} 
                    />
             
             <View style={{justifyContent:"center", alignItems:"center", paddingTop:25}}>
                <TouchableOpacity onPress={() => {
                          this.props.navigation.navigate('VideoTips',{videoItem : { video_url : 'https://protechgrad.protechgenie.in/upload/videointerview_wb.mp4' }});
                        }}>
                    <Image source={require('../assest/videmg1.png')} style={{width:200, height: 120}} />
                </TouchableOpacity>
              </View>
              
      </View>
      );
    }
  }

  export default VideoTips;