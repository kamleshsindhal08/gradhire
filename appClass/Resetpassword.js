import React, { Component } from 'react';
import {FlatList,Linking, Image,TouchableOpacity,ProgressBarAndroid,StyleSheet } from 'react-native';
import { Container, Header,Badge, Content, Form, Item, Input, Label,Button,Text, Card,Spinner , CardItem, Thumbnail, Icon, Left, Body, Right, View} from 'native-base';
const {baseUrl} = require('./baseurl/Url');

class Resetpassword extends React.Component {
    static navigationOptions = {
      header : null
      };


    constructor(props){
      super(props);
      this.state = {
        Password : '',
        ConnPassword : '',
        passerr : false,
        Connpasserr : false,
        passErrorMsg : '',
        confirmPassMsg : ''
      }
    }

    componentDidMount() {
      Linking.getInitialURL().then((url) => {
        if (url) {
          console.log('Initial url is: ' + url);
        }
      }).catch(err => console.error('An error occurred', err));
    }


    submit() {

      console.log('submit function');
      if (this.state.Password == '') {
        this.setState({
        passerr : true,
        passErrorMsg: 'Invalid Password!'
        })
        return;
        }
      if (this.state.Password.length < 8) {
        this.setState({
          passerr : true,
          passErrorMsg: 'At least 8 charater require password'
        })
    return;
    }   
    this.setState({ passerr : false , passErrorMsg: ''
  });
    if (this.state.ConnPassword == '') {
      this.setState({
        Connpasserr : true,
        confirmPassMsg : 'Invalid Confirm Password!'
      })
        return;
      }
    if (this.state.ConnPassword == this.state.Password) {
            this.setState({
              Connpasserr : true,
              confirmPassMsg : 'At least 8 charater require password'
            })
        return;
          }
     this.setState({ Connpasserr : false,confirmPassMsg : '' }); 
  
  //   fetch(baseUrl+"/app/auth/login", {
  //        method: 'POST',
  //        headers: {
  //         Accept: 'application/json',
  //         'Content-Type': 'application/json',
  //       },
  //      body: JSON.stringify({
  //    identity: this.state.Username,
  //    password: this.state.Password
  //  }),
  //  })
  //  .then(response => {
  //    response.json().then(respond => {
  //      console.log(respond.message);
  //      const {navigate} = this.props.navigation;
  //      navigate('Home');
           
  //    //  return
  //    })
  // // console.log(res);
  // })
  // .catch(error => {
  //   console.error(error);
  // });
  
} 

   
    render() {
      return (

        <Container style={styles.container}>
        <Image source={require('../assest/login_bg.png')} style={styles.backgroundImage} />
        <Content style={ styles.loginForm }>
        <Form>
        <Image resizeMode="contain" style={styles.logo} source={require('../assest/logo.png')} />
          
        

        <Text style={{color: '#0275d8', textAlign: "center", paddingTop: 10,textTransform: 'uppercase'}}>Reset Password</Text>
        <Text style={{textAlign: "center", paddingTop: 10}}>To Continue,Please Enter Details</Text>


              <Item floatingLabel style={styles.input}>
              <Label style={{color : (this.state.passerr)? 'red':'#0275d8' }}>Password</Label>
              <Thumbnail resizeMode="contain" small square style={styles.mailicon} source={require('../assest/password_icon.png')} />
              <Input secureTextEntry={true} value={this.state.Password} onChangeText={(Password)=> this.setState({Password})}/>
              </Item>
              <Text style={{color : 'red',fontSize: 13 , marginLeft : 12}}>{this.state.passErrorMsg}</Text> 

            
              <Item floatingLabel style={styles.input}>
              <Label style={{color : (this.state.Connpasserr)? 'red':'#0275d8' }}>Confirm Password</Label>
              <Thumbnail resizeMode="contain" small square style={styles.mailicon} source={require('../assest/password_icon.png')} />
              <Input secureTextEntry={true} value={this.state.ConnPassword} onChangeText={(ConnPassword)=> this.setState({ConnPassword})}/>
              </Item>
              <Text style={{color : 'red',fontSize: 13 , marginLeft : 12}}>{this.state.confirmPassMsg}</Text> 

             <Button
              block
              style={{marginTop: 20,marginLeft:20,marginRight:20,borderRadius:5}} 
              onPress={()=> {console.log('hi');
              // let ty =
              this.submit(); }}>
              <Text>Submit</Text>
              </Button>
           
          </Form>
        </Content>
      </Container>  
      );
    }
}
const styles = StyleSheet.create({

  backgroundImage: {
    flex: 1,
    resizeMode: 'cover', // or 'stretch'
},
loginForm: {
  flex: 1,
    position: 'absolute',
   
    backgroundColor: 'transparent', paddingTop:40, width:'90%', height:null, 
},
  container: {
    
      flex: 1,
      backgroundColor: 'transparent',
      alignItems: 'center',justifyContent: 'center',
      
      
      
  },
  mailicon: {
    height: 10,
    width: 20,
  
}, 

  loginContainer:{
      alignItems: 'center',
      flexGrow: 1,
      justifyContent: 'center'
  },
  logo: {
    flex: 1,
    alignItems: 'center',
      width: 300,
      height: 100,
     justifyContent: 'center',marginLeft:50,
      
  },
  input:{
    fontSize: 16,
    
    color: 'black',
    marginBottom:10, paddingTop:20,
    borderBottomColor: '#1f6fe7',
    borderBottomWidth: 1
    
  }

})
export default Resetpassword;