import React, { Component } from 'react';
import { FlatList, Modal, ActivityIndicator, Image, Switch, Dimensions, TouchableOpacity, ProgressBarAndroid, ProgressViewIOS, Platform, StyleSheet, YellowBox, BackHandler, Alert, SafeAreaView, ScrollView } from 'react-native';
import { Picker, Container, Toast, Header, Badge, Content, Form, Item, Input, Label, Button, Text, Card, Spinner, CardItem, Thumbnail, Icon, Left, Body, Right, View, ProgressBar } from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';
import HTML from 'react-native-render-html';
import Video from 'react-native-video';
//import { Dialog } from 'react-native-simple-dialogs';
//import { ScrollView } from 'react-native-gesture-handler';
import CustomMenuIcon from './CustomMenuIcon';
const { baseUrl } = require('./baseurl/Url');


import Carousel, { Pagination } from 'react-native-snap-carousel';

const IS_ANDROID = Platform.OS === 'android';
const SLIDER_1_FIRST_ITEM = 1;
const IS_IOS = Platform.OS === 'ios';
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

function wp (percentage) {
  const value = (percentage * viewportWidth) / 100;
  return Math.round(value);
}

const slideHeight = viewportHeight * 0.36;
const slideWidth = 200;
const itemHorizontalMargin = 0; 

const sliderWidth = viewportWidth - 70;
const itemWidth = slideWidth + itemHorizontalMargin * 2;

const entryBorderRadius = 8;


const colors = {
  black: '#1a1917',
  gray: '#888888',
  background1: '#B721FF',
  background2: '#21D4FD'
};

class Candidate_profile1 extends Component {


  static navigationOptions =({ navigation })=> (
    
    {
    headerStyle: { backgroundColor: '#2e2b48' },
    headerTintColor: 'white',
     
    headerRight: (
    <CustomMenuIcon
      //Menu Text
      menutext="Menu"
      //Menu View Style
      menustyle={{
        marginRight: 16,
        flexDirection: 'row',
        justifyContent: 'flex-end',
      }}
      //Menu Text Style
      textStyle={{
        color: '#2e2b48',
      }}
      //Click functions for the menu items
      editProfile={() => {
         navigation.navigate('EditProfile', { getProfileData: this.getProfileData, position :[] });
      }}
      changePassword={() => {
        console.log('aaa');
        navigation.navigate('Changepassword', { getProfileData: this.getProfileData });
     }}
      option2Click={async() => {   
         console.log('logout method')
      try {
        await AsyncStorage.clear();
        console.log('cleard method')
        navigation.navigate('RootScreen');
        } catch (error) {
          console.log('error in clear AsyncStorage : ',error);
        } }
      }
      option3Click={() => {}}
      option4Click={() => { }}
    />),
    
  });


  logout = async()=>{
    console.log('logout method')
  try {
    await AsyncStorage.clear();
    console.log('cleard method');

    this.props.navigation.navigate('RootScreen');
    } catch (error) {
      console.log('error in clear AsyncStorage : ',error);
    }
}


  constructor(props) {
    super(props);
    this.state = {
      emailid: '',
      videoList: [],
      data: {},
      universityData: [],
      schoolData: [],
      workExpData: [],
      progressPercent: 0,
      progressBar: 0.0,

      contactDetail: {},
      videoModel: false,
      videoList: [],
      videoitem: {},
      statusToggle: false,
      togglebuttonVisible: false,
      lookingVisible : false,
      isRefresh : true,
      content: {},
      enableScrollViewScroll: true,
      slider1ActiveSlide: 1,
      position : [],
      // video controller




    }
    this.video = Video;
    this.getProfileData = this.getProfileData.bind(this);
    this.logout = this.logout.bind(this);

    this.position = this.state.position;
  }


  // async logout() {
  //   try {
  //     await AsyncStorage.clear();
  //     this.props.navigation.navigate('RootScreen');
  //   } catch (error) {
  //     console.log('error in clear AsyncStorage : ', error);
  //   }
  // }


  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  componentDidUpdate(prevProps) {
    console.log(this.props.isFocused);
    if(prevProps.isFocused !== this.props.isFocused) {
      console.log('back');
    }
}

componentWillReceiveProps(nextProps,prevState) {
    this.getProfileData();
  
}

  handleBackButton = () => {
    Alert.alert(
      'Gradhire',
      'Exiting the application?', [{
        text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel'
      }, {
        text: 'OK',
        onPress: () => BackHandler.exitApp()
      },], {
        cancelable: false
      }
    )
    return true;
  }



  componentDidMount() {
    //this.getProfileData();
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }

  componentWillMount() {
    this.getProfileData();
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }

  _renderItemWithParallax ({item, index}) {
    return (
  
      <Card style={{ marginTop: 10, padding: 10, color: "#0275d8" , paddingHorizontal: itemHorizontalMargin}}>
        <View style={{flex:1, justifyContent: 'center',alignItems: 'center',}}>
          <TouchableOpacity onPress={() => {
                this.props.navigation.navigate('VideoPlayer',{videoItem : item , question : true});
              }}>
            <Image source={require('../assest/vidscreen.png')} style={{ height: 120, width: 180 }} />
          </TouchableOpacity>

          <View style={{justifyContent: 'center',alignItems: 'center', position : "absolute", alignSelf : "center" }}>
              <TouchableOpacity onPress={() => {
                this.props.navigation.navigate('VideoPlayer',{videoItem : item, question : true});
              }}>
                <View style={{flexDirection : "row"}}> 
                  <Icon name="ios-play-circle"  style={{ fontSize:35 , color : '#ffffff'}} />
                </View>
              </TouchableOpacity>
          </View>

          
        </View>
        <Text style={{ flex: 1, flexWrap: 'wrap', fontSize: 10,padding:5 }}>{item.ques_name}</Text>
        <View style={{  alignSelf: 'center',height:35}}>
          <Button style={[styles.btntake]} onPress={() => { 
            // this.props.navigation.navigate('ComingSoon');
            this.props.navigation.navigate('VideoProfile', { item: item, getProfileData : this.getProfileData, });
          }} 
          >
            <Text  style={{ fontSize:12}}>Re-Take</Text>
          </Button>
          </View>
      </Card>
       
    );
  }


  async getProfileData() {
    try {
      const value = await AsyncStorage.getItem('UserId');
      const email = await AsyncStorage.getItem('Emailid');
      if (value !== null) {
        // We have data!!
        console.log('user id : ', value);

        fetch(baseUrl + "/mobile/api/get_candidate_profile_info", {
          method: 'POST',
          headers: new Headers({
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          }),
          body: JSON.stringify({
            user_id: value
          })
        })
          .then(response => {
            response.json().then(respond => {
              console.log('response ', respond);
              if (respond.status === "1") {
                console.log('load data');
                var progress, progressbarPercent, progressbar;
                var visibleToggle = false, togglestate = false,lookingVisible = false;
                 if(respond.data.complete_profile === ""){
                        progressbarPercent = 0;
                        progressbar = 0.0;
                 }else{
                  try {
                    progress = respond.data.complete_profile;
                    console.log('load data progress : ', progress);
                    progressbarPercent = parseInt(progress);
                    console.log('progress bar Percent : ', progressbarPercent);
                    progressbar = progressbarPercent / 100;
                    console.log('progress bar : ', progressbar);
                  } catch (err) {
                    console.log('error p', err);
                    progressPercent = 0;
                    progressbar = 0.0;
                  }
                 }
                 //togglestate = true;
                if (respond.data.status == "1") {
                  visibleToggle = true;
                  togglestate = true;
                  lookingVisible = true;
                } else if (respond.data.status == "5") {
                  visibleToggle = true;
                   lookingVisible = false;
                }
                console.log('123');

                var position = respond.data.current_position.split(",");
                console.log(respond.data);

                global.position = respond.data.current_position.split(",");
                global.position = (global.position.length > 0) ? global.position : [];
                this.setState({
                  data: respond.data, progressPercent: progressbarPercent, progressBar: progressbar, emailid: email,isRefresh : false, position : position,
                   togglebuttonVisible: visibleToggle, statusToggle: togglestate,lookingVisible : lookingVisible
                });
                // if(respond.data.name !== ""){
                this.defaultApi();
                // }
              }

              console.log('state data : ', this.state.data);

            })
          })
          .catch(error => {
            console.error(error);
          });

      }
    } catch (error) {
      console.error('error in AsyncStorage data: ', error);

    }
  }



  // universityDetail(){

  // }

  defaultApi() {
    this.getVideoList();
    this.getPersonalDetailData();
    this.getSchoolData();
    this.getWorkExpData();
    this.getUniversityData();
  }


  getVideoList=()=>{

    fetch(baseUrl + "/mobile/api/answer_video_detail", {
      method: 'POST',
      headers: new Headers({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      }),
      body: JSON.stringify({
        user_id: this.state.data.user_id
      })
    })
      .then(response => {
        response.json().then(respond => {
          console.log('response video list : ', respond);
          if (respond.status === "1") {
            this.setState({ isRefresh: false,videoList: respond.data });
          }else{
            this.setState({ isRefresh: false,videoList: [] });
          }
        })
      })
      .catch(error => {
        console.error(error);
      });
  }


  getUniversityData = () => {
    fetch(baseUrl + "/mobile/api/university_details", {
      method: 'POST',
      headers: new Headers({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      }),
      body: JSON.stringify({
        candidate_profile_id: this.state.data.candiateid
      })
    })
      .then(response => {
        response.json().then(respond => {
          console.log('response university : ', respond);
          if (respond.status === "1") {

            this.setState({ isRefresh: false,universityData: respond.data });
          }else{
            this.setState({ isRefresh: false,universityData: [] });
          }
        })
      })
      .catch(error => {
        console.error(error);
      });
  }

  getSchoolData = () => {
    fetch(baseUrl + "/mobile/api/school_details", {
      method: 'POST',
      headers: new Headers({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      }),
      body: JSON.stringify({
        candidate_profile_id: this.state.data.candiateid
      })
    })
      .then(response => {
        response.json().then(respond => {
          console.log('response school : ', respond);
          if (respond.status === "1") {
            this.setState({ schoolData: respond.data });
          } else{
            this.setState({ schoolData: [] });
          }
        })
      })
      .catch(error => {
        console.error(error);
      });
  }


  getWorkExpData = () => {
    fetch(baseUrl + "/mobile/api/work_experience_details", {
      method: 'POST',
      headers: new Headers({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      }),
      body: JSON.stringify({
        candidate_profile_id: this.state.data.candiateid
      })
    })
      .then(response => {
        response.json().then(respond => {
          console.log('response workexp : ', respond);
          if (respond.status === "1") {
            this.setState({ workExpData: respond.data });
          }else{
            this.setState({ workExpData: [] });
          }
        })
      })
      .catch(error => {
        console.error(error);
      });
  }


  getPersonalDetailData = () => {
    fetch(baseUrl + "/mobile/api/single_personal_contact_detail", {
      method: 'POST',
      headers: new Headers({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      }),
      body: JSON.stringify({
        user_id: this.state.data.user_id
      })
    })
      .then(response => {
        response.json().then(respond => {
          console.log('response personal details : ', respond);
          if (respond.status === "1") {
            this.setState({ contactDetail: respond.data });
          }
        })
      })
      .catch(error => {
        console.error(error);
      });
  }


  deleteWorkExp = (id) => {
    console.log('delete workexp : ', id);
    fetch(baseUrl + "/mobile/api/delete_work_exp_detail", {
      method: 'POST',
      headers: new Headers({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      }),
      body: JSON.stringify({
        work_id: id
      })
    })
      .then(response => {
        response.json().then(respond => {
          console.log('response school : ', respond);
          if (respond.status == "1") {
            Toast.show({
              text: respond.message,
              type: "success"
            });
            this.getWorkExpData();
          } else {
            Toast.show({
              text: respond.message,
              type: "danger"
            });
          }
        })
      })
      .catch(error => {
        console.error(error);
      });
  }


  deleteUniversity = (uniid) => {

    fetch(baseUrl + "/mobile/api/delete_uni_detail", {
      method: 'POST',
      headers: new Headers({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      }),
      body: JSON.stringify({
        uni_id: uniid
      })
    })
      .then(response => {
        response.json().then(respond => {
          console.log('response delete university', respond);
          if (respond.status == "1") {
            Toast.show({
              text: respond.message,
              type: "success"
            });
            this.getUniversityData();
          } else {
            Toast.show({
              text: respond.message,
              type: "danger"
            });
          }
        })
      })
      .catch(error => {
        console.error(error);
      });
  }


  schoolDelete = (id) => {
    console.log('school delete');
    fetch(baseUrl + "/mobile/api/delete_school_detail", {
      method: 'POST',
      headers: new Headers({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      }),
      body: JSON.stringify({
        school_id: id
      })
    })
      .then(response => {
        response.json().then(respond => {
          console.log('response delete school', respond);
          if (respond.status == "1") {
            Toast.show({
              text: respond.message,
              type: "success"
            });
            this.getSchoolData();
          } else {
            Toast.show({
              text: respond.message,
              type: "danger"
            });
          }
        })
      })
      .catch(error => {
        console.error(error);
      });


  }

  toggleStatusApi = (toggle) => {
    console.log('toggle status api toggle : ', toggle);
    this.setState({ statusToggle: toggle });
    var status = toggle ? "1" : "5";
    //var status = toggle ? "5" : "1";
    console.log('status : ', status);
    fetch(baseUrl + "/mobile/api/profile_activation", {
      method: 'POST',
      headers: new Headers({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      }),
      body: JSON.stringify({
        user_id: this.state.data.user_id,
        status: status
      })
    })
      .then(response => {
        response.json().then(respond => {

          console.log('response delete school', respond);
          if (respond.status == "1") {
            Toast.show({
              text: respond.message,
              type: "success"
            });
            this.getProfileData();
          } else {
            Toast.show({
              text: respond.message,
              type: "danger"
            });
            this.setState({ statusToggle: !toggle });
          }
        })
      })
      .catch(error => {
        console.error(error);
        this.setState({ statusToggle: !toggle });
      });
  }



  render() {


    // const {navigate} = this.props.navigation;
    YellowBox.ignoreWarnings(['Warning: Async Storage has been extracted from react-native core']);
    const { navigate } = this.props.navigation;

    //var job_category = "<div style='height:50; overflow: hidden '>"+this.state.data.job_category+"</div>";
    var job_category = this.state.data.job_category;
    //var names = 'Harry,John,Clark,Peter,Rohn,Alice';
    //console.log(typeof(this.state.data.job_category));
    if(job_category){
      job_category = job_category.split(',');
    }
    
    //console.log(job_category);
    if(this.state.isRefresh){
           return <View style={{flex : 1,justifyContent : 'center'}}><ActivityIndicator size="large" color="#0000ff" /></View>;
    }else {
      return (
        <Container style={styles.container} 
        onStartShouldSetResponderCapture={() => {
          this.setState({ enableScrollViewScroll: true });
        }}>
          <Image source={require('../assest/login_bg.png')} style={styles.backgroundImage} />
          <Content style={styles.loginForm} contentContainerStyle={this.state.content}
          scrollEnabled={this.state.enableScrollViewScroll}
          ref={myScroll => (this._myScroll = myScroll)}
          >
   
            <Form>
              <View style={{ paddingTop: 10, flexDirection: 'row' }} >
                <View style={{ paddingRight: 15, }} >
                 
                  <View style={{ borderRadius: 100/2, overflow: 'hidden'}}>
                  <Image source={{ uri: this.state.data.img_url}} style={{ height: 70, width: 70, }} />
                  </View>
                </View>
                <View style={{ flex: 1, flexDirection: 'column'}}>
                  <View style={{ flexDirection: 'row' ,marginBottom:5, marginRight: 5,  marginLeft: 2,  }}>
                    <Text style={{ color: '#000', fontSize:18, textAlign: "center", fontWeight:"bold" }}>{this.state.data.name} {this.state.data.surname}</Text>
                    {/* <Button style={styles.btnspan}><Text style={{color: '#fff'}}> Full Time</Text>
                  </Button> */}
                  </View> 
                  { /*
                  <View
                    style={{flexDirection:'row', flex: 1, margin:5, flexWrap: "wrap" }}
                    onStartShouldSetResponderCapture={() => {
                      this.setState({ enableScrollViewScroll: false });
                      if (this._myScroll.contentOffset === 0
                        && this.state.enableScrollViewScroll === false) {
                        this.setState({ enableScrollViewScroll: true });
                      }
                      }}>
  
                      <FlatList
                        maxHeight={100}
                        style={{ flexGrow: 1}}
                        //umColumns = {1}
                        horizontal={false}
                        data={job_category} 
                        keyExtractor={item => item}
                        renderItem={({ item }, index) =><Text style={{fontSize:14}} key={index+1}>{item},</Text>
                        }
                      />
                     
                  </View>
                      */ }
                 
                  <View style={{flexDirection: 'row',flex : 1}}>
                    <View style={{flex : 2,justifyContent : 'center'}}>
                      <View style={{borderRadius: 5,  marginTop:5, paddingTop: 2, paddingBottom: 2, height: 24,backgroundColor : this.state.lookingVisible? '#43b05c' : '#5bc0de'}}>
                        <Text style={{fontSize:14, fontWeight : "bold" , color:"#fff", textAlign:"center"}}>{this.state.data.status_name}</Text>
                      </View>
                    </View>
  
                   <View style={{flex : 1, alignItems: 'flex-end', marginRight: 10}}>
                   {this.state.togglebuttonVisible? 
                      <Switch style={{width : 50, height: 30, }}
                      onValueChange={(s) => { this.toggleStatusApi(s); }}
                      value={this.state.statusToggle} />  
                   : null}
                  </View>
  
                  </View>
                 
                </View>
              </View>
  
              <View style={{ flex: 1, flexDirection: 'row', justifyContent:"flex-end" }}>
                <Text style={{ color: '#0275d8', paddingTop: 12 }}>{this.state.progressPercent}% Complete</Text>
              </View>
             
              {/* ProgresBar  */}
  
              {(Platform.OS == 'android') ? <ProgressBarAndroid
                styleAttr="Horizontal"
                indeterminate={false}
                progress={this.state.progressBar}
              /> : <ProgressViewIOS styleAttr="Horizontal"
                indeterminate={false}
                progress={this.state.progressBar} />}
  
              <View style={{ flex: 1, flexDirection: 'row' }}>
  
                <View flex={1}>
                { (this.state.data.phone_number)?
                  <View style={{ paddingTop: 5, flexDirection: 'row',}}>
                    <Thumbnail resizeMode="contain" small square style={{ width: 20, height: 20, margin: 5 }}source={require('../assest/phone.png')} />
                    <Text style={{ paddingTop: 5, fontSize:14, paddingLeft: 5 }}>{this.state.data.phone_number}</Text>
                    <Thumbnail resizeMode="contain" style={{ width: 20, height: 20, margin: 5, marginRight : 23,  justifyContent: 'flex-end',marginLeft: 'auto', }} small square
                      source={(this.state.data.phone_number == "") ? require('../assest/delete.png') : require('../assest/check1.png')} />
                  </View>
                  : null
                }
                 { (this.state.data.state_name)?
                  <View style={{ paddingTop: 5, flexDirection: 'row' }}>
                    <Thumbnail resizeMode="contain" small square style={{ width: 20, height: 20, margin: 5 }} source={require('../assest/address.png')} />
                    <Text style={{ paddingTop: 5, flexWrap: 'wrap' , fontSize:14, paddingLeft: 5, width : "80%" }}>{this.state.data.state_name} </Text>
                    <Thumbnail resizeMode="contain" style={{width: 20, height: 20, margin: 5, marginRight : 23, justifyContent: 'flex-end',marginLeft: 'auto',}} small square
                      source={(this.state.data.state_name == "") ? require('../assest/delete.png') : require('../assest/check1.png')} />
                  </View>
                  : null
                 }
                  <View style={{ paddingTop: 5, flexDirection: 'row' }}>
                    <Thumbnail resizeMode="contain" small square style={{ width: 20, height: 20, margin: 5 }} source={require('../assest/profile_mail.png')} />
                    <Text style={{paddingTop: 5, fontSize:14, paddingLeft: 5 }}>{this.state.emailid}</Text>
                    <Thumbnail resizeMode="contain"style={{ width: 20, height: 20, margin: 5, marginRight : 23, justifyContent: 'flex-end',marginLeft: 'auto', }} small square
                      source={(this.state.data.emailid == "") ? require('../assest/delete.png') : require('../assest/check1.png')} />
                  </View>
                  <View style={{ paddingTop: 5, flexDirection: 'row' }}>
                    <Thumbnail resizeMode="contain" small square style={{ width: 20, height: 20, margin: 5 }} source={require('../assest/video_icon.png')} />
  
                    <Text style={{ paddingTop: 5, fontSize:14, paddingLeft: 5}}>{this.state.data.total_answer_video} of {this.state.data.total_question} Videos Complete</Text>
                    <Thumbnail resizeMode="contain" style={{ width: 20, height: 20, margin: 5, marginRight : 23, justifyContent: 'flex-end',marginLeft: 'auto', }} small square
                      source={(this.state.data.total_question != this.state.data.total_answer_video) ? require('../assest/delete.png') : require('../assest/check1.png')} />
                  </View>
                  
  
  
  
                </View>
              </View>
              <View style={{ paddingTop: 10, flexDirection: 'row' }}>
              
                <Card style={styles.carditn}>
                  <View style={{ paddingTop: 5, paddingBottom:5, flexDirection: 'row' }}>
                    <Text style={[styles.texth1, {flex:1}]}>VIDEO</Text>
                    <View style={{flex:2, flexDirection: 'row'}}>
                          <View style={{ flex: 2, flexDirection: 'row', alignItems: 'center',height: 20}}>

                            <TouchableOpacity style={{  flexDirection: 'row', marginLeft: 'auto', justifyContent: 'flex-end'}} onPress={() => {  this.props.navigation.navigate('VideoProfile',{ getProfileData : this.getProfileData, action : 'retake' }); }}>
                              <Thumbnail style={{ width: 20, height: 20 , marginRight: 5 }} resizeMode="contain" small square
                              source={require('../assest/add.png')} /> 
                              <Text style={styles.texth1} >RETAKE</Text>
                            </TouchableOpacity>

                            { (this.state.data.total_answer_video != 5 ) ?
                            <TouchableOpacity style={{  flexDirection: 'row', marginLeft: 'auto', justifyContent: 'flex-end',height: 20 }} onPress={() => {  this.props.navigation.navigate('VideoProfileCreate',{ getProfileData : this.getProfileData , action : 'create' }); }}>
                              <Thumbnail style={{ width: 20, height: 20,  marginRight: 5  }} resizeMode="contain" small square
                              source={require('../assest/add.png')} /> 
                              <Text style={styles.texth1} >CREATE</Text>
                            </TouchableOpacity>
                             : null }

                          </View>

                      </View>
                  </View>
                  {/* FlatLIst Video List */}
  
                  <View style={styles.exampleContainer}>
                
                  <Carousel
                    ref={c => this._slider1Ref = c}
                    data={this.state.videoList}
                    renderItem={this._renderItemWithParallax.bind(this)}
                    sliderWidth={sliderWidth}
                    itemWidth={itemWidth}
                    //hasParallaxImages={true}
                    //firstItem={SLIDER_1_FIRST_ITEM}
                    //inactiveSlideScale={0.94}
                    //inactiveSlideOpacity={0.7}
                    // inactiveSlideShift={20}
                    //containerCustomStyle={styles.slider}
                    //contentContainerCustomStyle={styles.sliderContentContainer}
                    loop={true}
                    //loopClonesPerSide={2}
                    //autoplay={false}
                    //autoplayDelay={500}
                    //autoplayInterval={3000}
                    layoutCardOffset={15}
                    onSnapToItem={(index) => this.setState({ slider1ActiveSlide: index }) }
                  />
                  <Pagination
                    dotsLength={this.state.videoList.length}
                    activeDotIndex={this.state.slider1ActiveSlide}
                    containerStyle={styles.paginationContainer}
                    dotColor={'blue'}
                    dotStyle={styles.paginationDot}
                    inactiveDotColor={'#000'}
                    inactiveDotOpacity={0.8}
                    inactiveDotScale={0.7}
                    carouselRef={this._slider1Ref}
                    tappableDots={!!this._slider1Ref}
                  />
              </View>

              {/*
              
                  <FlatList
                    horizontal
                    data={this.state.videoList}
                    style = {{marginBottom:10}}
                    keyExtractor={item => item.ques_name}
                    renderItem={({ item }, index) =>
                      <Card style={{ marginTop: 20, width: 160, padding: 10, color: "#0275d8" }}>
                        <View style={{flex:1, justifyContent: 'center',alignItems: 'center',}}>
                          <TouchableOpacity onPress={() => {
                                this.props.navigation.navigate('VideoPlayer',{videoItem : item , question : true});
                              }}>
                            <Image source={require('../assest/vidscreen.png')} style={{ height: 100, width: 140 }} />
                          </TouchableOpacity>
  
                          <View style={{justifyContent: 'center',alignItems: 'center', position : "absolute", alignSelf : "center" }}>
                              <TouchableOpacity onPress={() => {
                                this.props.navigation.navigate('VideoPlayer',{videoItem : item, question : true});
                              }}>
                                <View style={{flexDirection : "row"}}> 
                                  <Icon name="ios-play-circle"  style={{ fontSize:35 , color : '#ffffff'}} />
                                </View>
                              </TouchableOpacity>
                          </View>
                        </View>
                        <Text style={{ flex: 1, flexWrap: 'wrap', fontSize: 10,padding:5 }}>{item.ques_name}</Text>
                        <Button style={styles.btntake} onPress={() => { 
                         // this.props.navigation.navigate('ComingSoon');
                          this.props.navigation.navigate('VideoProfile', { item: item, getProfileData : this.getProfileData, });
                        }} 
                        >
                          <Text  style={{ fontSize:12}}>Re-Take</Text>
                          </Button>
                      </Card>
                    }
                  />
                  */ }

                </Card>
              </View>
  
            <View style={{ paddingTop: 10, flexDirection: 'row' }}>
                <Card style={styles.carditn}>
                  <View style={{ paddingTop: 10,marginBottom : 10, flexDirection: 'row' }}>
                    <Text style={styles.texth1}>KEY ACHIEVEMENTS</Text>
                  </View>
                  <View style={{marginBottom: 10}}>
                    {this.state.data.description === ""? null : <HTML html={this.state.data.description}/>}
                  </View>
                 
                </Card>
              </View>
  
  
              {/* Education Card  */}
  
              <View>
                <Card style={styles.carditn}>
                  <View style={{ paddingTop: 5, paddingBottom: 5, flexDirection: 'row' }}>
                    <Text style={styles.texth1}>EDUCATION</Text>
                  </View>
  
                <View style={{marginBottom: 10}}>
                  <View style={{ flex: 1, backgroundColor: 'red', flexDirection: 'row', backgroundColor: '#edf4fe', padding: 3, marginTop: 5 }}>
                    <Text style={{ justifyContent: 'flex-start', color: '#333333', alignItems: 'center', padding: 5 }}>UNIVERSITY</Text>
                    <TouchableOpacity style={{ marginRight: 10, marginLeft: 'auto' , marginBottom: 5 , justifyContent: 'flex-end' }} onPress={() => { navigate('AddUniversity', { candidate_id: this.state.data.candiateid, getUniversityData: this.getProfileData }); }}>
                      <Thumbnail style={{ width: 20, height: 20 }} resizeMode="contain" small square source={require('../assest/add.png')} />
                    </TouchableOpacity>
  
  
                  </View>
  
  
  
                  {this.state.universityData.map((item, index) => {
                    return (
                      <Card key={index}> 
                        <View style={{padding:7, width:"100%"}}>
                          <Body>
                            <View style={{flexDirection: 'row', }}>
  
                              <View style={{ flex: 3 }}>
                                <Text style={styles.texth2}>{item.university_name}</Text>
                                <Text>{item.uni_from_year} - {item.uni_to_year}</Text>
                                <Text>{item.courst_type}-{item.course_name}</Text>
                                <Text>Grade : {item.grade} </Text>
                              </View>
                              <View style={{ flex: 1 }}>
  
  
                                <TouchableOpacity style={{ marginRight: 5, marginLeft: 'auto', justifyContent: 'flex-end', marginTop: 5 }} onPress={() => { navigate('EditUniversity', { uni_id: item.uni_id, getUniversityData: this.getProfileData }); }}>
                                  <Thumbnail style={{ width: 20, height: 20 }} resizeMode="contain" small square
                                    source={require('../assest/edit_icon.png')} />
                                </TouchableOpacity>
  
  
                                <TouchableOpacity style={{ marginRight: 5, marginLeft: 'auto', justifyContent: 'flex-end', marginTop: 15 }} onPress={() => {
                                  Alert.alert('University', 'Are you sure delete this university',
                                    [{
                                      text: 'Cancel',
                                      onPress: () => { console.log('Cancel Pressed'); },
                                      style: 'cancel'
                                    },
                                    { text: 'OK', onPress: () => { console.log('OK Pressed'); this.deleteUniversity(item.uni_id); } },
                                    ],
                                  );
                                }}>
                                  <Thumbnail style={{ width: 20, height: 20 }} resizeMode="contain" small square
                                    source={require('../assest/delete_img.png')} />
                                </TouchableOpacity>
  
  
                              </View>
  
                            </View>
  
                          </Body>
                        </View>
                      </Card>);
                  })}
  
                  <View style={{ flex: 1, backgroundColor: 'red', flexDirection: 'row', backgroundColor: '#edf4fe', padding: 3, marginTop: 5 }}>
  
                    <Text style={{ justifyContent: 'flex-start', color: '#333333', alignItems: 'center', padding: 5 }}>SCHOOL / COLLEGE</Text>
                    <TouchableOpacity style={{ marginRight: 10, marginBottom:5, marginLeft: 'auto', justifyContent: 'flex-end' }} onPress={() => { navigate('AddSchool', { candidate_id: this.state.data.candiateid, getSchoolData: this.getProfileData }); }}>
                      <Thumbnail style={{ width: 20, height: 20 }} resizeMode="contain" small square
                        source={require('../assest/add.png')} />
                    </TouchableOpacity>
                  </View>
  
  
                  {/* School List  */}
  
                  {this.state.schoolData.map((item, index) => {
                    return (
                      <Card key={index}>
                        <View style={{padding:7, width:"100%"}}>
                          <Body>
  
                            <View style={{ flexDirection: 'row', flex: 1 }}>
                              <View style={{ flex: 3 }}>
                                <Text style={styles.texth2}>{item.school_name}</Text>
                                <Text>{item.school_from_year} - {item.school_to_year}</Text>
                                <Text>{item.exam_type}</Text>
                                <Text>{item.result}</Text>
                              </View>
                              <View style={{ flex: 1 }}>
  
                                <TouchableOpacity style={{ marginRight: 5, marginLeft: 'auto', justifyContent: 'flex-end', marginTop: 5 }} onPress={() => { navigate('EditSchool', { id: item.school_id, getSchoolData: this.getProfileData }); }}>
                                  <Thumbnail style={{ width: 20, height: 20 }} resizeMode="contain" small square
                                    source={require('../assest/edit_icon.png')} />
                                </TouchableOpacity>
  
  
                                <TouchableOpacity style={{ marginRight: 5, marginLeft: 'auto', justifyContent: 'flex-end', marginTop: 15 }} onPress={() => {
                                  Alert.alert('Gradhire', 'Are you sure delete this school',
                                    [{
                                      text: 'Cancel',
                                      onPress: () => { console.log('Cancel Pressed'); },
                                      style: 'cancel'
                                    },
                                    { text: 'OK', onPress: () => { console.log('OK Pressed'); this.schoolDelete(item.school_id); } },
                                    ],
                                  );
                                }}>
                                  <Thumbnail style={{ width: 20, height: 20 }} resizeMode="contain" small square
                                    source={require('../assest/delete_img.png')} />
                                </TouchableOpacity>
  
  
                              </View>
                            </View>
  
                          </Body>
                        </View>
                      </Card>);
  
                  })}
  
  
                </View>
                </Card>
              </View>
  
  
  
  
  
              <View>
                <Card style={styles.carditn}>
                  <View style={{ paddingTop: 5, paddingBottom:5, flexDirection: 'row' }}>
                    <Text style={styles.texth1}>WORK EXPERIENCE</Text>
                    <TouchableOpacity style={{ marginRight: 13, marginLeft: 'auto', justifyContent: 'flex-end' }} onPress={() => { navigate('AddWorkExp', { candidate_id: this.state.data.candiateid, getWorkExpData: this.getProfileData }); }}>
                      <Thumbnail resizeMode="contain" small square style={{ width: 20, height: 20 }}  source={require('../assest/add.png')} />
                    </TouchableOpacity>
                  </View>
                  <View style={{marginBottom: 10}}>
  
                    {this.state.workExpData.map((item, index) => {
                      return (
                        <Card key={index}>
                          <View style={{padding:7, width:"100%"}}>
                            <Body>
                              <View style={{ flexDirection: 'row', flex: 1 }}>
                                <View style={{ flex: 3 }}>
                                  <Text>{item.title}</Text>
                                  <Text>{item.company}</Text>
                                  <Text>{item.from_month} {item.from_year} - {(item.currently_work == "on") ? "Present" : "" + item.to_month + " " + item.to_year}</Text>
                                </View>
                                <View style={{ flex: 1 }}>
  
                                  <TouchableOpacity style={{ marginRight: 5, marginLeft: 'auto', justifyContent: 'flex-end', marginTop: 5 }} onPress={() => { navigate('EditWorkExp', { id: item.id, getWorkExpData: this.getProfileData }); }}>
                                    <Thumbnail style={{ width: 20, height: 20 }} resizeMode="contain" small square
                                      source={require('../assest/edit_icon.png')} />
                                  </TouchableOpacity>
  
  
                                  <TouchableOpacity style={{ marginRight: 5, marginLeft: 'auto', justifyContent: 'flex-end', marginTop: 15 }} onPress={() => {
                                    Alert.alert('Gradhire', 'Are you sure delete this work experience',
                                      [{
                                        text: 'Cancel',
                                        onPress: () => { console.log('Cancel Pressed'); },
                                        style: 'cancel'
                                      },
                                      { text: 'OK', onPress: () => { console.log('OK Pressed'); this.deleteWorkExp(item.id); } },
                                      ],
                                    );
                                  }}>
                                    <Thumbnail style={{ width: 20, height: 20 }} resizeMode="contain" small square
                                      source={require('../assest/delete_img.png')} />
                                  </TouchableOpacity>
  
  
                                </View>
                              </View>
  
                            </Body>
                          </View>
                        </Card>);
  
                    })}
                </View>
                </Card>
              </View>
  
  
              <View>
                <Card style={styles.carditn}>
                  <View style={{ flexDirection: 'row', flex: 1 }}>
                    <View style={{ flex: 6 }}>
                      <Text style={[styles.texth1, {paddingTop : 5 ,paddingBottom:5}]} >
                      PERSONAL CONTACT DETAILS</Text>
                      <View style={{ padding: 5}}>
                      {this.state.contactDetail.email && <Text>{this.state.contactDetail.email}</Text>} 
                      {(this.state.contactDetail.mobile_number) ? <Text>{this.state.contactDetail.mobile_number}</Text> : null }
                      {(this.state.contactDetail.phone_number) ? <Text>{this.state.contactDetail.phone_number}</Text> : null }
                      </View>
                    </View>
                    <View style={{ flex: 1 }}>
                      <TouchableOpacity style={{ marginRight: 10, marginLeft: 'auto', justifyContent: 'flex-end', marginTop: 5 }} onPress={() => { 
                        // navigate('ComingSoon');
                        navigate('AddContact', { id: this.state.data.user_id, getPersonalDetailData: this.getProfileData });
                         }}>
                        <Thumbnail style={{ width: 20, height: 20 }} resizeMode="contain" small square
                          source={require('../assest/edit_icon.png')} />
                      </TouchableOpacity>
                    </View>
                  </View>
                </Card>
              </View>
  
              {/* <Button onPress={() => { this.props.navigation.navigate('JobAlert') }}
                block
                style={{ marginTop: 20, marginLeft: 20, marginRight: 20, borderRadius: 5 }}
              >
                <Text>Submit</Text>
              </Button> */}
  
  
              {/* <Button onPress={() => { this.logout(); }}
                block
                style={{ marginTop: 10, marginLeft: 20, marginRight: 20, borderRadius: 5 }}
              >
                <Text>Logout</Text>
              </Button> */}
  
  
              {/* <Button block
                style={{ marginTop: 10, marginLeft: 20, marginRight: 20, borderRadius: 5 }}
                onPress={() => { navigate('Changepassword'); }} >
                <Text>Change Password</Text>
              </Button> */}
  
  
            </Form>
            </Content>

        </Container>
  
      );
     }


  }
}
const styles = StyleSheet.create({
  btnspan: { backgroundColor: '#1f6fe7', borderRadius: 10,fontSize:14, paddingTop: 2, paddingBottom: 2, height: 24, },
  btngreen: { borderRadius: 5,fontSize:14, marginTop:5, paddingTop: 2, paddingBottom: 2, height: 24, },
  btntake: {  backgroundColor: '#1f6fe7', fontSize:12,textAlign: 'center', borderRadius: 5,height: 30, flex:1 },
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover', // or 'stretch'
  },
  loginForm: {
    flex: 1,
    position: 'absolute',

    backgroundColor: 'transparent', width: '94%', height: '100%',
  },
  container: {

    flex: 1,
    backgroundColor: 'transparent',
    alignItems: 'center', justifyContent: 'center'
  },
  headerText: {
    fontSize: 20,
    margin: 10,
    fontWeight: "bold"
  },
  menuContent: {
    color: "#000",
    fontWeight: "bold",
    padding: 2,
    fontSize: 20
  },


  loginContainer: {
    alignItems: 'center',
    flexGrow: 1,
    justifyContent: 'center'
  },
  logo: {
    flex: 1,
    alignItems: 'center',
    width: '100%',
    justifyContent: 'center',

  },
  input: {
    fontSize: 16,

    color: 'black',
    paddingTop: 10,
    borderBottomColor: '#1f6fe7',
    borderBottomWidth: 1

  },
  texth1: { fontWeight: 'bold', color: '#1f6fe7' },

  texth2: { color: '#1f6fe7' },
  carditn: { paddingTop: 5, paddingBottom: 5, paddingLeft: 10, paddingRight: 10, color: "#0275d8", borderRadius: 10,  flex:1 },
  playButton: {
    position: 'absolute',
    bottom: 20,
    left: 20,
    right: 20,
  },
  controls: {
    backgroundColor: 'white',
    opacity: 0.7,
    borderRadius: 5,
    position: 'absolute',
    bottom: 20,
    left: 20,
    right: 20,
  },
  progress: {
    flex: 1,
    flexDirection: 'row',
    borderRadius: 3,
    overflow: 'hidden',
  },
  rateControl: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  playControl: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  resizeModeControl: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },

  card: {
    borderWidth: 3,
    borderRadius: 3,
    borderColor: '#000',
    width: 300,
    height: 300,
    padding: 10
  },



  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: '#1a1917',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: '#1a1917',
  },
  highlight: {
    fontWeight: '700',
  },

  safeArea: {
    flex: 1,
    backgroundColor: colors.black
},

gradient: {
    ...StyleSheet.absoluteFillObject
},
scrollview: {
    flex: 1
},
exampleContainer: {
    paddingVertical: 0,
},

exampleContainerLight: {
    backgroundColor: 'white'
},
title: {
    paddingHorizontal: 30,
    backgroundColor: 'transparent',
    color: 'rgba(255, 255, 255, 0.9)',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center'
},

subtitle: {
    marginTop: 5,
    paddingHorizontal: 30,
    backgroundColor: 'transparent',
    color: 'rgba(255, 255, 255, 0.75)',
    fontSize: 13,
    fontStyle: 'italic',
    textAlign: 'center'
},
slider: {
    marginTop: 0,
    overflow: 'visible', // for custom animations,
   
},
sliderContentContainer: {
    paddingVertical: 10 ,// for custom animation
    borderColor: "green",
    borderWidth: 2
},
paginationContainer: {
    paddingVertical: 5
},
paginationDot: {
    width: 8,
    height: 8,
    borderRadius: 4,
    marginHorizontal: 8
},
slideInnerContainer: {
  width: itemWidth,
  height: slideHeight,
  paddingHorizontal: itemHorizontalMargin,
  paddingBottom: 10 ,// needed for shadow,
  borderColor:"yellow",
  borderWidth:1,
  paddingLeft: -20,
  //right: 20
},
shadow: {
  position: 'absolute',
  top: 0,
  left: itemHorizontalMargin,
  right: itemHorizontalMargin,
  bottom: 18,
  shadowColor: '#000',
  shadowOpacity: 0.25,
  shadowOffset: { width: 0, height: 10 },
  shadowRadius: 10,
  borderRadius: entryBorderRadius
},
imageContainer: {
  flex: 1,
  marginBottom: IS_IOS ? 0 : -1, // Prevent a random Android rendering issue
  backgroundColor: 'white',
  borderTopLeftRadius: entryBorderRadius,
  borderTopRightRadius: entryBorderRadius
},
imageContainerEven: {
  backgroundColor: '#000',
},
image: {
  ...StyleSheet.absoluteFillObject,
  resizeMode: 'cover',
  borderRadius: IS_IOS ? entryBorderRadius : 0,
  borderTopLeftRadius: entryBorderRadius,
  borderTopRightRadius: entryBorderRadius
},
radiusMask: {
  position: 'absolute',
  bottom: 0,
  left: 0,
  right: 0,
  height: entryBorderRadius,
  backgroundColor: 'white'
} ,
radiusMaskEven: {
  backgroundColor: colors.black
},
textContainer: {
  justifyContent: 'center',
  paddingTop: 20 - entryBorderRadius,
  paddingBottom: 20,
  paddingHorizontal: 16,
  backgroundColor: 'white',
  borderBottomLeftRadius: entryBorderRadius,
  borderBottomRightRadius: entryBorderRadius
},
textContainerEven: {
  backgroundColor: '#000',
},
title: {
  color: '#000',
  fontSize: 13,
  fontWeight: 'bold',
  letterSpacing: 0.5
},
titleEven: {
  color: 'white'
},
subtitle: {
  marginTop: 6,
  color: 'gray',
  fontSize: 12,
  fontStyle: 'italic'
},
subtitleEven: {
  color: 'rgba(255, 255, 255, 0.7)'
}

})


export default Candidate_profile1;