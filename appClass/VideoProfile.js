import React, { Component } from 'react';
import { Image,TouchableOpacity,StyleSheet,FlatList,ScrollView,Alert,ImageBackground,ActivityIndicator,Platform,BackHandler} from 'react-native';
import { Container, Header, Content, View,Form, Item, Input, Label,Button,Text,Icon, Card,Toast,Spinner, CardItem, Thumbnail, Left, Body, Right} from 'native-base';
import Video from "react-native-video";
import AsyncStorage from '@react-native-community/async-storage';
import LightVideo from "../assest/one.mp4";
// import { FlatList } from 'react-native-gesture-handler';
const {baseUrl} = require('./baseurl/Url');
var userid;



class VideoProfile extends Component {
  
  constructor(props) {
    super(props);
  
     this.state = {
      Email : '',
      MobileNumber : '',
      PhoneNumber: '',
      emailerr : false,
      MobileNumbererr : false,
      PhoneNumbererr : false,
      videoPath : '',
      videotype : '',
      videoName : '',
      startButtonVisible : true,
      reTakeButtonVisible : false,
      uploadButtonVisible : false,
      cancelButtonVisible : false,
      nextButtonVisible : true,
      prevButtonVisible : false,
      videoPause: true,
      job_typeArr : [],
      questionArr : [],
      questionObject : {},
      questionTotal : 0,
      currentQuestionIndex : 0,
      data : {},
      videoList : [],
      isRefresh : true,
      pointerEventView : 'box-none',
      progressBarVisible : false
     };

     this.loadVideo = this.loadVideo.bind(this);
     this.uploadVideo = this.uploadVideo.bind(this);
     this.reTakedeleteVideo = this.reTakedeleteVideo.bind(this);
  } 
  
  
  static navigationOptions =({navigation})=> {
  return   { title  : 'Video Profile',
    headerLeft : (<TouchableOpacity onPress={()=> { 
      navigation.navigate('Candidate_profile1', { refresh_screen : 'yes'});
       //navigation.navigate('Candidate_profile1'); 
       //navigation.state.params.getProfileData();
      //navigation.navigate('Candidate_profile1');
     }}><Image source={require('../assest/left_arrow.png')} style={{width: 24,height: 24,marginLeft : 20,marginRight: 10,marginBottom: 10,marginTop: 10,padding: 10}} /></TouchableOpacity>),
    headerStyle: { backgroundColor: '#2e2b48' },
    headerTintColor: 'white',
    } 
  };

   async componentDidMount(){
     var quesName = '';
       try{
         console.log('video profile : ',this.props.navigation.state.params.item)
        const { video_url,ques_name } = this.props.navigation.state.params.item;
         quesName = ques_name;
        this.setState({ videoPath : video_url });
       }catch(err){
         console.log('error : in params video path');
         this.setState({videoPath : ''});
       }
       try {
          userid = await AsyncStorage.getItem('UserId');
          if (userid !== null) {
            // We have data!!
            console.log('user id : ',userid);
            fetch(baseUrl+"/mobile/api/get_candidate_profile_info", {
              method: 'POST',
               headers: new Headers({
                 'Accept': 'application/json',
                    'Content-Type': 'application/json',
               }),
              body: JSON.stringify({
                user_id: userid
              })
           })
           .then(response => {
             response.json().then(respond => {
               console.log('response ',respond);
                var arr = respond.data.candidate_job_type.split(',');
                if(respond.status === "1"){
                    this.setState({ data : respond.data ,job_typeArr : arr});
                    this.answerApi(quesName); 
                }
                    console.log('state data : ',this.state.data);
                   
             })
          })
          .catch(error => {
            console.error(error);
          });
      
          }
        } catch (error) {
          console.error('error in AsyncStorage data: ',error);
        }
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }


     
     componentWillUnmount() {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
     }
     
     handleBackButton = () => {
      this.props.navigation.state.params.getProfileData();
      this.props.navigation.goBack();
       return true;
     } 

    defaultApis = async(ques_name) => {
      var action = this.props.navigation.state.params.action;
      console.log('action' + action);
      userid = await AsyncStorage.getItem('UserId');
      //fetch(baseUrl+"/mobile/api/question")
          fetch(baseUrl+"/mobile/api/videoquestion", {
            method: 'POST',
            headers: new Headers({
              'Accept': 'application/json',
                  'Content-Type': 'application/json',
            }),
            body: JSON.stringify({
              user_id: userid, action : action
            })
        })
            .then(response => {
              response.json().then(respond => {
                console.log('response question : ',respond);
                  if(respond.status === "1"){
                        this.setState({ questionArr : respond.data, total_question : respond.data.length});
                        if(respond.data !== null){
                            if(ques_name !== ''){
                                    respond.data.map((item,index) => {
                                          if(ques_name !== ''){
                                              if(item.ques_name === ques_name){
                                                  this.getQuestionObjectbyId(item.id,index);
                                                  }
                                          }    
                                      });
                            }else{
                              if(this.props.navigation.state.params.ques_id){
                                var qid = this.props.navigation.state.params.ques_id;
                                respond.data.map((item,index) => {
                                    if(item.id === qid){
                                        this.getQuestionObjectbyId(item.id,index);
                                    }
                                });

                                //this.getQuestionObjectbyId(qid,0);
                              }else{
                                this.getQuestionObjectbyId(respond.data[0].id,0);
                              }
                            }
                          
                      }
                          this.totalQuestion();
                        
                  }
              })
            })
            .catch(error => {
              console.error(error);
            });
        
    }

    totalQuestion = ()=> {
                fetch(baseUrl+"/mobile/api/count_all_question")
                .then(response => {
                  response.json().then(respond => {
                    console.log('response total question : ',respond);
                      if(respond.status === "1"){
                          this.setState({ questionTotal : respond.total_question});
                          }
                      this.setState({isRefresh: false});    
                  })
              })
              .catch(error => {
                console.error(error);
              });
    }

    getQuestionObjectbyId= (id,index)=> {
       console.log('question id ',id);
       console.log('index id ',index);
        fetch(baseUrl+"/mobile/api/single_question", {
          method: 'POST',
          headers: new Headers({
            'Accept': 'application/json',
                'Content-Type': 'application/json',
          }),
          body: JSON.stringify({
            id: id
          })
      })
      .then(response => {
        response.json().then(respond => {
          console.log('response single question : ',respond);
            if(respond.status === "1"){
                   var prevVisible = (index == 0)? false : true;
                   console.log('question arr length : ',this.state.questionArr.length);
                   var nextVisible = ((this.state.questionArr.length-1) == index )? false: true ;
                      if(this.state.videoList.length != 0){
                           console.log('video List :',this.state.videoList);
                            var startButtonVisible = true;
                            var reTakeButtonVisible= false;
                            var videopath = '';
                            this.state.videoList.map((item)=> {
                              if(item.ques_name == respond.data.ques_name){
                                      console.log('find video in list : ',item);
                                      startButtonVisible = false;
                                      reTakeButtonVisible = true;
                                      videopath = item.video_url;
                              }
                          });
                          console.log('set visible start button :',startButtonVisible);
                          console.log('set visible retake button :',reTakeButtonVisible);
                          console.log('video path :',videopath);
                          this.setState({videoPath : videopath,startButtonVisible : startButtonVisible,reTakeButtonVisible: reTakeButtonVisible});
                       
                    }    
                  this.setState({ questionObject : respond.data, currentQuestionIndex : index,prevButtonVisible : prevVisible,nextButtonVisible : nextVisible });
                }
                this.setState({pointerEventView : 'box-none',progressBarVisible: false});
        })
     })
     .catch(error => {
       console.error(error);
     });
    }


    uploadVideo = async(ques_id,quesName)=> {
            this.setState({pointerEventView : 'box-only',progressBarVisible: true});
         const formData = new FormData();
            formData.append('file', {uri: this.state.videoPath,name: this.state.videoName,type: this.state.videotype });
            formData.append('user_id',userid);
            formData.append('question_id',ques_id);
              console.log('upload video request data : ',formData);
            const options = {
              method: 'POST',
              headers: {
                'Content-type': 'multipart/form-data'
              },
              body: formData,
            };
      try{
          let res =   await fetch(baseUrl+'/mobile/api/upload_video', options)
              res.json().then((response)=> {
                      console.log('response : ',response);
                          if (response.status == "1") {
                            Toast.show({
                              text: response.message,
                              type: "success"
                            });
                            this.setState({ cancelButtonVisible : false,uploadButtonVisible : false,
                              reTakeButtonVisible : true,startButtonVisible: false , progressBarVisible: false});  
                              this.answerApi(quesName);
                            } else {
                            Toast.show({
                              text: response.message,
                              type: "danger"
                            });
                            this.setState({pointerEventView : 'box-none',progressBarVisible: false});
                          }
                 
              }).catch((error)=> {
                this.setState({pointerEventView : 'box-none',progressBarVisible: false});
                  console.log('error : ',error);
              })

      }catch(err){
            console.log('error in uploading video :',err)
      }
     
    }

    loadVideo = (path,type,name)=> {
        console.log('path : ',path);
        console.log('type : ',type);
        console.log('name : ',name);
       this.setState({ videoPath : path, videoName: name,videotype : type,cancelButtonVisible : true,reTakeButtonVisible: false,
        uploadButtonVisible : true,startButtonVisible: false,nextButtonVisible: false,prevButtonVisible: false});
    }

    reTakedeleteVideo = (ques_id)=>{
          this.setState({ pointerEventView : 'box-only',progressBarVisible: true });
              fetch(baseUrl+'/mobile/api/delete_video', {
                method: 'POST',
                headers: new Headers({
                  'Accept': 'application/json',
                      'Content-Type': 'application/json',
                }),
                body: JSON.stringify({ user_id: userid,
                  question_id : ques_id })
            })
            .then((res)=> {
                  res.json().then((response)=> {
                        console.log('response : ',response);
                        if (response.status == "1") {
                          Toast.show({
                            text: response.message,
                            type: "success"
                          });

                          //this.props.navigation.state.params.getProfileData(); 
                          this.props.navigation.navigate('VideoProfileCreate' , { action : 'create', ques_id : ques_id});

                          //this.setState({ cancelButtonVisible : false,videoPath: '',uploadButtonVisible : false,reTakeButtonVisible : false,startButtonVisible: true});  
                        //    this.componentDidMount();

                        /*
                          fetch(baseUrl+"/mobile/api/answer_video_detail", {
                            method: 'POST',
                            headers: new Headers({
                              'Accept': 'application/json',
                                  'Content-Type': 'application/json',
                            }),
                            body: JSON.stringify({
                              user_id : userid
                            })
                        })
                        .then(response => {
                          response.json().then(respond => {
                            console.log('response video list : ',respond);
                              if(respond.status == "1"){
                                  this.setState({ videoList : respond.data});
                              }else {
                                this.setState({ videoList : []});   
                              }
                              this.setState({pointerEventView : 'box-none', progressBarVisible: false});
                          })
                        })
                        .catch(error => {
                          this.setState({pointerEventView : 'box-none', progressBarVisible: false});
                          console.error(error);
                        });
                        */


                      } else {
                          Toast.show({
                            text: response.message,
                            type: "danger"
                          });
                          this.setState({pointerEventView : 'box-none',progressBarVisible: false});
                        }
                  })
            }).catch((error)=> {
              this.setState({pointerEventView : 'box-none',progressBarVisible: false});
                console.log('error : ',error);
            })
    }

    answerApi = (quesName)=> {
            fetch(baseUrl+"/mobile/api/answer_video_detail", {
              method: 'POST',
              headers: new Headers({
                'Accept': 'application/json',
                    'Content-Type': 'application/json',
              }),
              body: JSON.stringify({
                user_id : userid
              })
          })
          .then(response => {
            response.json().then(respond => {
              console.log('response video list : ',respond);
                if(respond.status === "1"){
                  console.log('answerApi quesName: ',quesName);
                  
                        if(quesName !== ''){
                          this.defaultApis(quesName);
                        }else{
                          this.defaultApis('');
                        }
                  
                    this.setState({ videoList : respond.data});
                }else{
                   this.defaultApis('');
                   this.setState({ videoList : [] });
                  }
            })
          })
          .catch(error => {
            console.error(error);
          });
    }
 
    render() {
     if(this.state.isRefresh){
              return <View style={{flex : 1,justifyContent : 'center'}}><ActivityIndicator size="large" color="#0000ff" /></View>;
      }else {
              return (
                <ImageBackground source={require('../assest/login_bg.png')} style={{width: '100%', height: '100%'}}>
                <ScrollView>
                  <View style={styles.container} >
                  <Form>

                  <View style={{flex : 1}}>
                     
                  <View style={{ flex:1, flexDirection : 'row',}} >
                      <View style={{flex:1}}>
                        <Image source={{uri : this.state.data.img_url}} style={{height: 70, width: 70,borderRadius: 400/ 2}}/>
                      </View>
                      <View style={{flex:3,}}>
                        <Text style={{ textAlign: "left", padding : 5,fontSize: 18,marginLeft: 5,fontWeight: 'bold'}}>{this.state.data.name} {this.state.data.surname}</Text> 
  
                      </View> 
                      

                      {/* <Text style={{padding : 8,fontSize : 12,color : '#0275d8',marginLeft: 5}}>{this.state.data.phone_number}</Text> */}
                      
                    </View>
                    <View style={{justifyContent:"center", alignItems:"center"}}>
                          <TouchableOpacity onPress={() => {
                                    this.props.navigation.navigate('VideoTips',{videoItem : { video_url : 'https://protechgrad.protechgenie.in/upload/videointerview_wb.mp4' }});
                                  }}>
                             <Image source={require('../assest/videmg.png')} style={{width:120,height: 70}} />
                          </TouchableOpacity>
                        </View>

                    <Card style={{backgroundColor: '#b1ceda',marginTop: 10}}>
                          <View style={{flexDirection: 'row',justifyContent: 'flex-end',padding: 8}} >  
                              <Text style={{fontSize: 12,color: '#1d426c'}}>Allocated time 00:00:{this.state.questionObject.duration}</Text>
                        </View>

                        {/* style={{  height: 200,width : '100%'}} */}
                        <View style={{backgroundColor : '#000000',flex: 1}}>
                          {  this.state.videoPath == ''?
                              <Image source={require('../assest/vidscreen.png')} style={{ height: 230,width : '100%'}}/>
                            : <Video  
                              source={{ uri: this.state.videoPath}} 
                              paused = {true}
                              controls={true}
                              style={{height: 200,width : '100%', flex: 1}} /> 
                        }
                      </View> 
      
                                    
                    </Card>

                    <Card style={{padding: 10}}>
                      <View>
                      <Text style={{alignSelf: 'flex-end',color: '#474747',fontSize: 12}}>{this.state.currentQuestionIndex+1}/{this.state.total_question}</Text>
                    <Label style={{color : '#202020',fontWeight: 'bold',fontSize: 14,padding: 5 }}>Questions</Label>
                    <Text style={{color : '#636363',padding: 5,marginLeft: 8,fontSize: 12}}>{this.state.questionObject.ques_name}</Text>
                  

                    <View style={{paddingTop: 10 ,flexDirection: 'row'}} >  
                    {this.state.startButtonVisible?   <Button block onPress={()=> {  this.props.navigation.navigate('OpenCamera',{loadVideo : this.loadVideo,duration: this.state.questionObject.duration}); }}
                    style={{height: 30, marginLeft:5,marginRight:2,borderRadius:5,backgroundColor: 'green'}}>
                      <Text uppercase={false} >Start</Text>
                    </Button> : null}
                    
                    
                  {this.state.cancelButtonVisible?  <Button block onPress={()=> {   this.setState({ videoPath : '', videoName: '',videotype : '',cancelButtonVisible : false,uploadButtonVisible : false,reTakeButtonVisible : false,startButtonVisible: true}); this.getQuestionObjectbyId(this.state.questionObject.id,this.state.currentQuestionIndex); }} 
                    style={{height: 30,marginLeft:5,marginRight:2,borderRadius:5}} >
                      <Text uppercase={false}>Cancel</Text>
                       </Button> : null}
                    
                  
                    {this.state.reTakeButtonVisible? <Button block onPress={()=> { 
                      Alert.alert(
                        'Gradhire',
                        'Are you sure delete this video',
                        [
                          {
                            text: 'Cancel',
                            onPress: () => console.log('Cancel Pressed'),
                            style: 'cancel',
                          },
                          { text: 'OK', onPress: () => {  this.reTakedeleteVideo(this.state.questionObject.id); } },
                        ]
                      );  }}
                    style={{height: 30,marginLeft:5,marginRight:2,borderRadius:5,backgroundColor: '#e71f45'}}
                    >
                      <Text uppercase={false}>Re-Take</Text>
                    </Button> : null }
                    {this.state.prevButtonVisible? <Button block onPress={()=> { this.getQuestionObjectbyId(this.state.questionArr[this.state.currentQuestionIndex-1].id,this.state.currentQuestionIndex-1); }}
                    style={{height: 30,marginLeft:5,marginRight:2,borderRadius:5,backgroundColor: '#1f6fe7'}}
                    >
                      <Text uppercase={false}>Previous</Text>
                    </Button> : null }

                    {this.state.nextButtonVisible? <Button block onPress={()=> {  this.getQuestionObjectbyId(this.state.questionArr[this.state.currentQuestionIndex+1].id,this.state.currentQuestionIndex+1);  }}
                    style={{height: 30,marginLeft:5,marginRight:2,borderRadius:5,backgroundColor: '#1f6fe7'}} >
                      <Text uppercase={false}>Next</Text>
                    </Button> : null }

                    { (this.state.currentQuestionIndex+1 == this.state.total_question && this.state.prevButtonVisible) ? 
                      <Button block onPress={()=> { this.props.navigation.navigate('Candidate_profile1');  }}
                      style={{height: 30,marginLeft:5,marginRight:2,borderRadius:5,backgroundColor: '#1f6fe7'}} >
                        <Text uppercase={false}>Finished</Text>
                      </Button>
                     : null }
                  
                    {this.state.uploadButtonVisible?<Button block onPress={()=> { this.uploadVideo(this.state.questionObject.id,this.state.questionObject.ques_name); }}
                    style={{height: 30,marginLeft:5,marginRight:2,borderRadius:5}}
                    >
                      <Text uppercase={false}>Save</Text>
                    </Button>  : null}
                    </View>

                        <View pointerEvents={this.state.pointerEventView} style={{position: 'absolute',justifyContent: 'center',width: '100%',height: '100%'}}>
                              {this.state.progressBarVisible && <ActivityIndicator style={{top: 0,left: 0,right: 0}} size="large" color="#0000ff" /> }
                        </View>
                    </View>
                    </Card>



                    <Card style={{padding: 10}}>
                      <View style={{flexDirection: 'row'}}>  
                        <Text style={{color: '#202020',paddingLeft: 5}}>Video</Text> 
                      
                        </View>
                          
            
            
            
                      {/* FlatLIst Video List */}
                        
                      {this.state.videoList.length == 0? <Text style={{alignSelf: 'center',padding: 20}}>No Video</Text>:   
                      <FlatList
                        horizontal 
                        data={this.state.videoList}
                        keyExtractor={item => item.ques_name}
                        renderItem={({item},index) => 
                          <TouchableOpacity onPress={()=> {   this.props.navigation.navigate('VideoPlayer',{videoItem : item});  }}>
                        <Card style={{marginTop:10,width: 200, padding: 10,color: "#0275d8"}}>
                          <View style={{justifyContent: 'center',alignItems: 'center',}}>
                            <Image source={require('../assest/vidscreen.png')} style={{height: 120, width:180}}/>

                            <View style={{justifyContent: 'center',alignItems: 'center', position : "absolute", alignSelf : "center" }}>
                                <TouchableOpacity onPress={() => {
                                  this.props.navigation.navigate('VideoPlayer',{videoItem : item});
                                }}>
                                  <View style={{flexDirection : "row"}}> 
                                    <Icon name="ios-play-circle"  style={{ fontSize:35 , color : '#ffffff'}} />
                                  </View>
                                </TouchableOpacity>
                            </View>
                          </View>
                          <Text style={{flex : 1,flexWrap: 'wrap',fontSize: 12,padding: 3,marginTop : 5}}>{item.ques_name}</Text>
                        

                        </Card>
                        </TouchableOpacity>
                      }
                    />  }
                    </Card>
                
                        
                          </View>
                         
                    </Form>
                   
              </View>
              </ScrollView>
              
              </ImageBackground>
              );
            }
    }
}

const styles = StyleSheet.create({

  backgroundImage: {
    flex: 1,
    resizeMode: 'cover', // or 'stretch'
},
loginForm: {
  flex: 1,
    position: 'absolute',
    backgroundColor: 'transparent', width:'90%', height:null, 
},
  container: {
    
      flex: 1,
      backgroundColor: 'transparent',
      padding : 12
      
  },
  mailicon: {
    height: 10,
    width: 20,
  
}, 

  loginContainer:{
      alignItems: 'center',
      flexGrow: 1,
      justifyContent: 'center'
  },
  logo: {
    flex: 1,
    alignItems: 'center',
      width: 300,
      height: 100,
     justifyContent: 'center',marginLeft:50,
      
  },
  input:{
    fontSize: 16,
    
    color: 'black',
    marginBottom:10, paddingTop:20,
    borderBottomColor: '#1f6fe7',
    borderBottomWidth: 1
    
  }

})

export default VideoProfile;

