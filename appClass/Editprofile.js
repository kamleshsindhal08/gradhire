import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {NavigationActions,StackActions} from 'react-navigation';
import { Image,TouchableOpacity,TouchableHighlight,YellowBox,BackHandler,StyleSheet,ActivityIndicator, ImageBackground, Platform, Dimensions, Alert, ScrollView } from 'react-native';

import ImagePicker from 'react-native-image-crop-picker';
import { Container,Picker, CheckBox,Header, Content, Form, Item, Input,Textarea, Label,Button,Text, Card, CardItem,  Thumbnail, Icon, Left, Body, Right, View,Toast, Row} from 'native-base';
import DatePicker from 'react-native-datepicker'
import AsyncStorage from '@react-native-community/async-storage';
import SectionedMultiSelect from 'react-native-sectioned-multi-select';
import { Dialog } from 'react-native-simple-dialogs';
import HTML from 'react-native-render-html';
import Tooltip from 'react-native-walkthrough-tooltip';
const {baseUrl} = require('./baseurl/Url');
//import {RichTextEditor, RichTextToolbar} from 'react-native-zss-rich-text-editor';

const { height: deviceHeight, width: deviceWidth } = Dimensions.get('window');

class EditProfile extends Component {


  static navigationOptions = {
    title : 'Edit Profile',
    headerStyle: { backgroundColor: '#2e2b48' },
    headerTintColor: 'white',
    headerLeft: <Button title="Save" onPress={() => state.params.handleSave()} />,
  };

  static propTypes = {
    children: PropTypes.any,
    horizontalPercent: PropTypes.number,
    verticalPercent: PropTypes.number,
  };

  constructor(props) {
    super(props);
    this.state = {
      name:'',
      sur_name:'',
      dob:'',
      gender:'',
      email:'',
      job_type:'',
      job_function:'',
      it_skill:'',
      hobbies:'',
      language:'',
      Phone_code:'',
      phone_no:'',
      country:'',
      state1:'',
      city:'',
      post_code:'',
      address:'',
      keyAchivement:'',
      open_to_location:'',
      post_code1:'',
      redius_in_miles1:'',
      post_code2:'',
      redius_in_miles2:'',
      latitude1: '',
      longitude1 : '',
      latitude2: '',
      longitude2 : '',
      openLocationChecked : true,
      mailingListChecked : false,
      chosenDate: new Date(),
      photo: {},
      user_id : '',
      img_url : null,
      selectedTag : 'body',
      selectedStyles : [],
      current_position : props.navigation.state.params.position,

      countryList: [],
      stateList : [],
      cityList : [],
      phoneCodeList : [],
      job_category_SelectedItems: [],
      job_category_selected : [],
      job_type_SelectedItems: [],
      job_type_selected : [],
      jobCategoryList: [],
      jobTypeList : [],

      name_err:  false,
      sur_name_err:  false,
      gender_err:false,
      email_err:false,
      job_type_err:false,
      job_function_err:false,
      it_skill_err:false,
      hobbies_err:false,
      language_err:false,
      Phone_code_err:false,
      phone_no_err:false,
      country_err:false,
      state_err1:false,
      city_err:false,
      post_code_err:false,
      address_err:false,
      keyAchivement_err:false,
      open_to_location_err:false,
      post_code1_err:false,
      redius_in_miles1_err:false,
      post_code2_err:false,
      redius_in_miles2_err:false,
      postcode1Error : false,
      postcode2Error : false,
      isRefresh : true,
      selected2: '',
      imageSelectActionDialogVisible : false,
      submitDisabled: false,
      toolTipVisible : false,
      submit_disable: false,
    };


    this.setDate = this.setDate.bind(this);
    this.defualtApis = this.defualtApis.bind(this);
    this.getApiState = this.getApiState.bind(this);
    this.getApiCity = this.getApiCity.bind(this);
    this.getlocation1PostCode = this.getlocation1PostCode.bind(this);
    this.getlocation2PostCode = this.getlocation2PostCode.bind(this);
    this.mulitSelectApis = this.mulitSelectApis.bind(this);
    this.uploadImageApi = this.uploadImageApi.bind(this);
    this.handleChoosePhoto = this.handleChoosePhoto.bind(this);
    this.cameraImagePicker = this.cameraImagePicker.bind(this);
    this.gallaryImagePicker = this.gallaryImagePicker.bind(this);
    this.getHTML = this.getHTML.bind(this);
    this.setFocusHandlers = this.setFocusHandlers.bind(this);

    this.handleBackButton = this.handleBackButton.bind(this);

    this.didFocus = props.navigation.addListener("didFocus", payload =>
    BackHandler.addEventListener("hardwareBackPress", this.onBack),
  );

  console.log('test123');
  console.log(global.position);
  }



  onStyleKeyPress = (toolType) => {
    this.editor.applyToolbar(toolType);
}

onSelectedTagChanged = (tag) => {
    this.setState({
        selectedTag: tag
    })
}

onSelectedStyleChanged = (styles) => { 
    this.setState({
        selectedStyles: styles,
    })
}

onValueChanged = (value) => {
    this.setState({
        value: value
    });
}


  async componentDidMount(){
    console.log('url : ',baseUrl)
    try {
      const value = await AsyncStorage.getItem('UserId');
      const email = await AsyncStorage.getItem('Emailid');
      if (value !== null) {
        // We have data!!
        console.log('user id : ',value);
        fetch(baseUrl+"/mobile/api/get_candidate_profile_info", {
          method: 'POST',
           headers: new Headers({
             'Accept': 'application/json',
                'Content-Type': 'application/json',
           }),
          body: JSON.stringify({
            user_id: value
          })
       })
       .then(response => {
         response.json().then(respond => {
          var sjobCategory = '';
           console.log('response ',respond);
            if(respond.status === "1"){

              if(respond.data.name !== ""){
                var data = respond.data;
                var sjobCategory = (data.current_position !="" )? data.current_position.split(",") : [];


                var sjobType = (data.job_type !="" ) ? data.job_type.split(",") : [] ;
                 var openlocation = (data.open_location == "1")? true : false;
                 var mailingChecked = (data.mailing_status == "1")? true : false;
                 var  post_code_1='',distance_1 = '',post_code_2='',distance_2 ='',lat1='',lng1='',lat2='',lng2='';
                  if(!openlocation){
                         post_code_1 = respond.data.live_postcode;
                        distance_1 = respond.data.live_distance;
                        lat1 = respond.data.live_lat;
                        lng1 = respond.data.live_lng;
                        lat2 = respond.data.uni_lat;
                        lng2 = respond.data.uni_lng;
                        post_code_2 = respond.data.uni_postcode;
                        distance_2 = respond.data.uni_distance;
                  }
                console.log('job Category : ',sjobCategory)
                console.log('job Type : ',sjobType);
                this.setState({  
                 name: data.name ,
                sur_name: data.surname ,
                dob: data.dob,
                openLocationChecked : openlocation,
                mailingListChecked : mailingChecked,
                gender: data.gender,
                email: email,
                job_category_selected : sjobCategory,
                job_type_selected : sjobType, 
                it_skill: data.it_skills,
                hobbies: data.hobbies,
                language: data.lang_skills,
                Phone_code: data.country_code,
                phone_no: data.phone_number,
                country: data.country,
                state1: data.state,
                Phone_code : data.countrycode,
                city: data.city,
                post_code: data.postcode,
                address: data.address,
               keyAchivement: data.description,
                 user_id : value,
                img_url : data.img_url,
               post_code1: post_code_1,
               redius_in_miles1: distance_1,
               latitude1:  lat1,
              longitude1: lng1,
               post_code2 : post_code_2,
               redius_in_miles2: distance_2,
               latitude2:  lat2,
              longitude2: lng2,});

                console.log(this.state);

                 this.getApiState(data.country);
                 this.getApiCity(data.state);
            }else {
               this.setState({ email : email,user_id: value,img_url : respond.data.img_url});
            }
            
            this.mulitSelectApis();
               this.defualtApis();  
               //this.setState({  isRefresh : false });

               //var job_category_selected = this.state.job_category_selected;
               //this.setState({ current_position : job_category_selected });

            }
               
         })
      })
      .catch(error => {
        console.error(error);
      });
     
      }
    } catch (error) {
      console.error('error in AsyncStorage data: ',error);
    }
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }
  
    componentWillUnmount() {
      BackHandler.removeEventListener("hardwareBackPress", this.handleBackButton);
    }
     
     handleBackButton = () => {
          this.props.navigation.goBack();
          return true;
     } 

    defualtApis = async()=> {
       
     await fetch(baseUrl+"/mobile/api/country")
     .then(response => {
       response.json().then(respond => {
         console.log('country list : ',respond);
           if(respond.status == "1"){
                 this.setState({ countryList : respond.countries, phoneCodeList : respond.countries, isRefresh : false });      
           }
       })
     })
     .catch(error => {
      console.error(error);
     });

    
    }

    mulitSelectApis = async()=>{
      await fetch(baseUrl+"/mobile/api/job_category")
      .then(response => {
        response.json().then(respond => {
          console.log('job category list : ',respond);
          console.log(this.state.job_category_selected);
            if(respond.status == "1"){
              //this.setState({ jobCategoryList : respond.data }); 
                  this.setState({ jobCategoryList : (typeof(respond.data) !== 'undefined') ? respond.data : []  , job_category_SelectedItems : this.state.job_category_selected });    
                  
                 // console.log('job category list : ',job_category_SelectedItems);
            }
        })
      })
      .catch(error => {
       console.error(error);
      });
      
      await fetch(baseUrl+"/mobile/api/job_type")
      .then(response => {
        response.json().then(respond => {
          console.log('job type list : ',respond);
            if(respond.status == "1"){
                  this.setState({ jobTypeList : respond.data ,job_type_SelectedItems : this.state.job_type_selected });      
            }
        })
      })
      .catch(error => {
       console.error(error);
      });
    }

    getApiState = async(country) => {
      await fetch(baseUrl+"/mobile/api/state", {
        method: 'POST',
         headers: new Headers({
           'Accept': 'application/json',
              'Content-Type': 'application/json',
         }),
        body: JSON.stringify({
          country_id: country
        })
        }).then(response => {
          response.json().then(respond => {
            console.log('state list : ',respond);
              if(respond.status == "1"){
                    this.setState({ stateList : respond.statelist });
              }
          })
        })
        .catch(error => {
        console.error(error);
        });  
    }

    getApiCity = async(v) => {
      await fetch(baseUrl+"/mobile/api/city", {
        method: 'POST',
         headers: new Headers({
           'Accept': 'application/json',
              'Content-Type': 'application/json',
         }),
        body: JSON.stringify({
          state_id: v
        })
        }).then(response => {
          response.json().then(respond => {
            console.log('city list : ',respond);
              if(respond.status == "1"){
                    this.setState({ cityList : respond.statelist });
              }
          })
        })
        .catch(error => {
        console.error(error);
        });  
    }



  editprofile = async()=>
  {

    var name = this.state.name.trim();
    var sur_name = this.state.sur_name.trim();
    var email = this.state.email.trim();
    var it_skill = this.state.it_skill.trim();
    var hobbies = this.state.hobbies.trim();
    var language = this.state.language.trim();
    var phone_no = this.state.phone_no.trim();
    var post_code = this.state.post_code.trim();
    var address = this.state.address.trim();
    var keyAchivement = this.state.keyAchivement.trim();
    var post_code1 = this.state.post_code1.trim();
    var redius_in_miles1 = this.state.redius_in_miles1.trim();
    var post_code2 = this.state.post_code2.trim();
    var redius_in_miles2 = this.state.redius_in_miles2.trim();


    console.log('name : ',name);
    console.log('sur_name : ',sur_name);
  //  console.log('dob : ',ExamType);

    console.log('gender : ',this.state.gender);
    console.log('job_type : ',this.state.job_type_SelectedItems);
   // console.log('job_category : ',this.state.job_category_SelectedItems);
    
  
    console.log('it_skill  : ',it_skill);
    console.log('hobbies  : ',hobbies);
    console.log('language  : ',language);

    console.log('Country /Phone_code : ',this.state.Phone_code);
    console.log('phone_no  : ',phone_no);

    console.log('country : ',this.state.country);
    console.log('state1 : ',this.state.state1);
    console.log('city : ',this.state.city);
    
    console.log('post_code  : ',post_code);
    console.log('address  : ',address);
    console.log('keyAchivement  : ',keyAchivement);
    console.log('post_code1  : ',post_code1);
    console.log('redius_in_miles1  : ',redius_in_miles1);
    console.log('post_code2  : ',post_code2);
    console.log('redius_in_miles2  : ',redius_in_miles2);
    console.log('dob  : ',this.state.dob);
    console.log('openLocationCheck Box : ',this.state.openLocationChecked);
    console.log('mailing status Box : ',this.state.openLocationChecked);
    


    if(name === ""){
      console.log('name : ',);
      this.setState({ name_err : true});   
      Toast.show({
        text: 'Invalid Name',
        type: "danger",
      });
      return; 
    }
    this.setState({ name_err : false}); 

    
    if(sur_name === ""){
      console.log('sur_name : ',);
      this.setState({ sur_name_err : true});   
      Toast.show({
        text: 'Invalid Surname',
        type: "danger"
      });
      return; 
    }
   
    this.setState({ sur_name_err : false}); 
   


    // dob 
    /*
    if(this.state.dob === ""){
      console.log('sur_name : ',);
      Toast.show({
        text: 'Select Date of Birth',
        type: "danger"
      });   
      return; 
    }
    */

    /*
    if(this.state.gender === "" || this.state.gender === "Select"){
      console.log('gender  : ',this.state.gender);
      this.setState({ gender_err : true});
      Toast.show({
        text: 'Select Gender',
        type: "danger"
      });
      return;
     }
     this.setState({ gender_err : false});
    */
     
    if(email === ""){
      console.log('email : ',);
      this.setState({ email_err : true});   
      Toast.show({
        text: 'Invalid Email Address',
        type: "danger",
      });
      return; 
    }
    this.setState({ email_err : false}); 
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    if (!pattern.test(email)) {
      this.setState({
            emailerr : true,    
      });
      Toast.show({
        text: 'Invalid Email Address',
        type: "danger"
      });
      console.log('email pattern invalid : ',email);
      return;
    }       
    this.setState({ emailerr : false });

    /*
    if(this.state.job_type_SelectedItems.length == 0){
      this.setState({ job_type_err : true}); 
      console.log('job type selecteditems : ',this.state.job_category_SelectedItems);
      Toast.show({
        text: 'Select Job Type',
        type: "danger"
      });
      return; 
    }

    this.setState({ job_type_err : false}); 

    if(this.state.job_category_SelectedItem.length == 0){
      this.setState({ job_category_err : true}); 
      console.log('job type selecteditems : ',this.state.job_category_SelectedItems);
      Toast.show({
        text: 'Select Job Category',
        type: "danger"
      });
      return; 
    }

    this.setState({ job_category_err : false}); 
    
    if(it_skill === ""){
      console.log('it_skill : ',);
      this.setState({ it_skill_err : true});   
      Toast.show({
        text: 'Invalid IT Skills',
        type: "danger"
      });
      return; 
    }
    this.setState({ it_skill_err : false}); 

    if(hobbies === ""){
      console.log('hobbies : ',);
      this.setState({ hobbies_err : true});
      Toast.show({
        text: 'Invalid Hobbies',
        type: "danger"
      });   
      return; 
    }
    this.setState({ hobbies_err : false}); 
    */

    /*
    if(language === ""){
      console.log('language : ',);
      this.setState({ language_err : true}); 
      Toast.show({
        text: 'Invalid language',
        type: "danger"
      });   
      return; 
    }
    this.setState({ language_err : false}); 
    */

    /*
    if(this.state.Phone_code === "" || this.state.Phone_code === "Select"){
      console.log('Phone_code  : ',this.state.Phone_code);
      this.setState({ Phone_code_err : true});
      Toast.show({
        text: 'Select Phone No',
        type: "danger"
      }); 
      return;
     }
     this.setState({ Phone_code_err : false});
   */

    if(phone_no != "" && phone_no.length < 10){
      console.log('phone_no : ',);
      this.setState({ phone_no_err : true});   
      Toast.show({
        text: 'Invalid Phone no',
        type: "danger"
      }); 
      return; 
    }
    this.setState({ phone_no_err : false}); 

    /*
    if(this.state.country === "" || this.state.country === "Select"){
      console.log('country  : ',this.state.country);
      this.setState({ country_err : true});
      Toast.show({
        text: 'Select Country',
        type: "danger"
      }); 
      return;
     }
     this.setState({ country_err : false});

     if(this.state.state1 === "" || this.state.state1 === "Select"){
      console.log('state1  : ',this.state.state1);
      this.setState({ state_err : true});
      Toast.show({
        text: 'Select State',
        type: "danger"
      });
      return;
     }
     this.setState({ state_err : false});

     if(this.state.city === "" || this.state.city === "Select"){
      console.log('city  : ',this.state.city);
      this.setState({ city_err : true});
      Toast.show({
        text: 'Select City',
        type: "danger"
      });
      return;
     }
     this.setState({ city_err : false});
    
    if(post_code.length < 7 ){
      console.log('post_code : ',);
      this.setState({ post_code_err : true});
      Toast.show({
        text: 'Enter postcode in 7-8 character',
        type: "danger"
      });   
      return; 
    }
    this.setState({ post_code_err : false}); 
    */

    /*
    if(address === ""){
      console.log('address : ',);
      this.setState({ address_err : true});
      Toast.show({
        text: 'Invalid Address',
        type: "danger"
      });   
      return; 
    }
    this.setState({ address_err : false}); 
    

    if(keyAchivement === ""){
      console.log('keyAchivement : ',);
      this.setState({ keyAchivement_err : true});
      Toast.show({
        text: 'Invalid Key Achievements',
        type: "danger"
      });   
      return; 
    }
    this.setState({ keyAchivement_err : false}); 
    */

    
    if(!this.state.openLocationChecked){
      
      console.log('open location post code');
        
           // if(post_code1 !== ""){
                console.log('post code 1 validation');
                
                if(post_code1 === "" && post_code2 === "" && redius_in_miles1 === "" && redius_in_miles2 === "" ){
                  console.log('post_code1 : ');
                  Toast.show({
                    text: 'Postcode & Radius is required',
                    type: "danger"
                  });    
                  return; 
                }

                /*
                if(post_code1 === "" || this.state.postcode1Error){
                  console.log('post_code1 : ');
                  this.setState({ post_code1_err : true,postcode1Error: true});
                  Toast.show({
                    text: 'Invalid Postcode',
                    type: "danger"
                  });    
                  return; 
                }
                this.setState({ post_code1_err : false,postcode1Error: false}); 
                */


                if(post_code1.length < 7){
                  console.log('post_code1 : ');
                  this.setState({ post_code1_err : true,postcode1Error: true});
                  Toast.show({
                    text: 'Enter postcode in 7-8 character',
                    type: "danger"
                  });    
                  return; 
                }
                this.setState({ post_code1_err : false,postcode1Error: false}); 
               
              if(post_code1.length >= 7 && redius_in_miles1 === ""){
                console.log('redius_in_miles1 : ',);
                this.setState({ redius_in_miles1_err : true});
                Toast.show({
                  text: 'Enter Redius Milies distance',
                  type: "danger"
                });    
                return; 
              }
              this.setState({ redius_in_miles1_err : false,}); 
                  //redius_in_miles2 = '';

            //}else if(post_code2 !== ""){
              console.log('post code 2 validation');
                    
                  /*
                    if(post_code2 === "" || this.state.postcode2Error){
                      console.log('post_code2 : ',);
                      this.setState({ post_code2_err : true,postcode2Error: true});
                      Toast.show({
                        text: 'Invalid Postcode',
                        type: "danger"
                      });   
                      return; 
                    }
                    this.setState({ post_code2_err : false,postcode2Error: false})
                    */

                   if(post_code2 != ""){
                      if(post_code2 != "" && post_code2.length < 7){
                        console.log('post_code2 : ',);
                        this.setState({ post_code2_err : true,postcode2Error: true});
                        Toast.show({
                          text: 'Enter postcode in 7-8 character',
                          type: "danger"
                        });   
                        return; 
                      }
                      this.setState({ post_code2_err : false,postcode2Error: false})
                
                      if(post_code2.length >= 7  && redius_in_miles2 === ""){
                        console.log('redius_in_miles2 : ',);
                        this.setState({ redius_in_miles2_err : true});
                        Toast.show({
                          text: 'Enter Redius Milies distance',
                          type: "danger"
                        });   
                        return; 
                      }
                      this.setState({ redius_in_miles2_err : false, }); 
                      //redius_in_miles1 = '';    

                  }
                  
          //}
  
          
    }
    
  

    console.log('end validation');

    var locationCheck = (this.state.openLocationChecked)? "1": "0";
    var mailingChk = (this.state.mailingListChecked)? "1": "0";
    
    console.log('location checkbox ',locationCheck);
    console.log('mailingChk checkbox ',mailingChk);

   
      var jobTypeString = this.state.job_type_SelectedItems.toString();
      var jobCategoryString = this.state.job_category_SelectedItems.toString();
      console.log('jobTypeString arr :',jobTypeString);
      console.log('jobCategoryString arr :',jobCategoryString);

      console.log('latitude arr :',this.state.latitude1);
      console.log('logitute arr :',this.state.longitude1);

      var userid = await AsyncStorage.getItem('UserId');
      let dataobject =  JSON.stringify({
              user_id : userid,
              name : name,
              surname : sur_name,
              description : keyAchivement,
              gender: this.state.gender,
              dob: this.state.dob,
              country : this.state.country,
               postcode : post_code,
              it_skills : it_skill,
              lang_skills : language,
              state : this.state.state1,
              city: this.state.city,
              address : address,
              countrycode : this.state.Phone_code,
              phone_number : phone_no,
              hobbies : hobbies,
              live_postcode : post_code1,
              live_distance : redius_in_miles1,
              live_lat : this.state.latitude1,
              live_lng : this.state.longitude1,
              uni_postcode : post_code2,
              uni_distance : redius_in_miles2,
              uni_lat : this.state.latitude2,
              uni_lng : this.state.longitude2,
              open_location : locationCheck,
              mailing_status : mailingChk,
              job_type : jobTypeString,
              job_category : jobCategoryString
              
              });

                console.log('data object ',dataobject);
               this.setState({submitDisabled: true});
    
    fetch(baseUrl+'/mobile/api/edit_profile', {
        method: 'POST',
        headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
        },
      body: JSON.stringify({
        user_id : userid,
        name : name,
        surname : sur_name,
        description : keyAchivement,
        gender: this.state.gender,
        dob: this.state.dob,
        country : this.state.country,
         postcode : post_code,
        it_skills : it_skill,
        lang_skills : language,
        state : this.state.state1,
        city: this.state.city,
        address : address,
        countrycode : this.state.Phone_code,
        phone_number : phone_no,
        hobbies : hobbies,
        live_postcode : post_code1,
        live_distance : redius_in_miles1,
        live_lat : this.state.latitude1,
        live_lng : this.state.longitude1,
        uni_postcode : post_code2,
        uni_distance : redius_in_miles2,
        uni_lat : this.state.latitude2,
        uni_lng : this.state.longitude2,
        open_location : locationCheck,
        mailing_status : mailingChk,
          job_type : jobTypeString,
          job_category : jobCategoryString
        }),
    })
   .then((response) => {
       response.json().then(respond => {
        console.log(respond);
        this.setState({submitDisabled: false});
        if(respond.status == "1"){
              this.uploadImageApi(respond.message);
             }else {
            Toast.show({
              text: respond.message,
              type: "danger"
            });
      }
      
       })
     }).catch(error => {
      this.setState({submitDisabled: false});
      console.error(error);
    });






  }

  setDate(newDate) {
    this.setState({ chosenDate: newDate });
  }
    

    _onPressButton_login()
    {
      const {navigate} = this.props.navigation;
      navigate('Profile');
    }
    onValueChange2(value) {
      this.setState({
        selected2: value
      });
    }
   
    setDate(newDate) {
      // var d = new Date(newDate);
      // var curr_date = d.getDate();
      // var curr_month = d.getMonth() + 1; //Months are zero based
      // var curr_year = d.getFullYear();
      // this.setState({ dob : curr_date + "-" + curr_month + "-" + curr_year });
       this.setState({ dob : newDate });
    }


    changeCountry(value){
      if(value !== "Select")
            this.getApiState(value);       
        this.setState({ country : value })
    }
    onChangeState(value){
      if(value !== "Select")
            this.getApiCity(value);       
        this.setState({ state1 : value })
    }

    changecity(value){
      this.setState({city : value }) 
    }

    onChangeGender(value){
      this.setState({gender : value }) 
    }

    changePhoneCode(value){
      this.setState({ Phone_code : value }) 
    }

    onSelectedItemsChange = (selectedItems) => {
      console.log('category selected item : ',selectedItems);
      this.setState({ job_category_SelectedItems : selectedItems });
    };

    onSelectedJobTypeItemsChange  = (selectedItems) => {
  
      //console.log(selectedItems);
          var arrjobtype = this.state.jobTypeList;
          console.log('arrjobtype : ',arrjobtype)
          console.log('selected Items : ',arrjobtype)
          
            var internshipSeleted = false;
         
            var arrother = [];
            var arr = [];
       arrjobtype.map((arrItem)=> { 
         
        selectedItems.map((item)=> { 
          if(arrItem.id == item && arrItem.job_flag == "internship"){
                              if(internshipSeleted == false){
                                    internshipSeleted = true;       
                                   arr.push(item);
                                   
                              }
                  }
                  else{
                     if(arrItem.id == item){
                      arrother.push(arrItem.id);
                     }
                  
                  }
          console.log('selected item : ',item);
         });
        })
         
         console.log('internship selected : ',internshipSeleted);
        
            if(internshipSeleted){
                    console.log('arr push ',arr);
                    console.log('internship seleted');
                    this.setState({ job_type_SelectedItems : arr });
            }else{
              console.log('arrother : ',arrother);
              this.setState({ job_type_SelectedItems : arrother });
            }
           
      console.log('job type selected item : ',selectedItems);
   
    };


    getlocation1PostCode(postcode){
       if(postcode == "" || postcode.length < 7){
        this.setState({postcode1Error : false,  submitDisabled : false });
        return;
       }
       console.log(postcode);
        fetch(baseUrl+"/mobile/api/getLatLong", {
          method: 'POST',
           headers: new Headers({
             'Accept': 'application/json',
                'Content-Type': 'application/json',
           }),
          body: JSON.stringify({
            postcode: postcode
          })
          }).then(response => {
            response.json().then(respond => {
              console.log('post code  : ',respond);
                if(respond.status == "1"){
                     this.setState({ longitude1 : respond.data.longitude, latitude1 : respond.data.latitude,postcode1Error : false,post_code1_err : false, submitDisabled : false});
                }else {
                  this.setState({ postcode1Error : true, submitDisabled : true });
                }
            })
          })
          .catch(error => {
          console.error(error);
          });  
      

    }

    getlocation2PostCode(postcode){
      if(postcode == "" || postcode.length < 7 ) {
        this.setState({postcode2Error : false, submitDisabled : false });
        return;
       }
    
      fetch(baseUrl+"/mobile/api/getLatLong", {
        method: 'POST',
         headers: new Headers({
           'Accept': 'application/json',
              'Content-Type': 'application/json',
         }),
        body: JSON.stringify({
          postcode: postcode
        })
        }).then(response => {
          response.json().then(respond => {
            console.log('post code  : ',respond);
              if(respond.status == "1"){
                   this.setState({ longitude2 : respond.data.longitude, latitude2 : respond.data.latitude,postcode2Error : false,post_code2_err: false, submitDisabled : false});
              }else {
                this.setState({ postcode2Error : true, submitDisabled : true });
              }
          })
        })
        .catch(error => {
        console.error(error);
        });
      
    }

    handleChoosePhoto = () => {


      ImagePicker.openPicker({
        width: 300,
        height: 400,
        cropping: true
      }).then(response => {
        console.log(response);
        if (response.path) {
           var filename = uri.replace(/^.*[\\\/]/, '');
              this.setState({ photo: {uri : response.path,type : response.mime ,fileName:  filename} , img_url : response.path});
             }
      });

    };

    cameraImagePicker = ()=> {
      console.log('camera image picker ');
        this.setState({ imageSelectActionDialogVisible: false });
        ImagePicker.openCamera({
          width: 300,
          height: 400,
          cropping: true,
        }).then(response => {
          console.log(response);
          if (response.path != null) {
             var filename = response.path.replace(/^.*[\\\/]/, '');
                this.setState({ photo: {uri : response.path,type : response.mime ,fileName:  filename} , img_url : response.path});
               }
        }).catch(() => { console.log('cancel image camera') });
    }

    gallaryImagePicker = ()=> {
      console.log('gallary image picker ')
      this.setState({ imageSelectActionDialogVisible: false });
      ImagePicker.openPicker({
        width: 300,
        height: 400,
        cropping: true
      }).then(response => {
        console.log(response);
        
           var filename = response.path.replace(/^.*[\\\/]/, '');
        console.log(response);
              this.setState({ photo: {uri : response.path,type : response.mime ,fileName:  filename} , img_url : response.path});
           
      }).catch(() => { console.log('cancel image gallary') });
    }
    


    uploadImageApi = (message)=> {
      if(this.state.photo.uri != null){
        const formData = new FormData();
         const {uri , fileName , type} = this.state.photo;
         console.log('photo uri : ',uri);
         console.log('photo name : ',fileName);
         console.log('photo type : ',type);
        formData.append('file', {uri: uri,name: fileName,type: type});
        formData.append('user_id',this.state.user_id );
          console.log('image upload request ',formData);
        const options = {
          method: 'POST',
          headers: {
            'Content-type': 'multipart/form-data'
          },
          body: formData,
        };
  
        fetch(baseUrl+'/mobile/api/photo_upload', options)
        .then((res)=> {
              res.json().then((response)=> {
                    console.log('response : ',response);
                    if(response.status == "1"){
                      Toast.show({
                        text: message,
                        type: "success"
                      });
                   

                      const resetAction = StackActions.reset({
                        index: 0,
                        actions: [NavigationActions.navigate({ routeName: 'Candidate_profile1' })],
                      });
                      this.props.navigation.dispatch(resetAction);


                      //  this.props.navigation.state.params.getProfileData();
                    //  this.props.navigation.goBack();
                    }
              })
        }).catch((error)=> {
            console.log('error : ',error);
        })
      }else{
        console.log('response success upload image ');
        Toast.show({
          text: message,
          type: "success"
        });
      
        const resetAction = StackActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({ routeName: 'Candidate_profile1' })],
        });
        this.props.navigation.dispatch(resetAction);

      
        //  this.props.navigation.state.params.getProfileData();
        // this.props.navigation.goBack();
      }
     
    }




    onEditorInitialized() {
      this.setFocusHandlers();
      this.getHTML();
    }
  
    async getHTML() {
      const titleHtml = await this.richtext.getTitleHtml();
      const contentHtml = await this.richtext.getContentHtml();
      //  this.setState({ keyAchivement : contentHtml });
      // alert(titleHtml + ' ' + contentHtml)
    }
  
    setFocusHandlers() {
      this.richtext.setTitleFocusHandler(() => {
        //alert('title focus');
      });
      this.richtext.setContentFocusHandler(() => {
        //alert('content focus');
      });
    }

  
  


    render() {


      const { children, horizontalPercent = 1, verticalPercent = 1 } = this.props;
      const height = verticalPercent ? deviceHeight * verticalPercent : deviceHeight;
      const width = horizontalPercent ? deviceWidth * horizontalPercent : deviceWidth;

      leftIcon = require('../assest/left_arrow.png');



      eventHandler = (
        <TouchableOpacity style={{width:45,alignItems:"center"}} 
          onPress={() => {
            Alert.alert(
            "Confirm",
             "Going Back will Revert all Unsaved Changes, Do You Want to Continue?", 
             [
              {
                text: "Yes",
                onPress: () => this.props.navigation.navigate('Candidate_profile1')
              },
              {
                text: "No",
                
              }
            ]
            )
          }}
        hitSlop={{top:12, left:36, bottom:12,right:0}}>
          <Image source={leftIcon} style={{width:24,height:24}} />
        </TouchableOpacity>
      );
      
    //     console.log('this props edit profile : ',this.props);
      YellowBox.ignoreWarnings(['Warning: Async Storage has been extracted from react-native core','ListView is deprecated']);
      if(this.state.isRefresh){
        return <View style={{flex : 1,justifyContent : 'center'}}><ActivityIndicator size="large" color="#0000ff" /></View>;
      }else {
      return (
        <Container style={styles.container}>
          
          <ImageBackground style={{
            backgroundColor: "#2e2b48",
                width: null,
                zIndex:1,
                elevation: 10
                }}>

              <View style={{ backgroundColor: "trasparent", height: 70, width: '100%',  ...Platform.select({android:{height:height>800?70:60}}), }} >
                  <View
                      style={{
                        flexDirection: "row",
                        justifyContent: "space-between",
                        flex:1,
                        marginTop: Platform.OS == "ios" ? height>800?25:20: height>800?25:20, // only for IOS to give StatusBar Space
                        ...Platform.select({android:{marginTop:0}}),
                      }}>
                      <View style={{justifyContent:"center",marginLeft:15,width:"18%"}}>
                        {eventHandler}
                      </View>
                      <View style={{width:"82%",justifyContent:"center"}}>
                        <Text numberOfLines={1} style={{color:"#fff", fontSize:21,fontWeight:"bold",textAlign:"left",fontStyle: "normal"}}>
                          Edit Profile
                        </Text>
                      </View>
                  </View>

              </View>
            </ImageBackground>
            <Image source={require('../assest/login_bg.png')} style={styles.backgroundImage} />
           
           <Content style={styles.loginForm} enableResetScrollToCoords={false} disableKBDismissScroll={true}>
          
          <Form style={styles.form1}>
         


                <View style={{ flex: 1,  flexDirection: 'row',paddingTop : 20 }}>
                        {this.state.img_url && (
                          <Image
                            source={{ uri: this.state.img_url }}
                            style={{ width: 100, height: 100 ,borderRadius: 400/ 2 }}
                          />
                        )}
                        <View style={{ paddingLeft:20,justifyContent: 'center'}}>
                          <Button style={styles.btnchose}  onPress={()=> { this.setState({imageSelectActionDialogVisible : true});  }} ><Text>Choose Photo</Text></Button>
                        </View>
                      </View>


                      <View style={{ flex: 1,flexDirection: 'row'}}>
                        <View style={{ flex: 1,  marginTop:8,marginRight:5}}>
            <Item floatingLabel style={styles.labunder}>
            <Label style={{color : (this.state.name_err)?'red' : '#1f6fe7'}}>Name</Label>
            <Input style={styles.input} value={this.state.name} onChangeText={(name)=> this.setState({name})} />
            </Item>
            </View>
            <View style={{ flex: 1, marginTop:8,marginLeft:5}}>
            <Item floatingLabel style={styles.labunder}>
            <Label style={{color : (this.state.sur_name_err)?'red' : '#1f6fe7'}}>Surname</Label>
            <Input style={styles.input} value={this.state.sur_name} onChangeText={(sur_name)=> this.setState({sur_name})}/>
            </Item>
            </View>
            </View>

            <View style={{ flex: 1,flexDirection: 'row'}}>
             
                
                <Item style={[styles.labunder, {flex:1}]}>
                <Label style={{flex : 1, color : (this.state.dob_err)?'red' : '#1f6fe7' , fontSize : 15}}>Date of Birth</Label>
                <DatePicker
                    ref={(picker) => { this.datePickerdob = picker; }}
                    style={{flex : 2, }}
                    customStyles={{
                      placeholderText: {
                        color: '#1f6fe7',
                      },
                      dateInput:{borderWidth: 0, alignItems: "flex-start"}
                    }}
                    
                    textStyle={{ borderWidth:0}}
                    date={this.state.dob}
                    minDate="01-01-1970"
                    maxDate={new Date()}
                    placeholder="Select DOB"
                    format="DD-MM-YYYY"
                    locale={"en"}
                    timeZoneOffsetInMinutes={undefined}
                    modalTransparent={false}
                    animationType={"fade"}
                    androidMode={"default"}
                    onDateChange={this.setDate}
                    disabled={false}
                    confirmBtnText = {"Select"}
                    cancelBtnText = {"Cancel"}

                />  
                </Item>
              

           
              { /*
              <View style={{ flex: 1, marginLeft:5}}>
             
                <Item  style={styles.labunder}>
                  <Label style={{color : (this.state.gender_err)?'red' : '#1f6fe7'}}>Gender</Label> 

                  <View style={{ flex: 1}}>         
                    <Picker
                      mode="dropdown"
                      iosIcon={<Icon name="arrow-down" />}
                      style={{ paddingLeft: 50, color : (this.state.gender_err)?'red' : '#1f6fe7'}}
                      placeholder="Select your Gender"
                      placeholderStyle={{ color: "#bfc6ea" }}
                      placeholderIconColor="#0275d8"
                      selectedValue={this.state.gender}
                      onValueChange={this.onChangeGender.bind(this)}
                      >
                      <Picker.Item label="Gender" value="Select" />
                      <Picker.Item label="Male" value="Male" />
                      <Picker.Item label="Female" value="Female" />
                      </Picker>
                  </View>
              </Item>
               
            </View>
            */ }
             
          </View>
                        
            <View style={{ flex: 1, marginTop:8}}>
              <Item floatingLabel style={styles.labunder}>
            
                <Label style={{color : (this.state.email_err)?'red' : '#1f6fe7'}}>Email Address</Label>
                <Input editable={false} value={this.state.email} onChangeText={(email)=> this.setState({email})} />
                </Item>
            </View>


            <View style={{ flex: 1}}>
              <Text style={{paddingTop : 8,paddingLeft : 4, fontSize : 15 , color : (this.state.job_type_err)?'red' : '#1f6fe7' }}>Select Job Type</Text>
              <Item style={[styles.labunder]}>
              <ScrollView>
                <SectionedMultiSelect
                    items={ (typeof(this.state.jobTypeList) !=='undefined' ) ? this.state.jobTypeList : []}
                      uniqueKey="id"
                      displayKey="job_type"
                      selectText="Select Job Type"
                      showDropDowns={true}
                      //alwaysShowSelectText = {true}
                      onSelectedItemsChange={this.onSelectedJobTypeItemsChange}
                      selectedItems={ (typeof(this.state.job_type_SelectedItems) !=='undefined' ) ? this.state.job_type_SelectedItems : []}
                      //itemNumberOfLines ={2}
                      // ={2}
                      //showChips = {false}
                      styles={{
                        selectToggleText:{
                          width: "100%",
                          //backgroundColor : "red",
                        }, 
                        selectToggle : { 
                          flex : 1,
                          alignItems : "flex-end", 
                        },
                        chipsWrapper : {
                          opacity: 0,
                          height : 10,
                          
                        },
                        chipContainer : {
                          overflow: 'hidden',
                          opacity: 0,
                          height : 0,
                          backgroundColor : "green",
                        }
                        
                        //chipContainer: { overflow: 'hidden', }
                      }}
                     
          
                    />
                    </ScrollView>
              </Item>
            </View>
            
            

                  <View style={{ flex: 1,}}>
                    <Text style={{paddingTop : 8,paddingLeft : 4, fontSize : 15, color : (this.state.job_category_err)?'red' : '#1f6fe7' }}>Select Job Category</Text>
                    <Item  style={styles.labunder}>
                    <ScrollView>
                      <SectionedMultiSelect
                        items={ ( typeof(this.state.jobCategoryList) !== 'undefined' )? this.state.jobCategoryList : [] }
                        uniqueKey="id"
                        //style={{ height:20,}}
                        displayKey="job_category"
                        selectText="Select Job Category"
                        showDropDowns={true}
                        onSelectedItemsChange={this.onSelectedItemsChange}
                        selectedItems={this.state.job_category_SelectedItems}
                        //selectedItems = { ( (this.state.current_position) && (typeof this.state.current_position !== 'undefined') &&(this.state.current_position.length >0) )? this.state.current_position : []}
                        //selectedItems={ (global.position.length > 0) ? global.position : []}
                        styles={{
                          selectToggleText:{
                            width: "100%",
                          }, 
                          selectToggle : { 
                            flex : 1,
                            alignItems : "flex-end", 
                          },
                          chipsWrapper : {
                            opacity: 0,
                            height : 10,
                          },
                          chipContainer : {
                            opacity: 0,
                            height : 0,
                            backgroundColor : "green",
                          }
                        }}
                      />
                      </ScrollView>
                  </Item>
                </View>


        <View style={{ flex: 1, marginTop:8}}>
            <Item floatingLabel style={styles.labunder}>
              <Label style={{color : (this.state.it_skill_err)?'red' : '#1f6fe7'}}>IT Skills</Label>
              <Input value={this.state.it_skill} onChangeText={(it_skill)=> this.setState({it_skill})}/>
            </Item>
            </View>

            <View style={{ flex: 1, marginTop:8}}>
            <Item floatingLabel style={styles.labunder}>
              <Label style={{color : (this.state.hobbies_err)?'red' : '#1f6fe7'}}>Hobbies And Interest</Label>
            <Input value={this.state.hobbies} onChangeText={(hobbies)=> this.setState({hobbies})}/>
            </Item>
            </View>

            <View style={{ flex: 1, marginTop:8}}>
            <Item floatingLabel style={styles.labunder}>
              <Label style={{color : (this.state.language_err)?'red' : '#1f6fe7'}}>Language Skills</Label>
            <Input value={this.state.language} onChangeText={(language)=> this.setState({language})} />
            </Item>
            </View>
  
          <View  style={[styles.labunder, {flexDirection: 'column' }]}>
 
            <View style={{ flex: 1, marginTop:10,flexDirection:"row"}}>
                <Text  style={{color : (this.state.phone_no_err)?'red' : '#1f6fe7'}}> Phone No </Text> 
                
                <Tooltip
                  animated
                  isVisible={this.state.toolTipVisible}
                  content={<Text style={{ fontSize:11 }}>Please enter your mobile telephone number without the lead zero e.g. 07 would start with just 7.We will use your mobile telephone number to notify you of real-time job alerts.</Text>}
                  placement="top" 
                  onClose={() => this.setState({ toolTipVisible: false })}
                  tooltipStyle = {{ paddingLeft:15, width: "70%" }}
                  arrowSize = {{ width: 10, height: 8 }}
                  arrowStyle ={{marginLeft:5}}
                >
                <TouchableOpacity  onPress={() => {this.setState({ toolTipVisible: true }) }} style={{flexDirection:"row", paddingLeft : 10}}>
                  <Icon name="ios-information-circle"  style={{ fontSize:20 , color : '#000000'}} />
                </TouchableOpacity>
              </Tooltip>
               
              { /*
                <View style={{flexDirection:"row", paddingLeft : 5}}>
                  <TouchableOpacity onPress={() => {
                    this.props.navigation.navigate('VideoPlayer',{videoItem : item});
                    }}>
                    <View style={{flexDirection : "row"}}> 
                      <Icon name="ios-information-circle"  style={{ fontSize:20 , color : '#000000'}} />
                    </View>
                  </TouchableOpacity>
                </View>  
                  */ }     
           </View>

            <View style={{ flex: 1,flexDirection: 'row', marginTop:0,}}>
              <View style={{ flex: 5, marginRight:0,}}>
                  <Picker
                      //floatingLabel
                      mode="dropdown"
                      iosHeader = "Select Country Code"
                      iosIcon={<Icon name="ios-arrow-down"  style={{right: (Platform.OS == "ios") ? 30 : 0 }} />} 
                      style={{ width: (Platform.OS == "ios") ? '80%' : '110%' }}
                      placeholderStyle={{  maxWidth: '5%', }}
                      textStyle={{ maxWidth: (Platform.OS == "ios") ? '120%' : '100%', width:10, paddingLeft:10, }}

                      //iosIcon={<Icon name="ios-arrow-down"  style={{right:30 }} />} 
                      //style={{ width: '80%' }}
                      //placeholderStyle={{  maxWidth: '5%', }}
                      //textStyle={{ maxWidth: '120%', width:10, paddingLeft:10, }}

                      //textStyle={{fontSize: 7,color:'#012552',paddingRight:50}}  
                      placeholder="Select Country"
                      placeholderStyle={{ color: "#64a8e3" }}
                      placeholderIconColor="#007aff"
                      selectedValue={this.state.Phone_code}
                      onValueChange={this.changePhoneCode.bind(this)}
                    >
                      <Picker.Item label="Country" value="Select" />
                      {this.state.phoneCodeList.map((item,index)=> {
                          return (<Picker.Item style={{ paddingRight : 10, marginRight : 10, borderColor: "red", borderWidth:1 }} key={index} label={item.name+'(+'+item.phonecode+')'} value={item.phonecode} />)
                      })}
                    </Picker>
             
              </View>
              <View style={{ flex: 3, marginRight:0,}}>
                <Item>
                  <Input keyboardType="numeric" placeholder="Phone No" maxLength={15} value={this.state.phone_no} onChangeText={(phone_no)=> this.setState({phone_no})}/>
                </Item>
              </View>
            </View>
            
            </View>

            <Label style={{fontSize:18, paddingLeft:5, paddingTop:20, color:'#333',  fontWeight: 'bold',}}>Where do you currently reside?</Label>

            { /*
            <View style={{ flex: 1, marginTop:8}}>
              <Item floatingLabel style={styles.labunder}>
                <Label style={{color : (this.state.address_err)?'red' : '#1f6fe7'}}>Address</Label>
                <Input value={this.state.address} onChangeText={(address)=> this.setState({address})}/>
              </Item>
            </View>
            */ }

            {/* <Label style={{paddingLeft: 15,paddingTop: 20}}>Country/Region</Label> */}

            { /*
            <View style={{ flex: 1,}}>
              <Item style={styles.labunder}>
                
                <Item picker>
                  <Picker
                    floatingLabel
                    mode="dropdown"
                    iosIcon={<Icon name="arrow-down" />}
                    style={{ width: '80%' }}
                    placeholderStyle={{ maxWidth: '100%' }}
                    textStyle={{ maxWidth: '100%',paddingLeft:7 }}
                    placeholder="Select your Country "
                    placeholderStyle={{ color: "#64a8e3" }}
                    placeholderIconColor="#007aff"
                    selectedValue={this.state.country}
                    onValueChange={this.changeCountry.bind(this)}
                  >
                    <Picker.Item label="Your Country" value="Your Country" />
                    {this.state.countryList.map((item,index)=> {
                        return (<Picker.Item key={index} label={item.name} value={item.id} />)
                    })}
                    </Picker>
                </Item>
              </Item>
            </View>
                  */ }


            {/* <Label style={{paddingLeft: 15,paddingTop: 20}}>Country/State</Label> */}
            <View style={{ flex: 1,}}>
              <Item style={styles.labunder}>
                
              <Item picker>
                <Picker
                  floatingLabel
                  mode="dropdown"
                  iosIcon={<Icon name="arrow-down" />}
                  style={{ width: '80%' }}
                  placeholderStyle={{ maxWidth: '100%' }}
                  textStyle={{ maxWidth: '100%',paddingLeft:7 }}
                  placeholder="Select your County"
                  placeholderStyle={{ color: "#64a8e3" }}
                  placeholderIconColor="#007aff"
                  selectedValue={this.state.state1}
                  onValueChange={this.onChangeState.bind(this)}
                >
                <Picker.Item label="Your County" value="" />
                  {this.state.stateList.map((item,index)=> {
                      return (<Picker.Item key={index} label={item.name} value={item.id} />)
                  })}
                  </Picker>
              </Item>
              </Item>
            </View>

            {/* <Label style={{paddingLeft: 15,paddingTop: 20}}>State/city</Label> */}
            { /*
            <View style={{ flex: 1,}}>
              <Item style={styles.labunder}>
              
            <Item picker>
              <Picker
                floatingLabel
                mode="dropdown"
                iosIcon={<Icon name="arrow-down" />}
                style={{ width: '80%' }}
                placeholderStyle={{ maxWidth: '100%' }}
                textStyle={{ maxWidth: '100%',paddingLeft:7 }}
                placeholder="Select your City "
                placeholderStyle={{ color: "#64a8e3" }}
                placeholderIconColor="#007aff"
                selectedValue={this.state.city}
                onValueChange={this.changecity.bind(this)}
              >
                <Picker.Item label="Your City" value="Your City" />
                {this.state.cityList.map((item,index)=> {
                    return (<Picker.Item key={index} label={item.name} value={item.id} />)
                })}
                </Picker>
            </Item>
            </Item>
          </View>
              */ }

          { /*  
          <View style={{ flex: 1, marginTop:8}}>
            <Item floatingLabel style={styles.labunder}>
              <Label style={{color : (this.state.post_code_err)?'red' : '#1f6fe7'}}>Post Code</Label>
              <Input maxLength={8} value={this.state.post_code} onChangeText={(post_code)=> this.setState({post_code})}/>
            </Item>
          </View>
          */ }
            

            <View style={{ flex: 1, marginTop:8}}>
              <Label style={{color : '#1f6fe7',fontSize : 16}}>Key Achievements</Label>
              <Textarea rowSpan={5} bordered value={this.state.keyAchivement} onChangeText={(keyAchivement)=> this.setState({keyAchivement})} style={[styles.labunder, {textAlignVertical: "top"} ]} />
             
            </View>

              {/* <View style={{flex : 1}}>
           
            <RichTextEditor
              ref={(r)=>this.richtext = r}
              style={styles.richText}
              initialTitleHTML={'Key Arhivement'}
              initialContentHTML={this.state.keyAchivement}
              editorInitializedCallback={() => this.onEditorInitialized()}
             />
                <RichTextToolbar
                  getEditor={() => this.richtext}
                />
          </View> */}



            <Label style={{fontSize:18,paddingLeft:2, paddingTop:20, color:'#333',  fontWeight: 'bold',}}>Where Would You Like To Work?</Label>

              <View style={{paddingTop: 10 ,flexDirection: 'row'}} > 
              
                <Label style={{paddingTop: 3}}>Open To Location</Label>
                <CheckBox
                    checked={this.state.openLocationChecked}
                    style={{marginTop: 5}}
                    onPress={()=>{
                      this.setState({
                            openLocationChecked: !this.state.openLocationChecked 
                    });
                    }}
                    //value={this.state.openLocationChecked}
                />
              </View>

              <View style={{paddingTop : 10}}>
                <Label>Location Sought 1</Label>     
              </View>

              <View style={{flex: 1, paddingTop: 10 ,flexDirection: 'row'}} > 
                
                 
                  <View style={{ flex: 1,marginRight:5,marginTop:8}}>
                    <Item floatingLabel style={styles.labunder}>
                    <Label style={{color : (this.state.post_code1_err || this.state.postcode1Error)?'red' : (this.state.openLocationChecked)? 'rgba(31,111,231,0.5)' :'#1f6fe7'}}>Postcode </Label>
                    <Input  maxLength={8} editable={!this.state.openLocationChecked} value={this.state.post_code1} onChangeText={(post_code1)=> {this.setState({post_code1}); this.getlocation1PostCode(post_code1); }}/>
                    </Item>
                  </View>
              
                  <View style={{ flex: 1,marginRight:5,marginTop:8}}>
                    <Item floatingLabel style={styles.labunder}>
                      <Label style={{color : (this.state.redius_in_miles1_err)?'red' :(this.state.openLocationChecked)? 'rgba(31,111,231,0.5)' :'#1f6fe7'}}>Radius In Miles</Label>
                      <Input keyboardType="numeric" editable={!this.state.openLocationChecked} value={this.state.redius_in_miles1} onChangeText={(redius_in_miles1)=> this.setState({redius_in_miles1})}/>
                    </Item>
                  </View>

                
            </View>

            <View style={{paddingTop : 10}}>
              <Label>Location Sought 2</Label>
            </View>      

            <View style={{ flex: 1,flexDirection: 'row'}}>
              <View style={{ flex: 1, marginRight:5,marginTop:5}}>
                <Item floatingLabel style={styles.labunder}>
                <Label style={{color : (this.state.post_code2_err || this.state.postcode2Error)?'red' : (this.state.openLocationChecked)? 'rgba(31,111,231,0.5)' :'#1f6fe7'}}>Postcode </Label>
                <Input maxLength={8} editable={!this.state.openLocationChecked} value={this.state.post_code2} onChangeText={(post_code2)=> {this.setState({post_code2}); this.getlocation2PostCode(post_code2);  }} />
              
                </Item>
              </View>

              <View style={{ flex: 1, marginRight:5,marginTop:5}}>
                <Item floatingLabel style={styles.labunder}>
                <Label style={{color : (this.state.redius_in_miles2_err)?'red' : (this.state.openLocationChecked)? 'rgba(31,111,231,0.5)' :'#1f6fe7'}}>Radius In Miles</Label>
                <Input keyboardType="numeric" editable={!this.state.openLocationChecked} value={this.state.redius_in_miles2} onChangeText={(redius_in_miles2)=> this.setState({redius_in_miles2})}/>
                </Item>
              </View>

            </View>

            {/* <Label>Valid</Label> */}
            <View style={{paddingTop: 10 ,flexDirection: 'row'}} > 
              <CheckBox
                  checked={this.state.mailingListChecked}
                  //style={{paddingRight: 10}}
                  onPress={()=>{
                    this.setState({
                      mailingListChecked: !this.state.mailingListChecked 
                  });
                  }}
                  value={this.state.mailingListChecked}
              />
              <Label style={{marginLeft:15}}>Add me to your mailing list</Label>
            </View>
            


            <Button block disabled={this.state.submitDisabled}
            onPress={()=> { this.editprofile(); }}
            style={{marginTop: 20, marginBottom:80}}>
            <Text>SUBMIT</Text>
          </Button>

          <Dialog
                    visible={this.state.imageSelectActionDialogVisible}
                    title="Select Action"
                    onTouchOutside={() => this.setState({imageSelectActionDialogVisible: false})} >
                    <View>
                          <TouchableOpacity style={{margin: 5}} onPress={this.cameraImagePicker}>
                           <Text style={{textAlign : 'center',fontSize: 16,padding: 5,color : '#1f6fe7'}}>Take from Camera</Text>
                          </TouchableOpacity> 
                          <TouchableOpacity style={{padding: 10,marginTop: 10}} onPress={this.gallaryImagePicker}>
                            <Text style={{textAlign : 'center',fontSize: 16,color : '#1f6fe7'}}>Take from Gallery</Text>
                          </TouchableOpacity> 
                          <TouchableOpacity style={{padding: 10,marginTop: 10}} onPress={() => this.setState({imageSelectActionDialogVisible: false})}>
                            <Text style={{textAlign : 'center',fontSize: 16,color : '#1f6fe7'}}>Cancel</Text>
                          </TouchableOpacity> 
                    </View>
                </Dialog>
          </Form>
             </Content>
           
      </Container>
      );
        }
    }
}
const styles = StyleSheet.create({
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover', // or 'stretch'
  },
  loginForm: {
    flex: 1,
    position: 'absolute',
    backgroundColor: 'transparent', width: '90%', height: '100%',
    paddingTop:"18%"
  },
  container: {

    flex: 1,
    backgroundColor: 'transparent',
    alignItems: 'center', justifyContent: 'center',



  },


  loginContainer: {
    alignItems: 'center',
    flexGrow: 1,
    justifyContent: 'center'
  },
  form1:{ flex: 1, paddingLeft:0 },
  btnchose: { backgroundColor: '#1f6fe7', fontSize:14,textAlign: 'center', borderRadius: 5, height: 35,},
  input:{
    color: '#333',
  },
  richText: {
    width: '100%',
    height: 200,
    alignItems:'center',
    justifyContent: 'center',
    backgroundColor: 'transparent',
  },
  labunder:{flex: 1,  flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: '#017bc2', marginBottom:5,marginTop: 10}
})

export default EditProfile;
