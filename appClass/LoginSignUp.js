import React from "react";
import {
    View,
    Text,
    TouchableOpacity,
    Animated,
    ScrollView,
    Image,
    StyleSheet,
    Dimensions,YellowBox,Alert,BackHandler
} from "react-native";
import { Container, Header, Content, Form, Item, Input, Label,Button,Icon,Thumbnail,CheckBox } from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';
const {baseUrl} = require('./baseurl/Url');
const { width } = Dimensions.get("window");

export default class LoginSignUp extends React.Component {

    static navigationOptions = {
        header : null
      };

       constructor(props){
           super(props);
           this.state = {
            active: 0,
            xTabOne: 0,
            xTabTwo: 0,
            translateX: new Animated.Value(0),
            translateXTabOne: new Animated.Value(0),
            translateXTabTwo: new Animated.Value(width),
            translateY: -1000,
            rememberLoginCheck : false,
            mailListSignUpCheck : false,
            iAgreeSignUpCheck : false,
            iagreeCheck : false,
            
            Password : '',
            Email : '',
            emailerr : false,
            passerr : false,
            emailErrSignup : false,
            emailSignUp : '',
            passErrSignup : false,
            passwordSignUp : '',
            conPassErrSignup : false,
            conPasswordSignUp : '',
            behavior: 'position' 
        };
           this.login = this.login.bind(this);
           this.signUp = this.signUp.bind(this);
           this._storeData = this._storeData.bind(this);
       }



       componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
      }
      
      componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
      }
    
      handleBackButton = () => {
        Alert.alert(
            'Gradhire',
            'Exiting the application?', [{
                text: 'Cancel',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel'
            }, {
                text: 'OK',
                onPress: () => BackHandler.exitApp()
            }, ], {
                cancelable: false
            }
         )
         return true;
       } 


       

    _storeData = async (id,email) => {
        try {
          await AsyncStorage.setItem('UserId', id);
          await AsyncStorage.setItem('Emailid', email);
             const {navigate} = this.props.navigation;
                navigate('Candidate_profile1');
        } catch (error) {
          console.error(error);
        }
      };



      login = ()=> {
        var email = this.state.Email.replace(/ /g,'');
        console.log("email : 111,"+email+",adsfasdf");
        var password = this.state.Password.replace(/ /g,'');
       //  var pass = this.state.Password.trim;
       //  console.log('email : ',pass);
       if (email == '') {
           this.setState({
             emailerr : true,
             });
           return;
           }
           console.log('email ',email)
           this.setState({ emailerr : false });
           var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
           if (!pattern.test(email)) {
             this.setState({
                   emailerr : true,
             });
             console.log('email pattern invalid : ',email);
             return;
           }       
           this.setState({ emailerr : false });
           if (password == '') {
               this.setState({
                 passerr : true,
                 });
               return;
               }
            this.setState({ passerr : false });
          
            fetch(baseUrl+"/mobile/auth/login", {
   method: 'POST',
    headers: new Headers({
      'Accept': 'application/json',
         'Content-Type': 'application/json',
    }),
   body: JSON.stringify({
     identity: email,
     password: password,
   })
})
.then(response => {
  response.json().then(respond => {
    console.log(respond);
      if(respond.status == "1"){
        this._storeData(respond.user_id,email);
        this.setState({ Email : '', Password : '',rememberLoginCheck : false })      
      }
      else 
        Alert.alert('Gradhire',respond.message);  

  })
})
.catch(error => {
 console.error(error);
});
   }

   // SignUp Fetch Api

   signUp = ()=> {
              
               var emailSignUp = this.state.emailSignUp.replace(/ /g,'');
               var passwordSignUp  = this.state.passwordSignUp.replace(/ /g,'');
               var conPasswordSignUp = this.state.conPasswordSignUp.replace(/ /g,'');
               
       
         if (emailSignUp == '') {
           this.setState({
             emailErrSignup : true,
             });
           return;
           }
           var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
           if (!pattern.test(emailSignUp)) {
             this.setState({
               emailErrSignup : true,
             });
             return;
           }      
           
         this.setState({  emailErrSignup : false });
         if (passwordSignUp == '') {
           this.setState({
             passErrSignup : true,
             });
           return;
           }
         this.setState({ passErrSignup : false});
         if (passwordSignUp < 8 && passwordSignUp ) {
            this.setState({
              passErrSignup : true,
              });
            return;
            }
          this.setState({ passErrSignup : false});
         if (conPasswordSignUp == '') {
           this.setState({
             conPassErrSignup : true,
             });
           return;
           }
         this.setState({ conPassErrSignup : false});
         if (conPasswordSignUp != passwordSignUp) {
           this.setState({
              passErrSignup : true,  
             conPassErrSignup : true
             });
           return;
           }
         this.setState({ passErrSignup : false,  
           conPassErrSignup : false }); 
       
           if (!this.state.rememberSign_up_oneCheck) {
                   this.setState({ iagreeCheck : true});
               return;
               }
               this.setState({ iagreeCheck : false });  
            var mailing_stat = (this.state.mailListSignUpCheck)? "1" : "0";
           fetch(baseUrl+'/mobile/auth/create_user', {
               method: 'POST',
                headers: {
                  Accept: 'application/json',
                  'Content-Type': 'application/x-www-form-urlencoded',
                },
               body: JSON.stringify({
                 register_as: '3', 
                 email: emailSignUp,
                 password: passwordSignUp,
                 confirm_pass: conPasswordSignUp,
                 mailing_status : mailing_stat
                 }),
             })
             .then((response) => {
                // console.log('res : ',response);
                  response.json().then(respond => {
                    console.log(respond);
                     if(respond.status == "1"){
                         this.setState({ userNameSignUp : '', emailSignUp : '',passwordSignUp : '', conPasswordSignUp : '',rememberSign_up_oneCheck : false, mailListSignUpCheck : false})
                     }
                       Alert.alert('Gradhire',respond.message);
                   })
               // console.log(res);
               })
               .catch(error => {
                 console.error(error);
               });

   }




   handleSlide = type => {
    let {
        active,
        xTabOne,
        xTabTwo,
        translateX,
        translateXTabOne,
        translateXTabTwo
    } = this.state;
    Animated.spring(translateX, {
        toValue: type,
        duration: 100
    }).start();
    if (active === 0) {
        Animated.parallel([
            Animated.spring(translateXTabOne, {
                toValue: 0,
                duration: 100
            }).start(),
            Animated.spring(translateXTabTwo, {
                toValue: width,
                duration: 100
            }).start()
        ]);
    } else {
        Animated.parallel([
            Animated.spring(translateXTabOne, {
                toValue: -width,
                duration: 100
            }).start(),
            Animated.spring(translateXTabTwo, {
                toValue: 0,
                duration: 100
            }).start()
        ]);
    }
};

    render() {
       
        let {
            xTabOne,
            xTabTwo,
            translateX,
            active,
            translateXTabOne,
            translateXTabTwo,
            translateY
        } = this.state;
        YellowBox.ignoreWarnings(['Warning: Async Storage has been extracted from react-native core']);
        return (
          
            <View style={{ flex: 1 }}>
            <View
                style={{
                    width: "90%",
                    marginLeft: "auto",
                    marginRight: "auto"
                }}
            >
                <View
                    style={{
                        flexDirection: "row",
                        marginTop: 40,
                        marginBottom: 20,
                        height: 36,
                        position: "relative"
                    }}
                >
                   <Animated.View
                            style={{
                                position: "absolute",
                                width: "50%",
                                height: "100%",
                                top: 0,
                                left: 0,
                                backgroundColor: "#007aff",
                                borderRadius: 4,
                                transform: [
                                    {
                                        translateX
                                    }
                                ]
                            }}
                        />
                        <TouchableOpacity
                            style={{
                                flex: 1,
                                justifyContent: "center",
                                alignItems: "center",
                                borderWidth: 1,
                                borderColor: "#007aff",
                                borderRadius: 4,
                                borderRightWidth: 0,
                                borderTopRightRadius: 0,
                                borderBottomRightRadius: 0
                            }}
                            onLayout={event =>
                                this.setState({
                                    xTabOne: event.nativeEvent.layout.x
                                })
                            }
                            onPress={() =>
                                this.setState({ active: 0 }, () =>
                                    this.handleSlide(xTabOne)
                                )
                            }
                        >
                            <Text
                                style={{
                                    color: active === 0 ? "#fff" : "#007aff"
                                }}
                            >
                                Sign In
                            </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={{
                                flex: 1,
                                justifyContent: "center",
                                alignItems: "center",
                                borderWidth: 1,
                                borderColor: "#007aff",
                                borderRadius: 4,
                                borderLeftWidth: 0,
                                borderTopLeftRadius: 0,
                                borderBottomLeftRadius: 0
                            }}
                            onLayout={event =>
                                this.setState({
                                    xTabTwo: event.nativeEvent.layout.x
                                })
                            }
                            onPress={() =>
                                this.setState({ active: 1 }, () =>
                                    this.handleSlide(xTabTwo)
                                )
                            }
                        >
                            <Text
                                style={{
                                    color: active === 1 ? "#fff" : "#007aff"
                                }}
                            >
                                Sign Up
                            </Text>
                        </TouchableOpacity>
                    </View>
                    
                    <ScrollView>
                        <Animated.View
                            style={{
                                justifyContent: "center",
                                alignItems: "center",
                                transform: [
                                    {
                                        translateX: translateXTabOne
                                    }
                                ]
                            }}
                            onLayout={event =>
                                this.setState({
                                    translateY: event.nativeEvent.layout.height
                                })
                            }
                        >
                            
                            <Text style={{color: '#0275d8',fontSize:24, textAlign: "center", paddingTop: 10,textTransform: 'uppercase'}}>Sign In</Text>
                            <Text style={{textAlign: "center", paddingTop: 10}}>To Continue,Please Enter Details</Text>
                            <Item floatingLabel >
                            <Label style={{color : (this.state.emailerr)? 'red':'#0275d8' }}>Username</Label>
                            <Thumbnail resizeMode="contain" small square  source={require('../assest/mail.png')} />
                            <Input  style={styles.input} value={this.state.Email} onChangeText={(text)=> this.setState({
                 Email : text })}/>
                            </Item>
                            
                            <Item floatingLabel >
                            <Label style={{color : (this.state.passerr)? 'red':'#0275d8' }}>Password</Label>
                            <Thumbnail resizeMode="contain" small square  source={require('../assest/password_icon.png')} />
          
                            <Input secureTextEntry={true} style={styles.input} value={this.state.Password} onChangeText={(Password)=> this.setState({
                 Password})}/>
                            </Item>
                          
                      

                            <View style={{ flexDirection : 'row', flex : 1,margin : 5}}>
                            <CheckBox 
                            checked={this.state.rememberLoginCheck}
                            onPress={()=> { console.log('Checked box'); 
                            this.setState(preState=>({rememberLoginCheck : !preState.rememberLoginCheck}))}} />
                          <Label style={{paddingLeft:10}} >Remembar Password</Label>
                            </View>

                           <Button block 
                          style={{borderRadius:5,color: '#fff',backgroundColor:'#1f6fe7',}}
                          onPress={()=> {console.log('hi');
                          // let ty =
                          this.login(); 
            
                          }}
                          >
                             <Text>Sign In</Text>
                          </Button>

                            <TouchableOpacity onPress={()=> { this.props.navigation.navigate('Forget'); }}>
                                <View style={{justifyContent: 'center',alignItems: 'center'}}>
                                <Text style={{ padding: 8 ,marginTop : 10, fontSize: 14,color : '#0275d8'}}>Forget Password?</Text>
                                </View>
                            </TouchableOpacity>
                             
                        </Animated.View>
                        
                        <Animated.View
                            style={{
                                justifyContent: "center",
                                alignItems: "center",
                                transform: [
                                    {
                                        translateX: translateXTabTwo
                                    },
                                    {
                                        translateY: -translateY
                                    }
                                ]
                            }}
                        >
                            
                          
                           
                            <Text style={{color: '#0275d8',fontSize:24, textAlign: "center", paddingTop: 10,textTransform: 'uppercase'}}>Sign Up</Text>
                            <Text style={{textAlign: "center", paddingTop: 10}}>To Continue,Please Enter Details</Text>
                            <Item floatingLabel >
                            <Label style={{color : (this.state.emailErrSignup)? 'red':'#0275d8' }}>Email</Label>
                            <Thumbnail resizeMode="contain" small square  source={require('../assest/mail.png')} />
              <Input  style={styles.input} value={this.state.emailSignUp} onChangeText={(email)=> this.setState({emailSignUp : email})}/>
              </Item>
            
              <Item floatingLabel >
                            <Label style={{color : (this.state.passErrSignup)? 'red':'#0275d8' }}>Password</Label>
                            <Thumbnail resizeMode="contain" small square  source={require('../assest/password_icon.png')} />
              <Input secureTextEntry={true} style={styles.input}  value={this.state.passwordSignUp} onChangeText={(Password)=> this.setState({passwordSignUp : Password})}/>
              </Item>
            
              <Item floatingLabel >
                <Label style={{color : (this.state.conPassErrSignup)? 'red':'#0275d8' }}>Confirm Password</Label>
                <Thumbnail resizeMode="contain" small square  source={require('../assest/password_icon.png')} />
              <Input secureTextEntry={true} style={styles.input} value={this.state.conPasswordSignUp} onChangeText={(ConnPassword)=> this.setState({conPasswordSignUp :ConnPassword})}/>
              </Item>

              <View style={{ flexDirection : 'row', flex : 1,margin : 5}}>
                            <CheckBox 
                            checked={this.state.rememberSign_up_oneCheck}
                            onPress={()=> { console.log('Checked box'); 
                            this.setState(preState=>({rememberSign_up_oneCheck : !preState.rememberSign_up_oneCheck}))}} />
                            <Label style={{paddingLeft:10 ,color : (this.state.iagreeCheck)? "red": "#333"}} >I agree with the Terms of Use</Label>
                            </View>
                            <View style={{ flexDirection : 'row', flex : 1,margin : 5}}>
                            <CheckBox 
                            checked={this.state.rememberSign_up_twoCheck}
                            onPress={()=> { console.log('Checked box'); 
                            this.setState(preState=>({rememberSign_up_twoCheck : !preState.rememberSign_up_twoCheck}))}} />
                            <Label style={{paddingLeft:10,color : "#333"}} >Add me to your mailing list</Label>
                            </View>
            
            

              <Button block
            style={{marginTop: 20,borderRadius:5,backgroundColor:'#1f6fe7',}}
            onPress={()=> {console.log('hi');
            // let ty =
             this.signUp(); 
            
            }}
            
            >
            <Text style={{color: '#fff', textAlign: "center", paddingTop:5, paddingBottom:5, textTransform: 'uppercase'}}>SUBMIT</Text>
          </Button>
                      
                        </Animated.View>
                        </ScrollView>
                        
            
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
  
    log2: {  
      height:'100%', 
      alignItems: 'center',
      },
    
  loginContainer:{
    alignItems: 'center',
    flexGrow: 1,
    justifyContent: 'center', backgroundColor:'#333'
},
logo: {
    alignItems: 'center',
    width: '100%', marginTop:40,
      },
loginForm:{
            width:'100%',
            height:'15%',
          },
container:{
     width:'94%',
     backgroundColor:'transparent',
     alignItems: 'center',
     flexGrow: 1,
     justifyContent: 'center',
     height:'15%',
    },
input:{
    fontSize: 16,
    color: 'black',
    paddingTop:10,
    borderBottomColor: '#1f6fe7',
    borderBottomWidth: 1, marginBottom:8,
      },
texth1:{
    fontWeight:'bold'
       },

texth2:{
    color:'#1f6fe7'
       },
carditn:{
     paddingTop: 5,
     paddingBottom: 5,
     paddingLeft: 10 ,
     color: "#0275d8", 
     borderRadius:10,
     width:'100%'
    },

  })
  