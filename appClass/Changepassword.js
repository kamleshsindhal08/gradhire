import React, { Component } from 'react';
import { Image,TouchableOpacity,StyleSheet} from 'react-native';
import { Container, Header, Content, View,Form, Item, Input, Label,Button,Text,Icon, Card,Spinner, CardItem, Thumbnail, Left, Body, Right,Toast} from 'native-base';
import { baseUrl } from './baseurl/Url';
import AsyncStorage from '@react-native-community/async-storage';

class Changepassword extends React.Component {

   
  
  constructor(props) {
    super(props);
  
     this.state = {
      
      oldpassword : '',
      newpassword : '',
      reenterpassword: '',
      oldpassworderr : false,
      newpassworderr : false,
      reenterpassworderr : false
     };
     this.Changepassword = this.Changepassword.bind(this);
  } 
  
  
  static navigationOptions = {
    title: 'Change Password',
    };

  //   componentDidMount(){
  //     const { email } = this.props.navigation.state.params;

    Changepassword = async()=>{
      var oldpassword  = this.state.oldpassword.trim();
      var newpassword  = this.state.newpassword.trim();
      var reenterpassword = this.state.reenterpassword.trim();


      if(oldpassword == ""){
        console.log('oldpassword : ',);
        this.setState({ oldpassworderr : true});   
        Toast.show({
          text: 'Enter Old Password',
          type: "danger"
        });
      
        return; 
      }
      this.setState({ oldpassworderr : false});  


      if(newpassword == ""){
        console.log('newpassword : ',);
        this.setState({ newpassworderr : true});   
        Toast.show({
          text: 'Enter New Password',
          type: "danger"
        });
        return; 
      }
      this.setState({ newpassworderr : false});  


      if(reenterpassword == ""){
        console.log('reenterpassword : ',);
        this.setState({ reenterpassworderr : true});  
        Toast.show({
          text: 'Enter Confirm Password',
          type: "danger"
        }); 
        return; 
      }
      this.setState({ reenterpassworderr : false});
      if(reenterpassword !== newpassword){
        console.log('reenterpassword : ',);
        this.setState({ reenterpassworderr : true,newpassworderr : true});  
        Toast.show({
          text: 'Enter Confirm Password',
          type: "danger"
        }); 
        return; 
      }
      this.setState({ reenterpassworderr : false,newpassworderr : false});  
     
        const email = await AsyncStorage.getItem('Emailid');
    
        fetch(baseUrl+'/mobile/Auth/change_password', {
          method: 'POST',
          headers: {
                      Accept: 'application/json',
                     'Content-Type': 'application/json',
                  },
          body: JSON.stringify({
            email : email,
            old : oldpassword,
            new  : newpassword,
            new_confirm : reenterpassword,
          }),
      })
     .then((response) => {
         response.json().then(respond => {
           console.log('update contact details ',respond);
                    if(respond.status == "1"){
                      this.props.navigation.goBack();
                    Toast.show({
                      text: respond.message,
                      type: "success"
                    });
                  }else {
                    Toast.show({
                      text: respond.message,
                      type: "danger"
                    });
                  }
         })
       }).catch(error => {
        console.error(error);
      });

          
    
  //   }
   }




    render() {

        
      return (

        <Container style={styles.container}>
         <Image source={require('../assest/login_bg.png')} style={styles.backgroundImage} />
        <Content style={ styles.loginForm }>
          <Form>
          <Image resizeMode="contain" style={styles.logo} source={require('../assest/logo.png')} />
            
          

          <Text style={{color: '#0275d8', textAlign: "center", paddingTop: 10,textTransform: 'uppercase'}}>Contact Details</Text>
          <Text style={{textAlign: "center", paddingTop: 10}}>Please Enter Details</Text>
         
            <Item floatingLabel style={styles.input}>
            <Label style={{color : (this.state.oldpassworderr)? 'red':'#0275d8' }}>Old Password</Label>
              <Thumbnail resizeMode="contain" small square style={styles.mailicon} source={require('../assest/mail.png')} />
              <Input secureTextEntry={true} style={styles.input} value={this.state.oldpassword} onChangeText={(text)=> this.setState({
                 oldpassword : text })}/>
            </Item>
                      
           
           



           
            <Item floatingLabel style={styles.input}>
              <Label style={{color : (this.state.newpassworderr)? 'red':'#0275d8' }}>New Password</Label>
              <Thumbnail resizeMode="contain" small square style={styles.mailicon} source={require('../assest/password_icon.png')} />
          
              <Input secureTextEntry={true}  style={styles.input} value={this.state.newpassword} onChangeText={(text)=> this.setState({
                 newpassword : text})}/>
            </Item>
            
            <Item floatingLabel style={styles.input}>
            <Label style={{color : (this.state.reenterpassworderr)? 'red':'#0275d8' }}>Re Enter Password</Label>
              <Thumbnail resizeMode="contain" small square style={styles.mailicon} source={require('../assest/password_icon.png')} />
              <Input secureTextEntry={true} style={styles.input} value={this.state.reenterpassword} onChangeText={(text)=> this.setState({
                 reenterpassword : text })}/>
            </Item>
           
         
            <Button block 
            style={{marginTop: 20,marginLeft:20,marginRight:20,borderRadius:5}}
             onPress={()=> {console.log('hi');
            // let ty =
             this.Changepassword(); 
            
            }}
             >
            <Text>Submit</Text>
          </Button>
          </Form>

          
          
             </Content>
           
      </Container>
      );
    }
}

const styles = StyleSheet.create({

  backgroundImage: {
    flex: 1,
    resizeMode: 'cover', // or 'stretch'
},
loginForm: {
  flex: 1,
    position: 'absolute',
   
    backgroundColor: 'transparent', paddingTop:40, width:'90%', height:null, 
},
  container: {
    
      flex: 1,
      backgroundColor: 'transparent',
      alignItems: 'center',justifyContent: 'center',
      
      
      
  },
  mailicon: {
    height: 10,
    width: 20,
  
}, 

  loginContainer:{
      alignItems: 'center',
      flexGrow: 1,
      justifyContent: 'center'
  },
  logo: {
    flex: 1,
    alignItems: 'center',
      width: 300,
      height: 100,
     justifyContent: 'center',marginLeft:50,
      
  },
  input:{
    fontSize: 16,
    
    color: 'black',
    marginBottom:10, paddingTop:20,
    borderBottomColor: '#1f6fe7',
    borderBottomWidth: 1
    
  }

})

export default Changepassword;

