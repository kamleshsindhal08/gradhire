import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {NavigationActions,StackActions} from 'react-navigation';
import { Image,TouchableOpacity,TouchableHighlight,YellowBox,BackHandler,StyleSheet,ActivityIndicator, ImageBackground, Platform, Dimensions, Alert, ScrollView } from 'react-native';

import ImagePicker from 'react-native-image-crop-picker';
import { Container,Picker, CheckBox,Header, Content, Form, Item, Input,Textarea, Label,Button,Text, Card, CardItem,  Thumbnail, Icon, Left, Body, Right, View,Toast, Row} from 'native-base';
import DatePicker from 'react-native-datepicker'
import AsyncStorage from '@react-native-community/async-storage';
import SectionedMultiSelect from 'react-native-sectioned-multi-select';
import { Dialog } from 'react-native-simple-dialogs';
import HTML from 'react-native-render-html';
import Tooltip from 'react-native-walkthrough-tooltip';
const {baseUrl} = require('./baseurl/Url');
//import {RichTextEditor, RichTextToolbar} from 'react-native-zss-rich-text-editor';

const { height: deviceHeight, width: deviceWidth } = Dimensions.get('window');

class EditProfile extends Component {


  static navigationOptions = {
    title : 'Edit Profile',
    headerStyle: { backgroundColor: '#2e2b48' },
    headerTintColor: 'white',
    headerLeft: <Button title="Save" onPress={() => state.params.handleSave()} />,
  };

  static propTypes = {
    children: PropTypes.any,
    horizontalPercent: PropTypes.number,
    verticalPercent: PropTypes.number,
  };

  constructor(props) {
    super(props);
    this.state = {
      name:'',
      sur_name:'',
      dob:'',
      gender:'',
      email:'',
      job_type:'',
      job_function:'',
      it_skill:'',
      hobbies:'',
      language:'',
      Phone_code:'',
      phone_no:'',
      country:'',
      state1:'',
      city:'',
      post_code:'',
      address:'',
      keyAchivement:'',
      open_to_location:'',
      post_code1:'',
      redius_in_miles1:'',
      post_code2:'',
      redius_in_miles2:'',
      latitude1: '',
      longitude1 : '',
      latitude2: '',
      longitude2 : '',
      openLocationChecked : true,
      mailingListChecked : false,
      chosenDate: new Date(),
      photo: {},
      user_id : '',
      img_url : null,
      selectedTag : 'body',
      selectedStyles : [],
      

      countryList: [],
      stateList : [],
      cityList : [],
      phoneCodeList : [],
      job_category_SelectedItem: [],
      job_category_selected : [],
      job_type_SelectedItems: [],
      job_type_selected : [],
      jobCategoryList: [],
      jobTypeList : [],

      name_err:  false,
      sur_name_err:  false,
      gender_err:false,
      email_err:false,
      job_type_err:false,
      job_function_err:false,
      it_skill_err:false,
      hobbies_err:false,
      language_err:false,
      Phone_code_err:false,
      phone_no_err:false,
      country_err:false,
      state_err1:false,
      city_err:false,
      post_code_err:false,
      address_err:false,
      keyAchivement_err:false,
      open_to_location_err:false,
      post_code1_err:false,
      redius_in_miles1_err:false,
      post_code2_err:false,
      redius_in_miles2_err:false,
      postcode1Error : false,
      postcode2Error : false,
      isRefresh : false,
      selected2: '',
      imageSelectActionDialogVisible : false,
      submitDisabled: false,
      toolTipVisible : false,
    };


    this.setDate = this.setDate.bind(this);
    this.defualtApis = this.defualtApis.bind(this);
    this.getApiState = this.getApiState.bind(this);
    this.getApiCity = this.getApiCity.bind(this);
    this.getlocation1PostCode = this.getlocation1PostCode.bind(this);
    this.getlocation2PostCode = this.getlocation2PostCode.bind(this);
    this.mulitSelectApis = this.mulitSelectApis.bind(this);
    this.uploadImageApi = this.uploadImageApi.bind(this);
    this.handleChoosePhoto = this.handleChoosePhoto.bind(this);
    this.cameraImagePicker = this.cameraImagePicker.bind(this);
    this.gallaryImagePicker = this.gallaryImagePicker.bind(this);
    this.getHTML = this.getHTML.bind(this);
    this.setFocusHandlers = this.setFocusHandlers.bind(this);

    this.handleBackButton = this.handleBackButton.bind(this);

    this.didFocus = props.navigation.addListener("didFocus", payload =>
    BackHandler.addEventListener("hardwareBackPress", this.onBack),
  );
  }



  onStyleKeyPress = (toolType) => {
    this.editor.applyToolbar(toolType);
}

onSelectedTagChanged = (tag) => {
    this.setState({
        selectedTag: tag
    })
}

onSelectedStyleChanged = (styles) => { 
    this.setState({
        selectedStyles: styles,
    })
}

onValueChanged = (value) => {
    this.setState({
        value: value
    });
}


  async componentWillMount(){
    console.log('url : ',baseUrl)
    try {
      const value = await AsyncStorage.getItem('UserId');
      const email = await AsyncStorage.getItem('Emailid');
      if (value !== null) {
        // We have data!!
        console.log('user id : ',value);
        fetch(baseUrl+"/mobile/api/get_candidate_profile_info", {
          method: 'POST',
           headers: new Headers({
             'Accept': 'application/json',
                'Content-Type': 'application/json',
           }),
          body: JSON.stringify({
            user_id: value
          })
       })
       .then(response => {
         response.json().then(respond => {
          var sjobCategory = '';
           console.log('response ',respond);
            if(respond.status === "1"){

              if(respond.data.name !== ""){
                var data = respond.data;
                var sjobCategory = data.current_position.split(",");
                var sjobType = data.job_type.split(",");
                console.log(typeof(sjobCategory));
                 var openlocation = (data.open_location == "1")? true : false;
                 var mailingChecked = (data.mailing_status == "1")? true : false;
                 var  post_code_1='',distance_1 = '',post_code_2='',distance_2 ='',lat1='',lng1='',lat2='',lng2='';
                  if(!openlocation){
                         post_code_1 = respond.data.live_postcode;
                        distance_1 = respond.data.live_distance;
                        lat1 = respond.data.live_lat;
                        lng1 = respond.data.live_lng;
                        lat2 = respond.data.uni_lat;
                        lng2 = respond.data.uni_lng;
                        post_code_2 = respond.data.uni_postcode;
                        distance_2 = respond.data.uni_distance;
                  }
                console.log('job Category : ',sjobCategory)
                console.log('job Type : ',sjobType);
                this.setState({  
                 name: data.name ,
                sur_name: data.surname ,
                dob: data.dob,
                openLocationChecked : openlocation,
                mailingListChecked : mailingChecked,
                gender: data.gender,
                email: email,
                job_category_selected : sjobCategory,
                //job_category_SelectedItem: (sjobCategory.length > 0)? sjobCategory : [],
                job_type_selected : sjobType, 
                it_skill: data.it_skills,
                hobbies: data.hobbies,
                language: data.lang_skills,
                Phone_code: data.country_code,
                phone_no: data.phone_number,
                country: data.country,
                state1: data.state,
                Phone_code : data.countrycode,
                city: data.city,
                post_code: data.postcode,
                address: data.address,
               keyAchivement: data.description,
                 user_id : value,
                img_url : data.img_url,
               post_code1: post_code_1,
               redius_in_miles1: distance_1,
               latitude1:  lat1,
              longitude1: lng1,
               post_code2 : post_code_2,
               redius_in_miles2: distance_2,
               latitude2:  lat2,
              longitude2: lng2,});
              console.log('kamlehs');

                  console.log(this.state);

                 //this.getApiState(data.country);
                 //this.getApiCity(data.state);
            }else {
               this.setState({ email : email,user_id: value,img_url : respond.data.img_url});
            }
            
           
               //this.defualtApis();  
               this.mulitSelectApis();
               //this.setState({  isRefresh : false });
              

            }
               
         })
      })
      .catch(error => {
        console.error(error);
      });
     
      }
    } catch (error) {
      console.error('error in AsyncStorage data: ',error);
    }
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }
  
    componentWillUnmount() {
      BackHandler.removeEventListener("hardwareBackPress", this.handleBackButton);
    }
     
     handleBackButton = () => {
          this.props.navigation.goBack();
          return true;
     } 

    defualtApis = ()=> {
       
      fetch(baseUrl+"/mobile/api/country")
     .then(response => {
       response.json().then(respond => {
         console.log('country list : ',respond);
           if(respond.status == "1"){
                 this.setState({ countryList : respond.countries, phoneCodeList : respond.countries, isRefresh : false });      
           }
       })
     })
     .catch(error => {
      console.error(error);
     });

    
    }

    mulitSelectApis = async() => {
      await fetch(baseUrl+"/mobile/api/job_category")
      .then(response => {
        response.json().then(respond => {
          console.log('job category list : ',respond);
            if(respond.status == "1"){
              //this.setState({ jobCategoryList : respond.data }); 
                  this.setState({ jobCategoryList : respond.data , job_category_SelectedItem : (this.state.job_category_selected !== null && typeof this.state.job_category_selected !== 'undefined') ? this.state.job_category_selected : [] });    
                  
                 // console.log('job category list : ',job_category_SelectedItems);
            }
        })
      })
      .catch(error => {
       console.error(error);
      });
      
      await fetch(baseUrl+"/mobile/api/job_type")
      .then(response => {
        response.json().then(respond => {
          console.log('job type list : ',respond);
            if(respond.status == "1"){
                  this.setState({ jobTypeList : respond.data ,job_type_SelectedItems : this.state.job_type_selected });      
            }
        })
      })
      .catch(error => {
       console.error(error);
      });
    }

    getApiState(country){
      fetch(baseUrl+"/mobile/api/state", {
        method: 'POST',
         headers: new Headers({
           'Accept': 'application/json',
              'Content-Type': 'application/json',
         }),
        body: JSON.stringify({
          country_id: country
        })
        }).then(response => {
          response.json().then(respond => {
            console.log('state list : ',respond);
              if(respond.status == "1"){
                    this.setState({ stateList : respond.statelist });
              }
          })
        })
        .catch(error => {
        console.error(error);
        });  
    }

    getApiCity(v){
      fetch(baseUrl+"/mobile/api/city", {
        method: 'POST',
         headers: new Headers({
           'Accept': 'application/json',
              'Content-Type': 'application/json',
         }),
        body: JSON.stringify({
          state_id: v
        })
        }).then(response => {
          response.json().then(respond => {
            console.log('city list : ',respond);
              if(respond.status == "1"){
                    this.setState({ cityList : respond.statelist });
              }
          })
        })
        .catch(error => {
        console.error(error);
        });  
    }



  editprofile = async()=>
  {

    var name = this.state.name.trim();
    var sur_name = this.state.sur_name.trim();
    var email = this.state.email.trim();
    var it_skill = this.state.it_skill.trim();
    var hobbies = this.state.hobbies.trim();
    var language = this.state.language.trim();
    var phone_no = this.state.phone_no.trim();
    var post_code = this.state.post_code.trim();
    var address = this.state.address.trim();
    var keyAchivement = this.state.keyAchivement.trim();
    var post_code1 = this.state.post_code1.trim();
    var redius_in_miles1 = this.state.redius_in_miles1.trim();
    var post_code2 = this.state.post_code2.trim();
    var redius_in_miles2 = this.state.redius_in_miles2.trim();


    console.log('name : ',name);
    console.log('sur_name : ',sur_name);
  //  console.log('dob : ',ExamType);

    console.log('gender : ',this.state.gender);
    console.log('job_type : ',this.state.job_type_SelectedItems);
   // console.log('job_category : ',this.state.job_category_SelectedItem);
    
  
    console.log('it_skill  : ',it_skill);
    console.log('hobbies  : ',hobbies);
    console.log('language  : ',language);

    console.log('Country /Phone_code : ',this.state.Phone_code);
    console.log('phone_no  : ',phone_no);

    console.log('country : ',this.state.country);
    console.log('state1 : ',this.state.state1);
    console.log('city : ',this.state.city);
    
    console.log('post_code  : ',post_code);
    console.log('address  : ',address);
    console.log('keyAchivement  : ',keyAchivement);
    console.log('post_code1  : ',post_code1);
    console.log('redius_in_miles1  : ',redius_in_miles1);
    console.log('post_code2  : ',post_code2);
    console.log('redius_in_miles2  : ',redius_in_miles2);
    console.log('dob  : ',this.state.dob);
    console.log('openLocationCheck Box : ',this.state.openLocationChecked);
    console.log('mailing status Box : ',this.state.openLocationChecked);
    


    if(name === ""){
      console.log('name : ',);
      this.setState({ name_err : true});   
      Toast.show({
        text: 'Invalid Name',
        type: "danger",
      });
      return; 
    }
    this.setState({ name_err : false}); 

    
    if(sur_name === ""){
      console.log('sur_name : ',);
      this.setState({ sur_name_err : true});   
      Toast.show({
        text: 'Invalid Surname',
        type: "danger"
      });
      return; 
    }
   
    this.setState({ sur_name_err : false}); 
   


    // dob 
    /*
    if(this.state.dob === ""){
      console.log('sur_name : ',);
      Toast.show({
        text: 'Select Date of Birth',
        type: "danger"
      });   
      return; 
    }
    */

    /*
    if(this.state.gender === "" || this.state.gender === "Select"){
      console.log('gender  : ',this.state.gender);
      this.setState({ gender_err : true});
      Toast.show({
        text: 'Select Gender',
        type: "danger"
      });
      return;
     }
     this.setState({ gender_err : false});
    */
     
    if(email === ""){
      console.log('email : ',);
      this.setState({ email_err : true});   
      Toast.show({
        text: 'Invalid Email Address',
        type: "danger",
      });
      return; 
    }
    this.setState({ email_err : false}); 
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    if (!pattern.test(email)) {
      this.setState({
            emailerr : true,    
      });
      Toast.show({
        text: 'Invalid Email Address',
        type: "danger"
      });
      console.log('email pattern invalid : ',email);
      return;
    }       
    this.setState({ emailerr : false });

    /*
    if(this.state.job_type_SelectedItems.length == 0){
      this.setState({ job_type_err : true}); 
      console.log('job type selecteditems : ',this.state.job_category_SelectedItem);
      Toast.show({
        text: 'Select Job Type',
        type: "danger"
      });
      return; 
    }

    this.setState({ job_type_err : false}); 

    if(this.state.job_category_SelectedItem.length == 0){
      this.setState({ job_category_err : true}); 
      console.log('job type selecteditems : ',this.state.job_category_SelectedItem);
      Toast.show({
        text: 'Select Job Category',
        type: "danger"
      });
      return; 
    }

    this.setState({ job_category_err : false}); 
    
    if(it_skill === ""){
      console.log('it_skill : ',);
      this.setState({ it_skill_err : true});   
      Toast.show({
        text: 'Invalid IT Skills',
        type: "danger"
      });
      return; 
    }
    this.setState({ it_skill_err : false}); 

    if(hobbies === ""){
      console.log('hobbies : ',);
      this.setState({ hobbies_err : true});
      Toast.show({
        text: 'Invalid Hobbies',
        type: "danger"
      });   
      return; 
    }
    this.setState({ hobbies_err : false}); 
    */

    /*
    if(language === ""){
      console.log('language : ',);
      this.setState({ language_err : true}); 
      Toast.show({
        text: 'Invalid language',
        type: "danger"
      });   
      return; 
    }
    this.setState({ language_err : false}); 
    */

    /*
    if(this.state.Phone_code === "" || this.state.Phone_code === "Select"){
      console.log('Phone_code  : ',this.state.Phone_code);
      this.setState({ Phone_code_err : true});
      Toast.show({
        text: 'Select Phone No',
        type: "danger"
      }); 
      return;
     }
     this.setState({ Phone_code_err : false});
   */

    if(phone_no != "" && phone_no.length < 10){
      console.log('phone_no : ',);
      this.setState({ phone_no_err : true});   
      Toast.show({
        text: 'Invalid Phone no',
        type: "danger"
      }); 
      return; 
    }
    this.setState({ phone_no_err : false}); 

    /*
    if(this.state.country === "" || this.state.country === "Select"){
      console.log('country  : ',this.state.country);
      this.setState({ country_err : true});
      Toast.show({
        text: 'Select Country',
        type: "danger"
      }); 
      return;
     }
     this.setState({ country_err : false});

     if(this.state.state1 === "" || this.state.state1 === "Select"){
      console.log('state1  : ',this.state.state1);
      this.setState({ state_err : true});
      Toast.show({
        text: 'Select State',
        type: "danger"
      });
      return;
     }
     this.setState({ state_err : false});

     if(this.state.city === "" || this.state.city === "Select"){
      console.log('city  : ',this.state.city);
      this.setState({ city_err : true});
      Toast.show({
        text: 'Select City',
        type: "danger"
      });
      return;
     }
     this.setState({ city_err : false});
    
    if(post_code.length < 7 ){
      console.log('post_code : ',);
      this.setState({ post_code_err : true});
      Toast.show({
        text: 'Enter postcode in 7-8 character',
        type: "danger"
      });   
      return; 
    }
    this.setState({ post_code_err : false}); 
    */

    /*
    if(address === ""){
      console.log('address : ',);
      this.setState({ address_err : true});
      Toast.show({
        text: 'Invalid Address',
        type: "danger"
      });   
      return; 
    }
    this.setState({ address_err : false}); 
    

    if(keyAchivement === ""){
      console.log('keyAchivement : ',);
      this.setState({ keyAchivement_err : true});
      Toast.show({
        text: 'Invalid Key Achievements',
        type: "danger"
      });   
      return; 
    }
    this.setState({ keyAchivement_err : false}); 
    */

    /*
    if(!this.state.openLocationChecked){

      
      console.log('open location post code');

        
            if(post_code1 !== ""){
                console.log('post code 1 validation');
                
                if(post_code1 === "" || this.state.postcode1Error){
                  console.log('post_code1 : ');
                  this.setState({ post_code1_err : true,postcode1Error: true});
                  Toast.show({
                    text: 'Invalid Postcode',
                    type: "danger"
                  });    
                  return; 
                }
                 this.setState({ post_code1_err : false,postcode1Error: false}); 
                

           
                  if(post_code1.length < 7){
                    console.log('post_code1 : ');
                    this.setState({ post_code1_err : true,postcode1Error: true});
                    Toast.show({
                      text: 'Enter postcode in 7-8 character',
                      type: "danger"
                    });    
                    return; 
                  }
              this.setState({ post_code1_err : false,postcode1Error: false}); 
               
              if(redius_in_miles1 === ""){
                console.log('redius_in_miles1 : ',);
                this.setState({ redius_in_miles1_err : true});
                Toast.show({
                  text: 'Enter Redius Milies distance',
                  type: "danger"
                });    
                return; 
              }
              this.setState({ redius_in_miles1_err : false,redius_in_miles2 : ''}); 
                  redius_in_miles2 = '';

            }
            else if(post_code2 !== ""){
              console.log('post code 2 validation');
                    
                    if(post_code2 === "" || this.state.postcode2Error){
                      console.log('post_code2 : ',);
                      this.setState({ post_code2_err : true,postcode2Error: true});
                      Toast.show({
                        text: 'Invalid Postcode',
                        type: "danger"
                      });   
                      return; 
                    }
                    this.setState({ post_code2_err : false,postcode2Error: false})
                    

                    if(post_code2.length < 7){
                      console.log('post_code2 : ',);
                      this.setState({ post_code2_err : true,postcode2Error: true});
                      Toast.show({
                        text: 'Enter postcode in 7-8 character',
                        type: "danger"
                      });   
                      return; 
                    }
                    this.setState({ post_code2_err : false,postcode2Error: false})
              
                    if(redius_in_miles2 === ""){
                      console.log('redius_in_miles2 : ',);
                      this.setState({ redius_in_miles2_err : true});
                      Toast.show({
                        text: 'Enter Redius Milies distance',
                        type: "danger"
                      });   
                      return; 
                    }
                    this.setState({ redius_in_miles2_err : false,redius_in_miles1 : '' }); 
                    redius_in_miles1 = '';    
                  
          }else if(post_code1 == "" && post_code2 == ""){
            console.log('post code both null validation');
            Toast.show({
              text: 'Enter Post code and Radius in milies',
              type: "danger"
            });
            return;
          }
  
          
    }
    */
  

    console.log('end validation');

    var locationCheck = (this.state.openLocationChecked)? "1": "0";
    var mailingChk = (this.state.mailingListChecked)? "1": "0";
    
    console.log('location checkbox ',locationCheck);
    console.log('mailingChk checkbox ',mailingChk);

   
      var jobTypeString = this.state.job_type_SelectedItems.toString();
      var jobCategoryString = this.state.job_category_SelectedItem.toString();
      console.log('jobTypeString arr :',jobTypeString);
      console.log('jobCategoryString arr :',jobCategoryString);

      console.log('latitude arr :',this.state.latitude1);
      console.log('logitute arr :',this.state.longitude1);

      var userid = await AsyncStorage.getItem('UserId');
      let dataobject =  JSON.stringify({
              user_id : userid,
              name : name,
              surname : sur_name,
              description : keyAchivement,
              gender: this.state.gender,
              dob: this.state.dob,
              country : this.state.country,
               postcode : post_code,
              it_skills : it_skill,
              lang_skills : language,
              state : this.state.state1,
              city: this.state.city,
              address : address,
              countrycode : this.state.Phone_code,
              phone_number : phone_no,
              hobbies : hobbies,
              live_postcode : post_code1,
              live_distance : redius_in_miles1,
              live_lat : this.state.latitude1,
              live_lng : this.state.longitude1,
              uni_postcode : post_code2,
              uni_distance : redius_in_miles2,
              uni_lat : this.state.latitude2,
              uni_lng : this.state.longitude2,
              open_location : locationCheck,
              mailing_status : mailingChk,
              job_type : jobTypeString,
              job_category : jobCategoryString
              
              });

                console.log('data object ',dataobject);
               this.setState({submitDisabled: true});
    
    fetch(baseUrl+'/mobile/api/edit_profile', {
        method: 'POST',
        headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
        },
      body: JSON.stringify({
        user_id : userid,
        name : name,
        surname : sur_name,
        description : keyAchivement,
        gender: this.state.gender,
        dob: this.state.dob,
        country : this.state.country,
         postcode : post_code,
        it_skills : it_skill,
        lang_skills : language,
        state : this.state.state1,
        city: this.state.city,
        address : address,
        countrycode : this.state.Phone_code,
        phone_number : phone_no,
        hobbies : hobbies,
        live_postcode : post_code1,
        live_distance : redius_in_miles1,
        live_lat : this.state.latitude1,
        live_lng : this.state.longitude1,
        uni_postcode : post_code2,
        uni_distance : redius_in_miles2,
        uni_lat : this.state.latitude2,
        uni_lng : this.state.longitude2,
        open_location : locationCheck,
        mailing_status : mailingChk,
          job_type : jobTypeString,
          job_category : jobCategoryString
        }),
    })
   .then((response) => {
       response.json().then(respond => {
        console.log(respond);
        this.setState({submitDisabled: false});
        if(respond.status == "1"){
              this.uploadImageApi(respond.message);
             }else {
            Toast.show({
              text: respond.message,
              type: "danger"
            });
      }
      
       })
     }).catch(error => {
      this.setState({submitDisabled: false});
      console.error(error);
    });






  }

  setDate(newDate) {
    this.setState({ chosenDate: newDate });
  }
    

    _onPressButton_login()
    {
      const {navigate} = this.props.navigation;
      navigate('Profile');
    }
    onValueChange2(value) {
      this.setState({
        selected2: value
      });
    }
   
    setDate(newDate) {
      // var d = new Date(newDate);
      // var curr_date = d.getDate();
      // var curr_month = d.getMonth() + 1; //Months are zero based
      // var curr_year = d.getFullYear();
      // this.setState({ dob : curr_date + "-" + curr_month + "-" + curr_year });
       this.setState({ dob : newDate });
    }


    changeCountry(value){
      if(value !== "Select")
            this.getApiState(value);       
        this.setState({ country : value })
    }
    onChangeState(value){
      if(value !== "Select")
            this.getApiCity(value);       
        this.setState({ state1 : value })
    }

    changecity(value){
      this.setState({city : value }) 
    }

    onChangeGender(value){
      this.setState({gender : value }) 
    }

    changePhoneCode(value){
      this.setState({ Phone_code : value }) 
    }

    onSelectedItemsChange = (selectedItems) => {
      console.log('category selected item : ',selectedItems);
      this.setState({ job_category_SelectedItem : selectedItems });
    };

    onSelectedJobTypeItemsChange  = (selectedItems) => {
  
      //console.log(selectedItems);
          var arrjobtype = this.state.jobTypeList;
          console.log('arrjobtype : ',arrjobtype)
          console.log('selected Items : ',arrjobtype)
          
            var internshipSeleted = false;
         
            var arrother = [];
            var arr = [];
       arrjobtype.map((arrItem)=> { 
         
        selectedItems.map((item)=> { 
          if(arrItem.id == item && arrItem.job_flag == "internship"){
                              if(internshipSeleted == false){
                                    internshipSeleted = true;       
                                   arr.push(item);
                                   
                              }
                  }
                  else{
                     if(arrItem.id == item){
                      arrother.push(arrItem.id);
                     }
                  
                  }
          console.log('selected item : ',item);
         });
        })
         
         console.log('internship selected : ',internshipSeleted);
        
            if(internshipSeleted){
                    console.log('arr push ',arr);
                    console.log('internship seleted');
                    this.setState({ job_type_SelectedItems : arr });
            }else{
              console.log('arrother : ',arrother);
              this.setState({ job_type_SelectedItems : arrother });
            }
           
      console.log('job type selected item : ',selectedItems);
   
    };


    getlocation1PostCode(postcode){
       if(postcode == "" || postcode.length < 7){
        this.setState({postcode1Error : false });
        return;
       }
       console.log(postcode);
        fetch(baseUrl+"/mobile/api/getLatLong", {
          method: 'POST',
           headers: new Headers({
             'Accept': 'application/json',
                'Content-Type': 'application/json',
           }),
          body: JSON.stringify({
            postcode: postcode
          })
          }).then(response => {
            response.json().then(respond => {
              console.log('post code  : ',respond);
                if(respond.status == "1"){
                     this.setState({ longitude1 : respond.data.longitude, latitude1 : respond.data.latitude,postcode1Error : false,post_code1_err : false});
                }else {
                  this.setState({ postcode1Error : true });
                }
            })
          })
          .catch(error => {
          console.error(error);
          });  
      

    }

    getlocation2PostCode(postcode){
      if(postcode == "" || postcode.length < 7 ) {
        this.setState({postcode2Error : false });
        return;
       }
    
      fetch(baseUrl+"/mobile/api/getLatLong", {
        method: 'POST',
         headers: new Headers({
           'Accept': 'application/json',
              'Content-Type': 'application/json',
         }),
        body: JSON.stringify({
          postcode: postcode
        })
        }).then(response => {
          response.json().then(respond => {
            console.log('post code  : ',respond);
              if(respond.status == "1"){
                   this.setState({ longitude2 : respond.data.longitude, latitude2 : respond.data.latitude,postcode2Error : false,post_code2_err: false});
              }else {
                this.setState({ postcode2Error : true });
              }
          })
        })
        .catch(error => {
        console.error(error);
        });
      
    }

    handleChoosePhoto = () => {


      ImagePicker.openPicker({
        width: 300,
        height: 400,
        cropping: true
      }).then(response => {
        console.log(response);
        if (response.path) {
           var filename = uri.replace(/^.*[\\\/]/, '');
              this.setState({ photo: {uri : response.path,type : response.mime ,fileName:  filename} , img_url : response.path});
             }
      });

    };

    cameraImagePicker = ()=> {
      console.log('camera image picker ');
        this.setState({ imageSelectActionDialogVisible: false });
        ImagePicker.openCamera({
          width: 300,
          height: 400,
          cropping: true,
        }).then(response => {
          console.log(response);
          if (response.path != null) {
             var filename = response.path.replace(/^.*[\\\/]/, '');
                this.setState({ photo: {uri : response.path,type : response.mime ,fileName:  filename} , img_url : response.path});
               }
        }).catch(() => { console.log('cancel image camera') });
    }

    gallaryImagePicker = ()=> {
      console.log('gallary image picker ')
      this.setState({ imageSelectActionDialogVisible: false });
      ImagePicker.openPicker({
        width: 300,
        height: 400,
        cropping: true
      }).then(response => {
        console.log(response);
        
           var filename = response.path.replace(/^.*[\\\/]/, '');
        console.log(response);
              this.setState({ photo: {uri : response.path,type : response.mime ,fileName:  filename} , img_url : response.path});
           
      }).catch(() => { console.log('cancel image gallary') });
    }
    


    uploadImageApi = (message)=> {
      if(this.state.photo.uri != null){
        const formData = new FormData();
         const {uri , fileName , type} = this.state.photo;
         console.log('photo uri : ',uri);
         console.log('photo name : ',fileName);
         console.log('photo type : ',type);
        formData.append('file', {uri: uri,name: fileName,type: type});
        formData.append('user_id',this.state.user_id );
          console.log('image upload request ',formData);
        const options = {
          method: 'POST',
          headers: {
            'Content-type': 'multipart/form-data'
          },
          body: formData,
        };
  
        fetch(baseUrl+'/mobile/api/photo_upload', options)
        .then((res)=> {
              res.json().then((response)=> {
                    console.log('response : ',response);
                    if(response.status == "1"){
                      Toast.show({
                        text: message,
                        type: "success"
                      });
                   

                      const resetAction = StackActions.reset({
                        index: 0,
                        actions: [NavigationActions.navigate({ routeName: 'Candidate_profile1' })],
                      });
                      this.props.navigation.dispatch(resetAction);


                      //  this.props.navigation.state.params.getProfileData();
                    //  this.props.navigation.goBack();
                    }
              })
        }).catch((error)=> {
            console.log('error : ',error);
        })
      }else{
        console.log('response success upload image ');
        Toast.show({
          text: message,
          type: "success"
        });
      
        const resetAction = StackActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({ routeName: 'Candidate_profile1' })],
        });
        this.props.navigation.dispatch(resetAction);

      
        //  this.props.navigation.state.params.getProfileData();
        // this.props.navigation.goBack();
      }
     
    }




    onEditorInitialized() {
      this.setFocusHandlers();
      this.getHTML();
    }
  
    async getHTML() {
      const titleHtml = await this.richtext.getTitleHtml();
      const contentHtml = await this.richtext.getContentHtml();
      //  this.setState({ keyAchivement : contentHtml });
      // alert(titleHtml + ' ' + contentHtml)
    }
  
    setFocusHandlers() {
      this.richtext.setTitleFocusHandler(() => {
        //alert('title focus');
      });
      this.richtext.setContentFocusHandler(() => {
        //alert('content focus');
      });
    }

  
  


    render() {


      const { children, horizontalPercent = 1, verticalPercent = 1 } = this.props;
      const height = verticalPercent ? deviceHeight * verticalPercent : deviceHeight;
      const width = horizontalPercent ? deviceWidth * horizontalPercent : deviceWidth;
      
      console.log('JOB CTE');
      console.log(typeof(this.state.job_category_selected));
      leftIcon = require('../assest/left_arrow.png');

      eventHandler = (
        <TouchableOpacity style={{width:45,alignItems:"center"}} 
          onPress={() => {
            Alert.alert(
            "Confirm",
             "Going Back will Revert all Unsaved Changes, Do You Want to Continue?", 
             [
              {
                text: "Yes",
                onPress: () => this.props.navigation.navigate('Candidate_profile1')
              },
              {
                text: "No",
                
              }
            ]
            )
          }}
        hitSlop={{top:12, left:36, bottom:12,right:0}}>
          <Image source={leftIcon} style={{width:24,height:24}} />
        </TouchableOpacity>
      );
      
    //     console.log('this props edit profile : ',this.props);
      YellowBox.ignoreWarnings(['Warning: Async Storage has been extracted from react-native core','ListView is deprecated']);
      if(this.state.isRefresh){
        return <View style={{flex : 1,justifyContent : 'center'}}><ActivityIndicator size="large" color="#0000ff" /></View>;
      }else {
      return (
        <Container style={styles.container}>
          
          <ImageBackground style={{
            backgroundColor: "#2e2b48",
                width: null,
                zIndex:1,
                elevation: 10
                }}>

              <View style={{ backgroundColor: "trasparent", height: 70, width: '100%',  ...Platform.select({android:{height:height>800?70:60}}), }} >
                  <View
                      style={{
                        flexDirection: "row",
                        justifyContent: "space-between",
                        flex:1,
                        marginTop: Platform.OS == "ios" ? height>800?25:20: height>800?25:20, // only for IOS to give StatusBar Space
                        ...Platform.select({android:{marginTop:0}}),
                      }}>
                      <View style={{justifyContent:"center",marginLeft:15,width:"18%"}}>
                        {eventHandler}
                      </View>
                      <View style={{width:"82%",justifyContent:"center"}}>
                        <Text numberOfLines={1} style={{color:"#fff", fontSize:21,fontWeight:"bold",textAlign:"left",fontStyle: "normal"}}>
                          Edit Profile
                        </Text>
                      </View>
                  </View>

              </View>
            </ImageBackground>
            <Image source={require('../assest/login_bg.png')} style={styles.backgroundImage} />
           
           <Content style={styles.loginForm}>
          
          <Form style={styles.form1}>
         



                  <View style={{ flex: 1,}}>
                    <Text style={{paddingTop : 8,paddingLeft : 4, fontSize : 15, color : (this.state.job_category_err)?'red' : '#1f6fe7' }}>Select Job Category</Text>
                    <Item  style={styles.labunder}>
                    <ScrollView>
                      <SectionedMultiSelect
                        items={this.state.jobCategoryList}
                        uniqueKey="id"
                        //style={{ height:20,}}
                        displayKey="job_category"
                        selectText="Select Job Category"
                        showDropDowns={true}
                        onSelectedItemsChange={this.onSelectedItemsChange}
                        //selectedItems={ [] }
                        selectedItems={(this.state.job_category_SelectedItem !== null && typeof this.state.job_category_SelectedItem !== 'undefined') ? this.state.job_category_SelectedItem : []}
                        styles={{
                          selectToggleText:{
                            width: "100%",
                          }, 
                          selectToggle : { 
                            flex : 1,
                            alignItems : "flex-end", 
                          },
                          chipsWrapper : {
                            opacity: 0,
                            height : 10,
                          },
                          chipContainer : {
                            opacity: 0,
                            height : 0,
                            backgroundColor : "green",
                          }
                        }}
                      />
                      </ScrollView>
                  </Item>
                </View>


      
          </Form>
             </Content>
           
      </Container>
      );
        }
    }
}
const styles = StyleSheet.create({
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover', // or 'stretch'
  },
  loginForm: {
    flex: 1,
    position: 'absolute',
    backgroundColor: 'transparent', width: '90%', height: '100%',
    paddingTop:"18%"
  },
  container: {

    flex: 1,
    backgroundColor: 'transparent',
    alignItems: 'center', justifyContent: 'center',



  },


  loginContainer: {
    alignItems: 'center',
    flexGrow: 1,
    justifyContent: 'center'
  },
  form1:{ flex: 1, paddingLeft:0 },
  btnchose: { backgroundColor: '#1f6fe7', fontSize:14,textAlign: 'center', borderRadius: 5, height: 35,},
  input:{
    color: '#333',
  },
  richText: {
    width: '100%',
    height: 200,
    alignItems:'center',
    justifyContent: 'center',
    backgroundColor: 'transparent',
  },
  labunder:{flex: 1,  flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: '#017bc2', marginBottom:5,marginTop: 10}
})

export default EditProfile;
