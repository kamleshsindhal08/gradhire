import React, { Component } from 'react';
import { Image, TouchableOpacity, StyleSheet, ScrollView, YellowBox, FlatList, BackHandler, Modal, Alert, Platform,PermissionsAndroid, UIManager, LayoutAnimation, Linking, ActivityIndicator } from 'react-native';
// import { Collapse, CollapseHeader, CollapseBody } from "accordion-collapse-react-native";
import { List, ListItem, Separator, Container, Header, Content, View, Form, Item, Input, Badge, Label, Button, Text, Icon, Card, Spinner, CardItem, Thumbnail, Left, Body, Right, Row, Toast } from 'native-base';
import { Dialog } from 'react-native-simple-dialogs';
import RNFS from 'react-native-fs';
import AsyncStorage from '@react-native-community/async-storage';
const { baseUrl } = require('./baseurl/Url');
import moment from "moment";
import Video from 'react-native-video';
var value = 0;

import FileViewer from 'react-native-file-viewer';

class Companyjobdetails extends Component {

  _isMounted = false;

  constructor(props) {
    super(props);

    this.state = {

      Email: '',
      MobileNumber: '',
      PhoneNumber: '',
      emailerr: false,
      MobileNumbererr: false,
      PhoneNumbererr: false,
      expanded: false,
      Alert_Visibility: false,
      videoList: [
        { name: 'sohit' },
        { name: 'rohit' },
        { name: 'sdohit' },
        { name: 'rohgit' },
        { name: 'soheit' },
        { name: 'rohijt' }
      ],
      data: {},
      companyvideolist: [],
      jobvideolist: [],
      videoitem: {},
      viewModel: false,
      interestedVisible: false,
      notInterestVisible: false,
      yesIamInterestVisible: false,
      noIamNotInterestedVisible: false,
      letMeThinkAboutVisible: false,
      letMeThinkAboutApiEnable: false,
      jobfileVisible : false,
      isRefresh : true,
      pointerEventView : 'box-none',
      progressBarVisible : false
    };
    this.delete = this.delete.bind(this);
    this.getJobAlertData = this.getJobAlertData.bind(this);
    if (Platform.OS === 'android') {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }

  }

  static navigationOptions = ({ navigation }) => {
        return {
    title: 'Job Detail',
    headerStyle: { backgroundColor: '#2e2b48' },
    headerTintColor: 'white',
    headerLeft : (<TouchableOpacity onPress={()=> {
      const {sortbySelected,getJobAlert } = navigation.state.params;
      getJobAlert(sortbySelected); navigation.goBack(); }}>
      <Image style={{width: 24,height: 24,marginLeft : 20,marginRight: 10,marginBottom: 10,marginTop: 10,padding: 10}} source={require('../assest/left_arrow.png')} />
      </TouchableOpacity>)
   }
  };

  changeLayout = () => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    this.setState({ expanded: !this.state.expanded });
  }




  async componentDidMount() {
    this._isMounted = true;
     value = await AsyncStorage.getItem('UserId');
    this.getJobAlertData();
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }

  componentWillUnmount() {
    this._isMounted = false;
  }


  getJobAlertData = async () => {
     this.setState({ 
       interestedVisible: false,
      notInterestVisible: false,
      yesIamInterestVisible: false,
      noIamNotInterestedVisible: false,
      letMeThinkAboutVisible: false,
      letMeThinkAboutApiEnable: false,
      jobfileVisible : false });
    try {
      if (value !== null) {
        // We have data!!
        console.log('user id : ', value);
        const { paramid } = this.props.navigation.state.params;
        fetch(baseUrl + "/mobile/api/job_alert", {
          method: 'POST',
          headers: new Headers({
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          }),
          body: JSON.stringify({
            user_id: value
          })
        })
          .then(response => {
            response.json().then(respond => {
              console.log('response job alert details', respond);
              if (respond.status === "1") {
                respond.data.map((item) => {
                  if (item.id == paramid) {
                      var filedownloadVisible = item.job_file == ""? false : true;
                    if (item.invite_status == "1") {
                      this.setState({ data: item, interestedVisible: true,jobfileVisible : filedownloadVisible });
                    } else if (item.invite_status == "2") {
                      this.setState({ data: item, notInterestVisible: true,jobfileVisible : filedownloadVisible });
                    } else if (item.invite_status == "4") {
                      this.setState({ data: item, noIamNotInterestedVisible: true, yesIamInterestVisible: true, letMeThinkAboutVisible: true, letMeThinkAboutApiEnable: false ,jobfileVisible : filedownloadVisible });
                    } else {
                      this.setState({ data: item, yesIamInterestVisible: true, noIamNotInterestedVisible: true, letMeThinkAboutVisible: true, letMeThinkAboutApiEnable: true ,jobfileVisible : filedownloadVisible });
                    }

                    console.log('match id ', item.id);
                  }
                })
                this.defaultApi();
                
              }

              console.log('state data : ', this.state.data);

            })
          })
          .catch(error => {
            console.error(error);
          });



      }

    } catch (error) {
      console.error(error);

    }
  }




  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  handleBackButton = () => {
    this.props.navigation.goBack();
    return true;
  }

  delete() {
    console.log('data of delete : ', this.state.data);

    fetch(baseUrl + "/mobile/api/delete_job", {
      method: 'POST',
      headers: new Headers({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      }),
      body: JSON.stringify({
        job_id: this.state.data.job_id,
        company_user_id: this.state.data.user_id,
        candidate_id: this.state.data.candidate_id,
      })
    })
      .then(response => {
        response.json().then(respond => {
          console.log('response delete api ', respond);
          if (respond.status === "1") {
            Toast.show({
              text: respond.message,
              type: "success "
            });

            this.props.navigation.navigate('JobAlert',{ token: '123' });

            if (this._isMounted) {
                //this.getJobAlertData();
            }
          } else {
            Toast.show({
              text: respond.message,
              type: "danger"
            });
          }
        })
      })
      .catch(error => {
        console.error(error);
      });
      
  }

  not_interested() {
    console.log('data of delete : ', this.state.data)
    fetch(baseUrl + "/mobile/api/not_interested_invite_dob_detail", {
      method: 'POST',
      headers: new Headers({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      }),
      body: JSON.stringify({
        job_id: this.state.data.job_id,
        company_user_id: this.state.data.user_id,
        candidate_id: this.state.data.candidate_id,
      })
    })
      .then(response => {
        response.json().then(respond => {
          console.log('response not interseted ', respond);
          if (respond.status === "1") {
            Toast.show({
              text: respond.message,
              type: "success "
            });
            if (this._isMounted) {
            this.getJobAlertData();
            }
          } else {
            Toast.show({
              text: respond.message,
              type: "danger"
            });
          }
        })
      })
      .catch(error => {
        console.error(error);
      });
  }

  letmethink() {
    console.log('data of delete : ', this.state.data)
    fetch(baseUrl + "/mobile/api/let_me_think_about_it", {
      method: 'POST',
      headers: new Headers({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      }),
      body: JSON.stringify({
        job_id: this.state.data.job_id,
        company_user_id: this.state.data.user_id,
        candidate_id: this.state.data.candidate_id,
      })
    })
      .then(response => {
        response.json().then(respond => {
          console.log('response letme think ', respond);
          if (respond.status === "1") {
            Toast.show({
              text: respond.message,
              type: "success "
            });
            if (this._isMounted) {
               this.getJobAlertData();
            }
          } else {
            Toast.show({
              text: respond.message,
              type: "danger"
            });
          }
        })
      })
      .catch(error => {
        console.error(error);
      });
  }





  async inapprocated() {
    try {
      const email = await AsyncStorage.getItem('Emailid');
      if (email !== null) {
        console.log('data of delete : ', this.state.data)
        fetch(baseUrl + "/mobile/api/inappropriate_job", {
          method: 'POST',
          headers: new Headers({
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          }),
          body: JSON.stringify({
            job_id: this.state.data.job_id,
            company_user_id: this.state.data.user_id,
            candidate_id: this.state.data.candidate_id,
            email: email,
          })
        })
          .then(response => {
            response.json().then(respond => {
              console.log('response inapprocated ', respond);
              if (respond.status === "1") {
                Toast.show({
                  text: respond.message,
                  type: "success "
                });
                if (this._isMounted) {
                     this.getJobAlertData();
                }
              } else {
                Toast.show({
                  text: respond.message,
                  type: "danger"
                });
              }
            })
          })
          .catch(error => {
            console.error(error);
          });
      }
    }
    catch (err) {
      console.error(err);
    }



  }

  async yesIamInterested() {
    const value = await AsyncStorage.getItem('UserId');
    console.log(value);
    if (value != null) {
      this.props.navigation.navigate('ImInterestContact', { id: value, company_user_id: this.state.data.client_id, candidate_id : this.state.data.candidate_id, job_id : this.state.data.job_id,  getJobAlertData: this.getJobAlertData });
    }
  }




  defaultApi() {

    fetch(baseUrl + "/mobile/api/company_video", {
      method: 'POST',
      headers: new Headers({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      }),
      body: JSON.stringify({
        company_user_id: this.state.data.user_id
      })
    })
      .then(response => {
        response.json().then(respond => {
          console.log('response company video', respond);
          if (respond.status == "1") {
            console.log('responsees');
            console.log(respond.data);
            this.setState({ companyvideolist: respond.data })
          }
          this.setState({isRefresh: false});
        })
      })
      .catch(error => {
        console.error(error);
      });

    fetch(baseUrl + "/mobile/api/job_video", {
      method: 'POST',
      headers: new Headers({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      }),
      body: JSON.stringify({
        company_user_id: this.state.data.user_id,
        job_id: this.state.data.job_id
      })
    })
      .then(response => {
        response.json().then(respond => {
          console.log('response job video', respond);
          if (respond.status == "1") {
            this.setState({ jobvideolist: respond.data })
          }
          this.setState({isRefresh: false});
        })
      })
      .catch(error => {
        console.error(error);
      });


  }

  fb(url) {
    Linking.canOpenURL(url).then(supported => {
      if (supported) {
        Linking.openURL(url);
      } else {
        console.log("Don't know how to open URI: " + url);
      }
    });
  }

  Show_Custom_Alert(visible) {

    this.setState({ Alert_Visibility: visible });

  }

  requestDownloadPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: 'Gradhire App Permission',
          message:
            'Gradhire App needs access to your Storage ' +
            'so you can view the job specification.',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          console.log('You can use the Storage');
          return true;
      } else {
        return false;
      }
    } catch (err) {
      console.warn(err);
    }
  }

  

  job_specification_downloadfile(){

    var date = Math.random();
    var name = date.toString(36).substr(2, 9);;
        console.log('filename :',name.toString());

      job_file = 'https://d9db56472fd41226d193-1e5e0d4b7948acaf6080b0dce0b35ed5.ssl.cf1.rackcdn.com/spectools/docs/wd-spectools-word-sample-04.doc';

        var fileext = this.state.data.job_file.split('.').pop();

        //var file= 'http://www.africau.edu/images/default/sample.pdf';
        //var fileext = file.split('.').pop();
        console.log('file extenstion : ',fileext)
        console.log(`${RNFS.DocumentDirectoryPath}/${name}.${fileext}`);

        //if(fileext == 'pdf'){
          if(Platform.OS == 'android'){
            var result = this.requestDownloadPermission();
            if(!result){
              return false;
            }
          }

          this.setState({pointerEventView : 'box-only',progressBarVisible: true});
          
          const path = (Platform.OS == 'android') ? RNFS.DocumentDirectoryPath : RNFS.LibraryDirectoryPath;
      
          console.log(`${path}/${name}.${fileext}`);

          RNFS.downloadFile({
            fromUrl: this.state.data.job_file,
            //fromUrl: 'http://www.africau.edu/images/default/sample.pdf',
            //fromUrl : job_file,
            toFile: `${path}/${name}.${fileext}`,
          }).promise.then((r) => {
            console.log('download file : ',r);
            this.setState({pointerEventView : 'box-none',progressBarVisible: false});

            var localFile = `${path}/${name}.${fileext}`;

            FileViewer.open(localFile,  { showOpenWithDialog: true })
            .then((response) => {
              console.log(response);
            })
            .catch(error => {
              //console.log(error);
              Alert.alert('Gradhire', "You don't have an app that can open this document. Please download an app and try again",
                [ { text: 'OK'} ],
              );


            });

            /*
            if(r.statusCode == 200){
              //Toast.show({ text: 'Download Successful',type: 'success'});
              //Linking.openURL(this.state.data.job_file);
            } else 
                  Toast.show({ text: 'Download Failed.',type: 'danger'});
  */
          }).catch(err => { 
            this.setState({pointerEventView : 'box-none',progressBarVisible: false});
            console.log('error : ',err) 
          });

        //}

  }




  render() {
    YellowBox.ignoreWarnings(['Warning: Async Storage has been extracted from react-native core']);
    if(this.state.isRefresh){
          return <View style={{flex : 1,justifyContent : 'center'}}><ActivityIndicator size="large" color="#0000ff" /></View>;
      }else {
        return (
          <Container style={styles.container}>
            <Image source={require('../assest/login_bg.png')} style={styles.backgroundImage} />
            <Content style={styles.loginForm}>
              <Form>
                <Card style={styles.cardNew}>
                  {/* <Text style={{color: '#0275d8', textAlign: "center", paddingTop: 10,textTransform: 'uppercase'}}>Job Alert</Text>
              <Text style={{textAlign: "center", paddingTop: 10}}>Get Job Details</Text> */}
                  <View style={{ flexDirection: 'row' }}>
                    <View style={{ paddingRight: 10, paddingleft: 10, }} >
                    { (this.state.data.image_url)?
                      <Image source={{ uri: this.state.data.image_url }} style={{ width: 100, height: 100, }} />
                      :
                      <View style={{ justifyContent:"center", alignItems:"center" , borderWidth:1 , borderRadius: 100/2, backgroundColor : "white", width:100, height:100}}>
                        <Icon name="ios-briefcase" style={{  fontSize: 50, color: "#333"}} />
                      </View>
                    }
                    </View>
                    <View style={{ flexDirection: 'column',flex:1 }}>
                      <View style={{ flexDirection: 'row', marginBottom: 7 }}>
                        <Text style={{ color: '#333', flexDirection: 'column', fontWeight:'bold', fontSize:17 }}>{this.state.data.job_title}</Text>
                      </View>
                      <View style={{ flexDirection: 'row',marginBottom: 8  }}>
                      { this.state.data.fb_link == "" || this.state.data.fb_link == null?null: 
                          <TouchableOpacity onPress={() => { this.fb(this.state.data.fb_link); }}>
                            <Image resizeMode="contain" style={{ height: 24, width: 24, marginLeft: 1, }} source={require('../assest/linkedin.png')} />
                          </TouchableOpacity> 
                      }
                      {this.state.data.instagram_link == "" || this.state.data.instagram_link == null?null:   
                          <TouchableOpacity onPress={() => { this.fb(this.state.data.instagram_link); }}>
                            <Image resizeMode="contain" style={{ height: 24, width: 24, marginLeft: 10, }} source={require('../assest/instagram.png')} />
                          </TouchableOpacity>
                      }
                      </View>

                      <View style={{flex:1, flexDirection: "row", marginBottom: 5 }}>
                        <Image source={require('../assest/clock_icon.png')} style={{width : 10,height: 8,padding : 8,marginTop: 3,marginRight: 3}} />
                        <Text style={styles.spanType}>{this.state.data.job_type}</Text>
                      </View>

                      {this.state.data.company_name &&   
                      <TouchableOpacity onPress={() => { (this.state.data.company_website !="") ? Linking.openURL(this.state.data.company_website) : console.log('website'); }}>
                        <View style={{ flexDirection: 'row' ,marginBottom: 8 }}>
                          <Thumbnail resizeMode="contain" small square style={{ height: 20, width: 20 }} source={require('../assest/job.png')} />
                          <Label style={{ color: '#1f6fe7', fontWeight: 'bold', paddingLeft: 5 }}>{this.state.data.company_name}</Label>
                        </View>
                      </TouchableOpacity> }
                    
                      <View style={{ flexDirection: 'row',marginBottom: 8 }}>
                        <View style={{ flexDirection: 'row', }}>
                          <Thumbnail resizeMode="contain" small square style={{ height: 20, width: 20, }} source={require('../assest/wallet.png')} />
                          <Text style={{ color: '#333', paddingLeft: 5 }}>£{this.state.data.min_salary} - {this.state.data.max_salary}</Text>
                        </View>

                      </View>
                    </View>
                  </View>


                  <View style={{ flex: 1, flexDirection: 'row', marginTop: 10, marginBottom:5 }}>
                    <Thumbnail resizeMode="contain" small square style={{ width: 20, height: 20, }} source={require('../assest/address.png')} />
                    <Text style={{ color: '#5c5c5c' }}>{this.state.data.country}, {this.state.data.postcode}</Text>
                    <TouchableOpacity onPress={() => { console.log('view map '); 
                    this.props.navigation.navigate('Map1', { job_id: this.state.data.job_id });
                //  this.props.navigation.navigate('ComingSoon');
                    
                    }}>
                      <View style={styles.vieMap}   >
                        <Text style={{ color: '#fff', fontSize: 14, }}>View on Map</Text>
                      </View>
                    </TouchableOpacity>

                  </View>

                  
                    { (this.state.interestedVisible || this.state.notInterestVisible || (!this.state.letMeThinkAboutApiEnable)) ?
                    <View style={{ flex: 1, flexDirection: 'row', margin: 5, alignItems: 'center', justifyContent: 'center', marginBottom: 10, marginTop: 10, }}>
                      <TouchableOpacity onPress={() => {
                        Alert.alert(
                          'Gradhire',
                          'Are you sure you want to delete this job?',
                          [
                            {
                              text: 'Cancel',
                              onPress: () => console.log('Cancel Pressed'),
                              style: 'cancel',
                            },
                            { text: 'OK', onPress: () => { this.delete(); console.log('OK Pressed') } },
                          ]
                        );
                      }}>
                        <View style={[styles.outView, {backgroundColor: '#d9534f', borderColor: '#d9534f'}]} >
                          <Text style={{ color: '#fff', paddingLeft: 10, paddingRight: 10, }}>Delete Job</Text>
                        </View>
                      </TouchableOpacity>
                    </View>
                     : null }
                  
 

                  <View style={{ flex: 1, flexDirection: 'row', margin: 5, alignItems: 'center', justifyContent: 'center', marginBottom: 10, }}>
                    {this.state.interestedVisible ?
                      <View style={styles.solView} >
                        <Text style={{ color: '#fff', padding: 7, fontSize: 12 }}>Interested</Text>
                      </View>

                      : null}

                    {this.state.notInterestVisible ?
                      <View style={[styles.solView, { backgroundColor: 'orange' }]} >
                        <Text style={{ color: '#fff', padding: 7, fontSize: 12 }}>Not Interested</Text>
                      </View>
                      : null}
                  </View>

                  {this.state.yesIamInterestVisible ?     
                  <View style={{ flex: 1, flexDirection: 'row', margin: 5, alignItems: 'center', justifyContent: 'center', marginBottom: 10, }}>
                        <TouchableOpacity onPress={() => {
                          Alert.alert(
                            'Gradhire',
                            'Are you sure you are Interested?',
                            [
                              {
                                text: 'Cancel',
                                onPress: () => console.log('Cancel Pressed'),
                                style: 'cancel',
                              },
                              { text: 'OK', onPress: () => { this.yesIamInterested(); console.log('OK Pressed') } },
                            ]
                          );
                        }}>
                          <View style={[styles.solView, { backgroundColor: '#5cb85c' }]} >
                            <Text style={{ color: '#fff', padding: 7, fontSize: 12 }}>Yes, I Am Interested</Text>
                          </View>
                        </TouchableOpacity> 
                    </View> : null}

                  {this.state.noIamNotInterestedVisible ?    
                  <View style={{ flex: 1, flexDirection: 'row', margin: 5, alignItems: 'center', justifyContent: 'center', marginBottom: 10, }}>
                      <TouchableOpacity onPress={() => {
                        Alert.alert(
                          'Gradhire',
                          'Are you sure you are not interested?',
                          [
                            {
                              text: 'Cancel',
                              onPress: () => console.log('Cancel Pressed'),
                              style: 'cancel',
                            },
                            { text: 'OK', onPress: () => { this.not_interested(); console.log('OK Pressed') } },
                          ]
                        );
                      }}>
                        <View style={[styles.solView, { backgroundColor: 'orange' }]} >
                          <Text style={{ color: '#fff', padding: 7, fontSize: 12 }}>No, I Am Not Interested</Text>
                        </View>
                      </TouchableOpacity>
                  </View>  : null}

                 

                  { (this.state.letMeThinkAboutVisible && (!this.state.letMeThinkAboutApiEnable) ) ?   
                    <View style={{ flex: 1, flexDirection: 'row', margin: 5, alignItems: 'center', justifyContent: 'center', marginBottom: 10, marginTop: 10, }}>
                      <View style={[styles.solView]} >
                        <Text style={{ color: '#fff', padding: 7, fontSize: 12 }}> Grade up </Text>
                      </View>
                     </View>
                  : null

                  }

                  { (this.state.letMeThinkAboutVisible && this.state.letMeThinkAboutApiEnable ) ?   
                  <View style={{ flex: 1, flexDirection: 'row', margin: 5, alignItems: 'center', justifyContent: 'center', marginBottom: 10, }}>
                      <TouchableOpacity onPress={() => {
                        if (this.state.letMeThinkAboutApiEnable) {
                          Alert.alert(
                            'Gradhire',
                            'Are you sure you want to think about it',
                            [
                              {
                                text: 'Cancel',
                                onPress: () => console.log('Cancel Pressed'),
                                style: 'cancel',
                              },
                              { text: 'OK', onPress: () => { this.letmethink(); console.log('OK Pressed') } },
                            ]
                          );
                        }else{
                          Alert.alert(
                            'Gradhire',
                            'Already selected. Please select Yes,I am Interested or Not',
                            [
                              { text: 'OK', onPress: () => {  console.log('OK Pressed') } },
                            ]
                          );
                        }
                      }}>
                        <View style={styles.solView}>
                          <Text style={{ color: '#fff', padding: 7, fontSize: 12 }}>Let me Think About It</Text>
                        </View>
                      </TouchableOpacity> 

                  </View> : null}

                    <View style={{ flex: 1, flexDirection: 'row', margin: 5, alignItems: 'center', justifyContent: 'center', marginBottom: 10, }}>
                      {this.state.jobfileVisible?
                      <TouchableOpacity onPress={() => {
                        Alert.alert(
                          'Gradhire',
                          'Are you sure you want to view the job specification?',
                          [
                            {
                              text: 'Cancel',
                              onPress: () => console.log('Cancel Pressed'),
                              style: 'cancel',
                            },
                            { text: 'OK', onPress: () => { this.job_specification_downloadfile(); } },
                          ]
                        );
                      }}>
                        <View style={[styles.solView, { backgroundColor: '#0275d8' }]} >
                          <Text style={{ color: '#fff', padding: 7, fontSize: 12 }}>CLICK TO VIEW JOB SPECIFICATION</Text>
                        </View>
                      </TouchableOpacity> : null }

                      <View pointerEvents={this.state.pointerEventView} style={{position: 'absolute',justifyContent: 'center',width: '100%',height: '100%'}}>
                              {this.state.progressBarVisible && <ActivityIndicator style={{top: 0,left: 0,right: 0}} size="large" color="#0000ff" /> }
                        </View>
                  </View>


                  <View style={{ flex: 1, marginLeft: 10, paddingBottom: 10, marginBottom: 10, borderBottomWidth: 1, borderBottomColor: '#d1d1d1' }}>
                    <Text style={{ color: '#000', flexDirection: 'row', fontWeight: 'bold' }}>Holiday</Text>
                    <Text style={{ color: '#333',padding: 5 }}>{this.state.data.holidays}</Text>
                  </View>
                  <View style={{ flex: 1, marginLeft: 10, paddingBottom: 10, marginBottom: 10, borderBottomWidth: 1, borderBottomColor: '#d1d1d1' }}>
                    <Text style={{ color: '#000', flexDirection: 'row', fontWeight: 'bold' }}>Pension</Text>
                    <Text style={{ color: '#333',padding: 5  }}>{this.state.data.pension}</Text>
                  </View>
                  <View style={{ flex: 1, marginLeft: 10, paddingBottom: 10, marginBottom: 10, borderBottomWidth: 1, borderBottomColor: '#d1d1d1' }}>
                    <Text style={{ color: '#000', flexDirection: 'row', fontWeight: 'bold' }}>Bonus</Text>
                    <Text style={{ color: '#333',padding: 5  }}>{this.state.data.bonus}</Text>
                  </View>
                  <View style={{ flex: 1, marginLeft: 10, paddingBottom: 10, marginBottom: 10, borderBottomWidth: 1, borderBottomColor: '#d1d1d1' }}>
                    <Text style={{ color: '#000', flexDirection: 'row', fontWeight: 'bold' }}>Additional Benifits</Text>
                    <Text style={{ color: '#333',padding: 5  }}>{this.state.data.additional_benefits}</Text>
                  </View>
                  <View style={{ flex: 1, paddingLeft: 10, }}>
                    <Text style={{ color: '#000', flexDirection: 'row', fontWeight: 'bold' }}>Job Description</Text>
                    <Text style={{ color: '#333', padding: 5 }}>{this.state.data.job_description}</Text>
                  </View>





                  {this.state.companyvideolist.length == 0 ? null :
                    <View style={{ flex: 1, marginLeftL: 10, marginTop: 14 }}>

                      <View style={{ paddingTop: 5, flexDirection: 'row', flex: 1, marginLeft: 10, paddingBottom: 6, }}>
                        <Text style={styles.texth1}>Video About this Company</Text>

                      </View>

                      {/* FlatLIst Video List */}


                      <View style={{ marginLeft: 10 }}>
                        <FlatList
                          horizontal
                          data={this.state.companyvideolist}
                          keyExtractor={item => item.company_video_id}
                          renderItem={({ item }, index) =>
                            <View key={item.company_video_id}>
                              <TouchableOpacity onPress={() => {
                                this.props.navigation.navigate('VideoPlayer', { videoItem: item });
                                //   this.setState({  videoitem : item,videoModel: !this.state.videoModel});  
                              }}>
                                <Card style={{ padding: 10, color: "#0275d8" }}>
                                <View style={{flex:1, justifyContent: 'center',alignItems: 'center'}}>
                                  <Image source={require('../assest/vidscreen.png')} style={{ flex: 1 }} />
                                  <View style={{justifyContent: 'center',alignItems: 'center', position : "absolute", alignSelf : "center" }}>
                                    <TouchableOpacity onPress={() => {
                                      this.props.navigation.navigate('VideoPlayer',{videoItem : item});
                                    }}>
                                      <View style={{flexDirection : "row"}}> 
                                        <Icon name="ios-play-circle"  style={{ fontSize:35 , color : '#ffffff'}} />
                                      </View>
                                    </TouchableOpacity>
                                  </View>
                                </View> 

                                <Text style={{ flex: 1, flexWrap: 'wrap', fontSize: 14, paddingTop: 5, paddingBottom: 5 }}>{item.question_title}</Text>

                                <Text style={{ flex: 1, flexWrap: 'wrap', fontSize: 10, paddingTop: 5, paddingBottom: 5 }}>
                                 {moment(item.create_date).format('DD-MM-YYYY')}
                                </Text>
                                </Card>
                              </TouchableOpacity>
                            </View>
                          }
                        />
                      </View>
                    </View>}




                  <View style={{ flex: 1, marginLeftL: 10, marginTop: 14 }}>


                    {this.state.jobvideolist.length == 0 ? null :

                      <View style={{ flex: 1 }}>
                        <View style={{ paddingTop: 5, flexDirection: 'row', flex: 1, marginLeft: 10, paddingBottom: 6, }}>
                          <Text style={styles.texth1}>Video About this Job</Text>

                        </View>

                        <View style={{ marginLeft: 10 }}>

                          <FlatList
                            horizontal
                            data={this.state.jobvideolist}
                            keyExtractor={item => item.job_video_id}
                            renderItem={({ item }, index) =>
                              <View key={item.job_video_id}>
                                <TouchableOpacity onPress={() => {
                                  this.props.navigation.navigate('VideoPlayer', { videoItem: item });
                                  //  this.setState({  videoitem : item,videoModel: !this.state.videoModel});
                                }}>
                                  <Card style={{ padding: 10, color: "#0275d8" }}>
                                    <View style={{flex:1, justifyContent: 'center',alignItems: 'center'}}>
                                      <Image source={require('../assest/vidscreen.png')} />
                                      <View style={{justifyContent: 'center',alignItems: 'center', position : "absolute", alignSelf : "center" }}>
                                        <TouchableOpacity onPress={() => {
                                          this.props.navigation.navigate('VideoPlayer',{videoItem : item});
                                        }}>
                                          <View style={{flexDirection : "row"}}> 
                                            <Icon name="ios-play-circle"  style={{ fontSize:35 , color : '#ffffff'}} />
                                          </View>
                                        </TouchableOpacity>
                                      </View>
                                    </View> 
                                    <Text style={{ flex: 1, flexWrap: 'wrap', fontSize: 10, paddingTop: 5, paddingBottom: 5 }}>{moment(item.create_date).format('DD-MM-YYYY')}</Text>
                                  </Card>
                                </TouchableOpacity>
                                
                              </View>
                            }
                          />
                        </View>
                      </View>

                    }


                    <Dialog
                      dialogStyle={{ borderRadius: 5 }}
                      visible={this.state.videoModel}
                      onTouchOutside={() => this.setState({ videoModel: false })} >

                      <View style={{ width: "100%", height: 300 }}>
                        <Text>Question :</Text>
                        <Text>{this.state.videoitem.create_date}</Text>

                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: 'white' }}>

                          <Video controls={true}
                            style={{ height: 300 }}
                            ref={(ref) => { this.video = ref }}

                            source={{ uri: this.state.videoitem.video_url }}

                            style={{
                              position: 'absolute',
                              top: 0,
                              left: 0,
                              bottom: 0,
                              right: 0
                            }}
                          />

                        </View>



                        {/* <View style={{width:"100%",height : 300}}>
                    <Video
                          source={{uri : this.state.videoitem.video_url}}
                          resizeMode="contain"
                          style={StyleSheet.absoluteFill}
                    />
                    </View> */}
                        <Button onPress={() => { this.setState({ videoModel: false }); }} style={{ width: "100%" }} ><Text style={{ textAlign: 'center', width: '100%' }}>Close</Text></Button>

                      </View>
                    </Dialog>
                  </View>
                
                  <View style={{ flex: 1, flexDirection: 'row', margin: 5, alignItems: 'flex-start', justifyContent: 'flex-start', marginBottom: 10, marginTop: 20, }}>
                    {this.state.notInterestVisible ?
                    <TouchableOpacity onPress={() => {
                      Alert.alert(
                        'Gradhire',
                        'Are you sure you want to delete this job?',
                        [
                          {
                            text: 'Cancel',
                            onPress: () => console.log('Cancel Pressed'),
                            style: 'cancel',
                          },
                          { text: 'OK', onPress: () => { this.delete(); console.log('OK Pressed') } },
                        ]
                      );
                    }}>
                      <View style={styles.outView} >
                        <Text style={{ color: '#8bb9ff', paddingLeft: 10, paddingRight: 10, }}>Delete Job</Text>
                      </View>
                    </TouchableOpacity>
                     : null }
                    <TouchableOpacity onPress={() => {
                      Alert.alert(
                        'Gradhire',
                        'Are you sure this is an Inappropriate Job?',
                        [
                          {
                            text: 'Cancel',
                            onPress: () => console.log('Cancel Pressed'),
                            style: 'cancel',
                          },
                          { text: 'OK', onPress: () => { this.inapprocated(); console.log('OK Pressed') } },
                        ]
                      );
                    }}>
                      <View style={[styles.outView1,{ backgroundColor : "red" }]} >
                        <Text style={{ color: '#ffffff', paddingLeft: 10, paddingRight: 10, fontSize:14 }}>Report as Inappropriate</Text>
                      </View>
                    </TouchableOpacity>
                  </View>

                
                </Card>
              </Form>
            </Content>
          </Container>
        );
    }
  }
}

const styles = StyleSheet.create({
  spanType: { flex: 1, flexDirection: 'row', flexWrap: 'wrap', padding: 3, fontSize: 13, marginBottom: 5},
  vieMap: { backgroundColor: '#8bb9ff', flexDirection: 'row', flexWrap: 'wrap', color: '#fff', paddingLeft: 10, paddingRight: 10, padding: 3, marginLeft: 5, borderRadius: 20, fontSize: 14, },
  cardNew: { borderRadius: 10, backgroundColor: '#fff', padding: 10, },
  outView: { alignItems: 'center', borderRadius: 4, borderColor: '#8bb9ff', padding: 3, borderWidth: 1, marginRight: 5, color: '#8bb9ff', backgroundColor: 'transparent', },
  outView1: { alignItems: 'center', borderRadius: 4, borderColor: 'red', padding: 3, borderWidth: 1, padding: 3, marginLeft: 5, color: '#333', backgroundColor: 'transparent', },
  solView: { alignItems: 'center', borderRadius: 4, marginLeft: 5, color: '#fff', backgroundColor: '#1f6fe7' },
  texth1: { fontWeight: 'bold', color: '#000' },
  carditn: { paddingTop: 5, paddingBottom: 5, paddingLeft: 10, paddingRight: 10, color: "#0275d8", borderRadius: 10, flex: 1 },
  btntake: { backgroundColor: '#1f6fe7', fontSize: 12, textAlign: 'center', borderRadius: 5, height: 24, flex: 1 },
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover', // or 'stretch'
  },
  loginForm: {

    position: 'absolute',

    backgroundColor: 'transparent', width: '94%', height: '100%',
  },
  container: {

    flex: 1,
    backgroundColor: 'transparent',
    alignItems: 'center', justifyContent: 'center',



  },
  mailicon: {
    height: 10,
    width: 20,

  },

  loginContainer: {
    alignItems: 'center',
    flexGrow: 1,
    justifyContent: 'center'
  },
  logo: {
    flex: 1,
    alignItems: 'center',
    width: 300,
    height: 100,
    justifyContent: 'center', marginLeft: 50,

  },
  input: {
    fontSize: 16,

    color: 'black',
    marginBottom: 10, paddingTop: 20,
    borderBottomColor: '#1f6fe7',
    borderBottomWidth: 1

  },
  MainContainer: {

    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: (Platform.OS == 'ios') ? 20 : 0

  },

  Alert_Main_View: {

    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: "#009688",
    height: 200,
    width: '90%',
    borderWidth: 1,
    borderColor: '#fff',
    borderRadius: 7,

  },

  Alert_Title: {

    fontSize: 25,
    color: "#fff",
    textAlign: 'center',
    padding: 10,
    height: '28%'

  },

  Alert_Message: {

    fontSize: 22,
    color: "#fff",
    textAlign: 'center',
    padding: 10,
    height: '42%'

  },

  buttonStyle: {

    width: '50%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center'

  },

  TextStyle: {
    color: '#fff',
    textAlign: 'center',
    fontSize: 22,
    marginTop: -5
  }


})




export default Companyjobdetails;
