'use strict';
import React, { PureComponent} from 'react';
import { StyleSheet, Text, TouchableOpacity, View,ActivityIndicator,BackHandler} from 'react-native';
import CountDown from 'react-native-countdown-component';
import { RNCamera } from 'react-native-camera';


class ExampleApp extends PureComponent {


  constructor(props){
    super(props);
      this.state = {
        recording : false,
        processing : false,
        timerRunning : false,
        duration: 0,
        count_color : '#000000'
      }
  }

  static navigationOptions = {
    header : null
    };

    componentDidMount(){
      BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
      const { duration } = this.props.navigation.state.params;
         var d = parseInt(duration);
          this.setState({duration: d});
    }

    componentWillUnmount() {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
     }
     
     handleBackButton = () => {
          this.props.navigation.goBack();
       return true;
     }


  async startRecording() {
    this.setState({ recording: true,timerRunning: true});
    // default to mp4 for android as codec is not set
            
    const dat = await this.camera.recordAsync({ 
      //ratio: '4:3',
      quality: RNCamera.Constants.VideoQuality['288p'],
      orientation :RNCamera.Constants.Orientation['portrait'],
      videoBitrate : 1*1000*1000
    });
        console.log('file video : ',dat);
                  const { uri } = dat;
                    console.log('uri : ',uri);
                   var filename = uri.replace(/^.*[\\\/]/, '')
                    console.log('file Name : ',filename);
                    var codec = filename.split('.').pop(); 
                    this.setState({ recording: false, processing: false,timerRunning: false });
            
                      console.log('codec : ',codec);
                      var type = `video/${codec}`;
                  
                     this.props.navigation.state.params.loadVideo(uri,type,filename);
                     this.props.navigation.goBack();


}

stopRecording() {
    this.camera.stopRecording();
}

go_back() {
  this.props.navigation.goBack();
}

checkstyle(){
  console.log(this.state.duration);
  this.setState({count_color : 'red'});
}

render() {


  const { recording, processing } = this.state;

  let button = (
    <View style={{flexDirection : 'row'}}>
      <TouchableOpacity
        onPress={this.go_back.bind(this)}
        style={styles.capture}
      >
        <Text style={{ fontSize: 14 }}> CANCEL </Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={this.startRecording.bind(this)}
        style={styles.capture}
      >
        <Text style={{ fontSize: 14 }}> RECORD </Text>
      </TouchableOpacity>
      
    </View>
  );

  if (recording) {
    button = (
      <TouchableOpacity
        onPress={this.stopRecording.bind(this)}
        style={styles.capture}
      >
        <Text style={{ fontSize: 14 }}> STOP </Text>
      </TouchableOpacity>
    );
  }

  if (processing) {
    button = (
      <View style={styles.capture}>
        <ActivityIndicator animating size={18} />
      </View>
    );
  }


  return (
    <View style={styles.container}>
      <RNCamera
        ref={ref => {
          this.camera = ref;
        }}
        autoFocusPointOfInterest={{ x: 0.5, y: 0.5 }}
        style={styles.preview}
        type={RNCamera.Constants.Type.front}
        androidCameraPermissionOptions={{
          title: 'Permission to use camera',
          message: 'We need your permission to use your camera',
          buttonPositive: 'Ok',
          buttonNegative: 'Cancel',
        }}
        androidRecordAudioPermissionOptions={{
          title: 'Permission to use audio recording',
          message: 'We need your permission to use your audio',
          buttonPositive: 'Ok',
          buttonNegative: 'Cancel',
        }}
       
      />
     <View
        style={{ flex: 0, flexDirection: "row", justifyContent: "center" }}
      >
        {button}
        <View style={{bottom: 0,right: 0,top: 0,justifyContent: 'center',marginTop : 15}}>
                  <CountDown
                  until={this.state.duration}
                  size={15}
                  running={this.state.timerRunning}
                  onFinish={() => {this.stopRecording();}}
                  digitStyle={{backgroundColor: '#FFF'}}
                  //digitTxtStyle={{color:  this.state.count_color }}
                  timeToShow={['M', 'S']}
                  //onChange = {() => {this.checkstyle()}}
                />
        </View>
     
      </View>
    </View>
  );
}






}

const styles = StyleSheet.create({
container: {
  flex: 1,
  flexDirection: 'column',
  backgroundColor: 'black',
},
preview: {
  flex: 1,
  justifyContent: 'flex-end',
  alignItems: 'center',
},
capture: {
  flex: 0,
  backgroundColor: '#fff',
  borderRadius: 5,
  padding: 10,
  paddingHorizontal: 10,
  alignSelf: 'center',
  margin: 10,
},
});



export default ExampleApp;
