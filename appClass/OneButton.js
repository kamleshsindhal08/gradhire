import React, { Component } from 'react';
import {
  Button
} from 'react-native';


class OneButton extends React.Component {
    static navigationOptions = {
      title: 'Welcome',
    };
    render() {
      const {navigate} = this.props.navigation;
      return (
        <Button
          title="Go to Jane's profile"
          onPress={() => navigate('CameraControl') }
        />
      );
    }
  }

  export default OneButton;