import React, { Component } from 'react';
import { Image,TouchableOpacity ,BackHandler,StyleSheet,ActivityIndicator} from 'react-native';
import { Container,DatePicker,Picker, CheckBox,Textarea,View,Toast, Header, Content, Form, Item, Input, Label,Button,Text, Card, CardItem, Thumbnail, Icon, Left, Body, Right} from 'native-base';
const {baseUrl} = require('./baseurl/Url');


class EditWorkExp extends React.Component {

  constructor(props) {
    super(props);
    this.state = {

      title_exp:'',
      company_exp:'',
      location_exp:'',

      title_exp_Error : false,
      company_exp_Error : false,
      location_exp_Error : false,

      from_month_exp: '',
      from_year_exp: '',
      from_month_Error : false,
      from_year_Error : false,

      to_month_exp: '',
      to_year_exp: '',
      to_month_Error : false,
      to_year_Error : false,
      isRefresh : true,
      submitDisabled : false,


      description:'',
      description_Error : false,
      isChecked : true,

      from_year_expList : [
        //{ year : "Year"},
        { year : "1990"},
        { year : "1991"},
        { year : "1992"},
        { year : "1993"},
        { year : "1995"},
        { year : "1996"},
        { year : "1997"},
        { year : "1998"},
        { year : "1999"},
        { year : "2000"},
        { year : "2001"},
        { year : "2002"},
        { year : "2003"},
        { year : "2004"},
        { year : "2005"},
        { year : "2006"},
        { year : "2007"},
        { year : "2008"},
        { year : "2009"},
        { year : "2010"},
        { year : "2011"},
        { year : "2012"},
        { year : "2013"},
        { year : "2014"},
        { year : "2015"},
        { year : "2016"},
        { year : "2017"},
        { year : "2018"},
        { year : "2019"},
     ],

      from_month_expList : [
        //{ month : "Month"},
        { month : "January"},
        { month : "February"},
        { month : "March"},
        { month : "April"},
        { month : "May"},
        { month : "June"},
        { month : "July"},
        { month : "August"},
        { month : "September"},
        { month : "October"},
        { month : "November"},
        { month : "December"},
      ],
      
      to_year_expList : [
        //{ year : "Year"},
        { year : "1990"},
        { year : "1991"},
        { year : "1992"},
        { year : "1993"},
        { year : "1995"},
        { year : "1996"},
        { year : "1997"},
        { year : "1998"},
        { year : "1999"},
        { year : "2000"},
        { year : "2001"},
        { year : "2002"},
        { year : "2003"},
        { year : "2004"},
        { year : "2005"},
        { year : "2006"},
        { year : "2007"},
        { year : "2008"},
        { year : "2009"},
        { year : "2010"},
        { year : "2011"},
        { year : "2012"},
        { year : "2013"},
        { year : "2014"},
        { year : "2015"},
        { year : "2016"},
        { year : "2017"},
        { year : "2018"},
        { year : "2019"},
       ],

      to_month_expList : [
        //{ month : "Month"},
        { month : "January"},
        { month : "February"},
        { month : "March"},
        { month : "April"},
        { month : "May"},
        { month : "June"},
        { month : "July"},
        { month : "August"},
        { month : "September"},
        { month : "October"},
        { month : "November"},
        { month : "December"},
      ],

     
    };

  }
  
    static navigationOptions = {
      title: 'Edit Work Exprience',
    };


    componentDidMount(){
      const { id } = this.props.navigation.state.params;
      fetch(baseUrl+'/mobile/api/single_work_experience_details', {
        method: 'POST',
        headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
        },
      body: JSON.stringify({
            id : id
        }),
    })
   .then((response) => {
       response.json().then(respond => {
         console.log(respond);
           if(respond.status == "1"){
              var to_year = respond.data.to_year;
               if(to_year == "0"){
                   to_year = "";
              } 
              var currently = (respond.data.currently_work == "on")? true : false;
                this.setState({ 
                      title_exp : respond.data.title,
                      company_exp : respond.data.company,
                      location_exp : respond.data.location,
                      from_month_exp : respond.data.from_month,
                      from_year_exp : respond.data.from_year,
                      to_month_exp : respond.data.to_month,
                      to_year_exp : to_year,
                      description : respond.data.description,
                      isChecked : currently,
                      isRefresh : false
                    });
           }
        
       })
     }).catch(error => {
      console.error(error);
    });
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }


     
     componentWillUnmount() {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
     }
     
     handleBackButton = () => {
          this.props.navigation.goBack();
       return true;
     } 
    

    expAdd = ()=> {

      var title_exp = this.state.title_exp.trim();
      var company_exp = this.state.company_exp.trim();
      var location_exp = this.state.location_exp.trim();
      var description = this.state.description.trim();

      console.log('from_month_exp : ',this.state.from_month_exp);
      console.log('from_year_exp : ',this.state.from_year_exp);
      console.log('to_month_exp : ',this.state.to_month_exp);
      console.log('to_year_Error : ',this.state.to_year_Error);
     
      console.log('title_exp  : ',title_exp);
      console.log('company_exp  : ',company_exp);
      console.log('location_exp  : ',location_exp);
      console.log('description  : ',description);

     

      if(title_exp == ""){
        console.log('Title : ',);
        this.setState({ title_exp_Error : true});   
        return; 
      }
      this.setState({ title_exp_Error : false});   
      if(company_exp == ""){
        console.log('company : ',);
        this.setState({ company_exp_Error : true});   
        return; 
      }
      this.setState({ company_exp_Error : false}); 
      if(location_exp == ""){
        console.log('location : ',);
        this.setState({ location_exp_Error : true});   
        return; 
      }
      this.setState({ location_exp_Error : false});   
      if(this.state.from_month_exp == "" || this.state.from_month_exp === "Month"){
        console.log('from_month_exp validate : ',this.state.from_month_exp);
          this.setState({ from_month_Error : true});
        return;
      }
        this.setState({ from_month_Error : false});
        if(this.state.from_year_exp === "" || this.state.from_year_exp === "Year"){
        console.log('to validate : ',this.state.from_year_exp);
        this.setState({ from_year_Error : true});
        return;
        }
        this.setState({ from_year_Error : false});
        
         if(!this.state.isChecked){
          if(this.state.to_month_exp === "" || this.state.to_month_exp === "Month"){
            console.log('to_month_exp validate : ',this.state.to_month_exp);
              this.setState({ to_month_Error : true});
            return;
            }
            this.setState({ to_month_Error : false});
    
            if(this.state.to_year_exp === "" || this.state.to_year_exp === "Year"){
            console.log('to_Year_exp validate : ',this.state.to_year_exp);
            this.setState({ to_year_Error : true});
            return;
            }
            this.setState({ to_year_Error : false});

          //    var to = parseInt(this.state.to_year_exp);
          //  var from = parseInt(this.state.from_year_exp);
      
          //   if(from >= to){
          //           console.log('from > to : ',this.state.to_year_exp);
          //           this.setState({ to_year_Error : true});
          //           Toast.show({
          //             text: 'Please Select a value greater than',
          //             type: "danger"
          //           });
          //           return;
          //   }
          //   this.setState({ to_year_Error : false});
         }
      


         console.log('else part');
           const { id } = this.props.navigation.state.params;
           this.setState({submitDisabled: true});
           console.log('work id : ',id);
        var current_work = (this.state.isChecked)? "1" : "0";
        var toMonth = (this.state.isChecked)? "" : this.state.to_month_exp;   
        var toYear = (this.state.isChecked)? "" : this.state.to_year_exp;  
  
  
  
        fetch(baseUrl+'/mobile/api/edit_candidate_work_experience', {
        method: 'POST',
        headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
        },
      body: JSON.stringify({
          // 
         title: title_exp,
         company: company_exp,
         location: location_exp,
         from_month : this.state.from_month_exp,
         from_year :this.state.from_year_exp,
         to_month: toMonth,
         to_year : toYear,
         currently_work: current_work,
         workCandidateId : id,
         description: description
        }),
    })
   .then((response) => {
       response.json().then(respond => {
         console.log(respond);
         this.setState({submitDisabled: false});
                    if(respond.status == "1"){
                      this.props.navigation.state.params.getWorkExpData();
                      this.props.navigation.goBack();
                    Toast.show({
                      text: respond.message,
                      type: "success"
                    });
                  }else {
                    Toast.show({
                      text: respond.message,
                      type: "danger"
                    });
                  }
       })
     }).catch(error => {
          this.setState({submitDisabled: false});
          console.error(error);
    });

     }

    
 
    onValueChangeToMonth(value) {
      this.setState({
        to_month_exp: value
      });
    }
    onValueChangeToYear(value) {
      this.setState({
        to_year_exp: value
      });
    }
    onValueChangeFromMonth(value) {
      this.setState({
        from_month_exp: value
      });
    }
    onValueChangeFromYear(value) {
      this.setState({
        from_year_exp: value
      });
    }


  
   
    
    render() {
      const {navigate} = this.props.navigation;
      
      if(this.state.isRefresh){
        return <View style={{flex : 1,justifyContent : 'center'}}><ActivityIndicator size="large" color="#0000ff" /></View>;
 }else {

      return (

<Container style={styles.container}>
        <Image source={require('../assest/login_bg.png')} style={styles.backgroundImage} />
        <Content style={styles.loginForm}>
        <View style={{width : "95%" , alignSelf : "center"}}>
          <Form>

          <View style={{ color:'#1f6fe7',paddingTop : 10}}>
            <View floatingLabel style={styles.labunder}>
              <Label style={{color : (this.state.title_exp_Error)?'red' : '#1f6fe7'}}>Title</Label>
            <Input style={styles.input} value={this.state.title_exp} onChangeText={(title_exp)=> this.setState({title_exp})} />
            </View>
            </View>

            <View style={{ color:'#1f6fe7'}}>
            <View floatingLabel style={styles.labunder}>
              <Label  style={{color : (this.state.company_exp_Error)?'red' : '#1f6fe7'}}>Company</Label>
            <Input style={styles.input} value={this.state.company_exp} onChangeText={(company_exp)=> this.setState({company_exp})} />
            </View>
            </View>

            <View style={{ color:'#1f6fe7'}}>
            <View floatingLabel style={styles.labunder}>
              <Label  style={{color : (this.state.location_exp_Error)?'red' : '#1f6fe7'}}>Location</Label>
            <Input  style={styles.input} value={this.state.location_exp} onChangeText={(location_exp)=> this.setState({location_exp})} />
            </View>
            </View>
            <View>
            <Label style={{paddingTop: 10, color:'#000', fontWeight:'bold'}}>From</Label></View>
            <View>
            <Label style={{ color:'#1f6fe7',paddingTop: 10,}}>Month</Label>
            </View>
            <View  style={styles.labunder}>
            <Item picker>
              <Picker
                mode="dropdown"
                iosIcon={<Icon name="arrow-down" />}
                style={{ height: 40, color : (this.state.from_month_Error)?'red' : '#333' }}
                placeholder="Select Year"
                placeholderStyle={{ color: "#64a8e3" }}
                placeholderIconColor="#0275d8"
                selectedValue={this.state.from_month_exp}
                onValueChange={this.onValueChangeFromMonth.bind(this)}
              >
               {this.state.from_month_expList.map((item,index)=> {
                    return (<Picker.Item key={index} label={item.month} value={item.month} />)
                })}
                </Picker>
            </Item>
            </View>


            <View><Label style={{ color:'#1f6fe7'}}>Year</Label></View>
            <View  style={styles.labunder}>
            <Item picker>
              <Picker
                mode="dropdown"
                iosIcon={<Icon name="arrow-down" />}
                style={{ height: 40, color : (this.state.from_year_Error)?'red' : '#333' }}
                placeholder="Select Year"
                placeholderStyle={{ color: "#64a8e3" }}
                placeholderIconColor="#0275d8"
                selectedValue={this.state.from_year_exp}
                onValueChange={this.onValueChangeFromYear.bind(this)}
              >
               {this.state.from_year_expList.map((item,index)=> {
                    return (<Picker.Item key={index} label={item.year} value={item.year} />)
                })}
                </Picker>
            </Item>
            </View>

                

                
          
            <View style={{paddingTop: 15 , paddingBottom:15, flexDirection: 'row'}} > 
             <CheckBox
                  checked={this.state.isChecked}
                  style={{marginLeft: 0}}
                  onPress={()=>{
                    this.setState(function (prevState) {
                      return {
                          isChecked: !prevState.isChecked
                      };
                    });
                  }}
                  //value={this.state.mailingListChecked}
              />     
            <Text style={{color: '#0275d8', marginLeft:20}}>I am Currently Working Here</Text>
            </View>

            <View style={{display : (this.state.isChecked)? 'none' : 'flex'}}>
            <Label style={{paddingTop: 10, color:'#000', fontWeight:'bold'}}>To</Label> 
          <View><Label style={{color:'#1f6fe7',paddingTop: 10}}>Month</Label></View>
          <View  style={styles.labunder}>
          <Item picker>
            <Picker
              mode="dropdown"
              iosIcon={<Icon name="arrow-down" />}
              style={{ height: 40, color : (this.state.to_month_Error)?'red' : '#333' }}
              placeholder="Select Year"
              placeholderStyle={{ color: "#64a8e3" }}
              placeholderIconColor="#0275d8"
              selectedValue={this.state.to_month_exp}
              onValueChange={this.onValueChangeToMonth.bind(this)}
            >
             {this.state.to_month_expList.map((item,index)=> {
                  return (<Picker.Item key={index} label={item.month} value={item.month} />)
              })}
              </Picker>
          </Item>
          </View>


          <View><Label style={{color:'#1f6fe7',}}>Year</Label></View>
          <View  style={styles.labunder}>
          <Item picker>
            <Picker
              mode="dropdown"
              iosIcon={<Icon name="arrow-down" />}
              style={{ height: 40, color : (this.state.to_year_Error)?'red' : '#333' }}
              placeholder="Select Year"
              placeholderStyle={{ color: "#64a8e3" }}
              placeholderIconColor="#0275d8"
              selectedValue={this.state.to_year_exp}
              onValueChange={this.onValueChangeToYear.bind(this)}
            >
             {this.state.to_year_expList.map((item,index)=> {
                  return (<Picker.Item key={index} label={item.year} value={item.year} />)
              })}
              </Picker>
            </Item>
            </View>

            </View>

            <View  style={styles.labunder}>
              <Text style={{ paddingTop: 10,color : (this.state.description_Error)?'red' : '#1f6fe7'}}>Description</Text>
              <Textarea rowSpan={4} bordered 
              value={this.state.description} onChangeText={(description)=> this.setState({description})} />
            </View>
              
            
            <Button block  disabled={this.state.submitDisabled}
              onPress={()=> { this.expAdd(); }}
              style={{marginTop: 10, marginBottom:10}}>
              <Text>SUBMIT</Text>
            </Button>

          </Form>

        </View>      
            </Content>
           
      </Container>
      

        
      );
            }
    }
}

const styles = StyleSheet.create({
  labunder:{flex: 1,  flexDirection: 'row',
  borderBottomWidth: 1,
  borderBottomColor: '#017bc2', marginBottom:5,},
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover', // or 'stretch'
  },
  loginForm: {
    
    position: 'absolute',
    backgroundColor: 'transparent', width: '100%', height: '100%',
  },
  container: {

    flex: 1,
    backgroundColor: 'transparent',
    alignItems: 'center', justifyContent: 'center',



  },


  loginContainer: {
    alignItems: 'center',
    flexGrow: 1,
    justifyContent: 'center'
  },
  form1:{ flex: 1, paddingLeft:0 },
  btnchose: { backgroundColor: '#1f6fe7', fontSize:14,textAlign: 'center', borderRadius: 5,height: 24,},
  input:{
    color: '#333',
    height: 40
  },
  labunder:{
    borderBottomWidth: 1,
    borderBottomColor: '#017bc2', marginBottom:5,},
    cusin:{ paddingTop:10,},

})


export default EditWorkExp;
