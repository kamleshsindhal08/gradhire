import React, { PureComponent } from 'react';
import {
  Button,View,Text,BackHandler
} from 'react-native';
import Video from "react-native-video";
import LightVideo from "../assest/one.mp4";

class VideoPlayer extends PureComponent {
    static navigationOptions = {
      title: 'Video',
    };


    constructor(props){
      super(props);
    }

    componentDidMount() {
      BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
     }
     
     componentWillUnmount() {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
     }
     
     handleBackButton = () => {
          this.props.navigation.goBack();
       return true;
     } 


    render() {
         var {videoItem, question } = this.props.navigation.state.params;
            console.log('video player : ',this.props);
      return ( <View style={{flex : 1}}>
             <Video controls={true}
                    style={{ height: 300 }}
                    ref={(ref) => { this.video = ref }}
                    paused = {true}
                    source={{ uri:  videoItem.video_url }}
                    ignoreSilentSwitch = {"ignore"}
                    // source={LightVideo} 
                    />
              { (question) ?
              <View>
                <Text style={{ padding : 10,marginTop : 10 ,fontSize : 15}}>Question :</Text>
                <Text style={{ paddingLeft : 8 ,fontSize : 12}}>{videoItem.ques_name}</Text>
              </View>
              : null
              }
      </View>
      );
    }
  }

  export default VideoPlayer;