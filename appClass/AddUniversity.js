import React, { Component } from 'react';
import { Image,TouchableOpacity,BackHandler,StyleSheet,View, Platform } from 'react-native';
import { Container,DatePicker,Picker,Toast, Header, Content, Form, Item, Input, Label,Button,Text, Card, CardItem, Thumbnail, Icon, Left, Body, Right} from 'native-base';
const {baseUrl} = require('./baseurl/Url');


class AddUniversity extends React.Component {


  static navigationOptions = {
    title: 'Add University',
  };



  constructor(props) {
    super(props);
    this.state = {
      from: '',
      to: '',
      university: '',
      coursetype :'',
      coursename :'',
      stage:'',
      grade:'',

      //error validation
      fromError : false,
      toError : false,
      universityError : false,
      courseTypeError : false,
      courseNameError : false,
      stageError : false,
      submitDisabled: false,



      fromList : [
        { year : "Year"},
        { year : "2015"},
        { year : "2016"},
        { year : "2017"},
        { year : "2018"},
        { year : "2019"}
      ],
      toList : [
        { year : "Year"},
        { year : "2017"},
        { year : "2018"},
        { year : "2019"}
      ],
      universityList : [],
      courseTypeList : [],
      stageList : []

    };

      this.universityAdd = this.universityAdd.bind(this);
  }


  componentDidMount(){

    fetch(baseUrl+"/mobile/api/mst_university_detail")
    .then(response => {
      response.json().then(respond => {
        console.log('response ',respond);
         if(respond.status === "1"){
             this.setState({ universityList : respond.data });
         }  
      })
   })
   .catch(error => {
     console.error(error);
   });


   fetch(baseUrl+"/mobile/api/mst_course_type_detail")
   .then(response => {
     response.json().then(respond => {
       console.log('response ',respond);
        if(respond.status === "1"){
            this.setState({ courseTypeList : respond.data });
        }  
     })
  })
  .catch(error => {
    console.error(error);
  });


    fetch(baseUrl+"/mobile/api/mst_stage_detail")
   .then(response => {
     response.json().then(respond => {
       console.log('response ',respond);
        if(respond.status === "1"){
            this.setState({ stageList : respond.data });
        }  
     })
  })
  .catch(error => {
    console.error(error);
  });
  BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }


   
   componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
   }
   
   handleBackButton = () => {
        this.props.navigation.goBack();
     return true;
   } 



  universityAdd = ()=> {

      var courseName = this.state.coursename.trim();
       console.log('from : ',this.state.from);
       console.log('to : ',this.state.to);
       console.log('university : ',this.state.university);
       console.log('coursetype : ',this.state.coursetype);
       console.log('coursename : ',courseName);
       console.log('stage : ',this.state.stage);
       console.log('grade : ',this.state.grade);


       if(this.state.from === "" || this.state.from === "Year"){
            console.log('from validate : ',this.state.from);
              this.setState({ fromError : true});
            return;
       }
       this.setState({ fromError : false});
       if(this.state.to === "" || this.state.to === "Year"){
        console.log('to validate : ',this.state.to);
        this.setState({ toError : true});
        return;
       }
       this.setState({ toError : false});
       
      //  var to = parseInt(this.state.to);
      //  var from = parseInt(this.state.from);
      //  if(from >= to){
      //   console.log('from > to  validate  to : '+this.state.to+' , from : '+this.state.from);
      //   this.setState({ toError : true});
      //   return;
      //  }
      //  this.setState({ toError : false});

       if(this.state.university === "" || this.state.university === "Select"){
        console.log('university validate : ',this.state.university);
        this.setState({ universityError : true});
        return;
      }
      this.setState({ universityError : false});
      
      if(this.state.coursetype === "" || this.state.coursetype === "Select"){
        console.log('coursetype validate : ',this.state.coursetype);
        this.setState({ courseTypeError : true});
        return;
      }
      this.setState({ courseTypeError : false});
      if(courseName === ""){
        console.log('coursename validate : ',this.state.coursename);
        this.setState({ courseNameError : true});
        return; 
      }
      this.setState({ courseNameError : false });
      if(this.state.stage === "" || this.state.stage === "Select"){
        console.log('stage validate : ',this.state.stage);
        this.setState({ stageError : true});
        return;
      }
      this.setState({ stageError : false});
      console.log('else part');
          this.setState({ submitDisabled: true });
            const { candidate_id } = this.props.navigation.state.params;
            console.log('candidate id : ',candidate_id);
     fetch(baseUrl+'/mobile/api/candidate_education', {
          method: 'POST',
          headers: {
                  Accept: 'application/json',
                  'Content-Type': 'application/json',
          },
        body: JSON.stringify({
              uni_from_year : this.state.from,
              uni_to_year : this.state.to,
              uni_name : this.state.university,
              course_name : courseName,
              course_ype : this.state.coursetype,
              stage : this.state.stage,
              grade : this.state.grade,
              candidate_profile_id : candidate_id
          }),
      })
     .then((response) => {
         response.json().then(respond => {
           console.log(respond);
           this.setState({ submitDisabled: false });
                  if(respond.status == "1"){
                    this.props.navigation.state.params.getUniversityData();
                    this.props.navigation.goBack();
                  Toast.show({
                    text: respond.message,
                    type: "success"
                  });
                }else {
                  Toast.show({
                    text: respond.message,
                    type: "danger"
                  });
                }
                
        
         })
       }).catch(error => {
        this.setState({ submitDisabled: false });
        console.error(error);
      });

      }

    
     fromValueChange(s) {
      this.setState({ from: s});
    }
    toValueChange(s) {
      this.setState({
        to: s
      });
    }
    onUniversityChange(s) {
      console.log('select university : ',s)
      this.setState({
        university: s
      });
    }
    onCourseTypeChange(s) {
      this.setState({
        coursetype: s
      });
    }
    onStageChange(s) {
      this.setState({
        stage: s
      });
    }
    onValueChangeGrade(s) {
      this.setState({
        grade: s
      });
    }
    render() {
        // console.log('Add University Props ',this.props);
        //  const { candidate_id } = this.props.navigation.state.params;
        //   console.log('candidate_id  ',candidate_id);
      return (

       
        <Container style={styles.container}>
           <Image source={require('../assest/login_bg.png')} style={styles.backgroundImage} />
           <Content style={styles.loginForm}>
           <View style={{width : "95%" , alignSelf : "center", justifyContent : "center" }}>
          <Form>

          <View style={{ paddingTop : 10 }}>
            <View >
          
            <Label style={{paddingTop: 0, color:'#1f6fe7',}}>From</Label>
            </View>
            
            <View  style={styles.labunder}>
            <View picker  style={{ flex: 1,flexDirection: 'row'}}>
              <Picker
                mode="dropdown"
                iosIcon={<Icon name="arrow-down" />}
                style={{  height:40, flex: 1, color : (this.state.fromError)?'red' : '#333'}}
                placeholder="Select Year"
                placeholderStyle={{ color: "#64a8e3" }}
                placeholderIconColor="#0275d8"
                selectedValue={this.state.from}
                onValueChange={(from)=> {  this.fromValueChange(from);  }} >

                {this.state.fromList.map((item,index)=> {
                    return (<Picker.Item key={index} label={item.year} value={item.year} />)
                })}

                </Picker>
            </View> 
            </View>
           
         

            <View>
            <Label style={{paddingTop: 0,color:'#1f6fe7'}}>To</Label>
            </View>
            <View  style={styles.labunder}>
            <View picker>
              <Picker
                mode="dropdown"
                iosIcon={<Icon name="arrow-down" />}
                style={{  height:40, color : (this.state.toError)?'red' : '#333' }}
                placeholder="Select Year"
                placeholderStyle={{ color: "#64a8e3" }}
                placeholderIconColor="#0275d8"
                selectedValue={this.state.to}
                onValueChange={(text)=> {this.toValueChange(text);}}
              >

              {this.state.toList.map((item,index)=> {
                    return (<Picker.Item key={index} label={item.year} value={item.year} />)
                })}
                </Picker>
            </View>
            </View>

            {(this.state.toError)?
              <Text style={{fontSize : 12,color : 'red'}}>{(this.state.toError)?"Please select value greater than" : ""}</Text> : null
            }

            <View><Label style={{color:'#1f6fe7' , paddingTop:0 }}>University</Label></View>
            <View  style={styles.labunder}>
            <Item picker>
              <Picker
                mode="dropdown"
                iosIcon={<Icon name="arrow-down" />}
                style={{  height:40, color : (this.state.universityError)?'red' : '#333'}}
                placeholder="Select University"
                placeholderStyle={{ color: "#64a8e3" }}
                placeholderIconColor="#0275d8"
                selectedValue={this.state.university}
                onValueChange={(text)=> {this.onUniversityChange(text);}}
              >
                   <Picker.Item label="Select" value="Select" />
                  {this.state.universityList.map((item,index)=> {
                    return (<Picker.Item key={index} label={item.mst_university} value={item.uni_id} />)
                })}
                </Picker>
                </Item>
            </View>

            <View>
            <Label style={{paddingTop: 0,color:'#1f6fe7'}}>Course Type</Label>
            </View>
            <View  style={styles.labunder}>
            <Item picker>
              <Picker
                
                mode="dropdown"
                iosIcon={<Icon name="arrow-down" />}
                style={{  height:40, color : (this.state.courseTypeError)?'red' : '#333'}}
                placeholder="Select Course Type"
                placeholderStyle={{ color: "#64a8e3" }}
                placeholderIconColor="#0275d8"
                selectedValue={this.state.coursetype}
                onValueChange={(text)=> {this.onCourseTypeChange(text); }}
              >
                 <Picker.Item label="Select" value="Select" />
                  {this.state.courseTypeList.map((item,index)=> {
                    return (<Picker.Item key={index} label={item.course_type_name} value={item.course_id} />)
                })}
                </Picker>
                </Item>
            </View>

            <View style={{ flex: 1,  paddingTop:0}}>
            <View floatingLabel style={styles.labunder}>
              <Label style={{color:'#1f6fe7', color : (this.state.courseNameError)?'red' : '#1f6fe7', fontSize : 17}}>Course Name</Label>
              <Input style={[styles.input, { paddingLeft : (Platform.OS == "ios") ? 12 : 12 }]} value={this.state.coursename} onChangeText={(coursename)=> this.setState({coursename})}/>
            </View>
            </View>



             <View>
            <Label style={{color:'#1f6fe7'}}>Stage</Label>
            </View>
            <View  style={styles.labunder}>
            <Item picker>
              <Picker
                
                mode="dropdown"
                iosIcon={<Icon name="arrow-down" />}
                style={{  height:40, color : (this.state.stageError)?'red' : '#333' }}
                placeholder="Select Stage"
                placeholderStyle={{ color: "#64a8e3" }}
                placeholderIconColor="#0275d8"
                selectedValue={this.state.stage}
                onValueChange={(text)=> {this.onStageChange(text); }}
              >
               <Picker.Item label="Select" value="Select" />
                  {this.state.stageList.map((item,index)=> {
                    return (<Picker.Item key={index} label={item.stage_name} value={item.stage_id} />)
                })}
                </Picker>
            </Item>
            </View>

           <View><Label style={{paddingLeft: 0,paddingTop: 0,color:'#1f6fe7'}}>Grade</Label></View> 
            <View  style={styles.labunder}>
            <Item picker>
              <Picker
                
                mode="dropdown"
                iosIcon={<Icon name="arrow-down" />}
                style={{  height:40, color : (this.state.stageError)?'red' : '#333' }}
                placeholder="Select Grade"
                placeholderStyle={{ color: "#64a8e3" }}
                placeholderIconColor="#0275d8"
                selectedValue={this.state.grade}
                onValueChange={(text)=> {this.onValueChangeGrade(text); }}
              >
                <Picker.Item label="Select" value="Select" />
                <Picker.Item label="1st" value="1st" />
                <Picker.Item label="2.1" value="2.1" />
                <Picker.Item label="2.2" value="2.2" />
                <Picker.Item label="3rd" value="3rd" />
                </Picker>
            </Item>
            </View>

            <Button block disabled={this.state.submitDisabled} onPress={()=> {this.universityAdd(); }}
           style={{marginTop: 20, marginBottom:10}}>
            <Text>SUBMIT</Text>
          </Button>

          </View>
          </Form>
          </View>
          
      
            </Content>
           
      </Container>
      

        
      );
    }
}
const styles = StyleSheet.create({
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover', // or 'stretch'
  },
  loginForm: {
   
    position: 'absolute',
    backgroundColor: 'transparent', width: '100%', height: '100%',
  },
  container: {

    flex: 1,
    
    backgroundColor: 'transparent',
    alignItems: 'center', justifyContent: 'center',



  },


  loginContainer: {
    alignItems: 'center',
    flexGrow: 1,
    justifyContent: 'center'
  },
  form1:{ flex: 1, paddingLeft:0 },
  btnchose: { backgroundColor: '#1f6fe7', fontSize:14,textAlign: 'center', borderRadius: 5,height: 24,},
  input:{
    color: '#333',
    height : 40
  },
  labunder:{
    borderBottomWidth: 1,
    borderBottomColor: '#017bc2', marginBottom:5,},

    //cusin:{ paddingTop:10,},

})
export default AddUniversity;
