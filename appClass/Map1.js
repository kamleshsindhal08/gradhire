/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text,Dimensions, View} from 'react-native';
import MapView,{PROVIDER_GOOGLE,Marker } from 'react-native-maps';
const {baseUrl} = require('./baseurl/Url');

 class Map1 extends Component {


    constructor(props){
        super(props);
        this.state = {
          lat : 0,
            log : 0
        }
    }



     componentDidMount(){
      console.log('')
            const { job_id } = this.props.navigation.state.params;
            fetch(baseUrl+"/mobile/api/view_map", {
              method: 'POST',
               headers: new Headers({
                 'Accept': 'application/json',
                    'Content-Type': 'application/json',
               }),
              body: JSON.stringify({
                job_id : job_id
              })
           })
           .then(response => {
             response.json().then(respond => {
               console.log('response ',respond);
                if(respond.status === "1"){
                      this.setState({ lat : respond.data.latitude , log : respond.data.logitude });
                      console.log('one this',this.state.lat);
                      console.log('two this',this.state.log);
                }
                 
                    console.log('state data : ',this.state.data);
                    console.log('one this',this.state.lat);
                    console.log('two this',this.state.log);
                   
             })
          })
          .catch(error => {
            console.error(error);
          });
      }

  render() {
    console.log('one this',parseFloat(this.state.lat));
    console.log('two this',parseFloat(this.state.log));
    return (
      <View style={styles.container}>
        { (Platform.OS == 'android') ? 
        <MapView 
          provider = {PROVIDER_GOOGLE}
          style={styles.mapcontainer}
          region={{
            latitude : parseFloat(this.state.lat),
            longitude: parseFloat(this.state.log),
            latitudeDelta:0.01,
            longitudeDelta:0.01,
          }}
          showsUserLocation={true} 
          > 
           <Marker
              coordinate={
                {latitude: parseFloat(this.state.lat),
                longitude: parseFloat(this.state.log)}}
            />
          </MapView>
          :
          <MapView 
          style={styles.mapcontainer}
          region={{
            latitude : parseFloat(this.state.lat),
            longitude: parseFloat(this.state.log),
            latitudeDelta:0.01,
            longitudeDelta:0.01,
          }}
          showsUserLocation={true} 
          > 
           <Marker
              coordinate={
                {latitude: parseFloat(this.state.lat),
                longitude: parseFloat(this.state.log)}}
            />
          </MapView>
          }

        
      </View>
    );
  }
}
var { width, height } = Dimensions.get('window')

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  mapcontainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
});


export default Map1;