import React, { Component } from 'react';
//import react in our code.
import { View, Text,Image, TouchableOpacity  } from 'react-native';
//import all the components we are going to use.
import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';
//import menu and menu item
 
export default class CustomMenuIcon extends Component {
  _menu = null;
  setMenuRef = ref => {
    this._menu = ref;
  };
  showMenu = () => {
    this._menu.show();
  };
  hideMenu = () => {
    this._menu.hide();
  };
  editProfile = () => {
    this._menu.hide();
    this.props.editProfile();
  };
  option2Click = () => {
    this._menu.hide();
    this.props.option2Click();
  };
  changePassword = () => {
    console.log('chnage password');
    this._menu.hide();
    this.props.changePassword();
  };
  option4Click = () => {
    this._menu.hide();
    this.props.option4Click();
  };
  render() {
    return (
      <View style={this.props.menustyle}>
        <Menu
          ref={this.setMenuRef}
          button={
            <TouchableOpacity onPress={this.showMenu}>
           <Image 
              source={require('../assest/menu.png')} 
              style={{width: 25, height: 25,tintColor : 'white'}} 
              
            />
            </TouchableOpacity>
          }>
          <MenuItem onPress={this.editProfile}>Edit Profile</MenuItem>
          <MenuItem onPress={this.changePassword}>Change Password</MenuItem>
          <MenuItem onPress={this.option2Click}>Logout</MenuItem>
          {/* <MenuItem onPress={this.option3Click} disabled>
            op3:Disabled option
          </MenuItem>
          <MenuDivider />
          <MenuItem onPress={this.option4Click}>
            op4:Option After Divider
          </MenuItem> */}
        </Menu>
      </View>
    );
  }
}