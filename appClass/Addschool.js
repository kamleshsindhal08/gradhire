import React, { Component } from 'react';
import { Image,TouchableOpacity,BackHandler,StyleSheet } from 'react-native';
import { Container,DatePicker,Picker,Textarea,Toast, Header, Content, Form, Item, View, Input, Label,Button,Text, Card, CardItem, Thumbnail, Icon, Left, Body, Right} from 'native-base';
const {baseUrl} = require('./baseurl/Url');

class Addschool extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      from: '1990',
      to: '',
      ExamType: '',
      schoolName : '',
      results : '',

      fromError : false,
      toError :false,
      schoolError :false,
      examTypeError :false,
      resultError :false,
      submitDisabled : false,

      fromList : [
        //{ year : "Year"},
        { year : "1990"},
        { year : "1991"},
        { year : "1992"},
        { year : "1993"},
        { year : "1994"},
        { year : "1995"},
        { year : "1996"},
        { year : "1997"},
        { year : "1998"},
        { year : "1999"},
        { year : "2001"},
        { year : "2002"},
        { year : "2003"},
        { year : "2004"},
        { year : "2005"},
        { year : "2006"},
        { year : "2007"},
        { year : "2008"},
        { year : "2009"},
        { year : "2010"},
        { year : "2011"},
        { year : "2012"},
        { year : "2013"},
        { year : "2014"},
        { year : "2015"},
        { year : "2016"},
        { year : "2017"},
        { year : "2018"},
        { year : "2019"}
      ],
      toList : [
       //{ year : "Year"},
        { year : "1990"},
        { year : "1991"},
        { year : "1992"},
        { year : "1993"},
        { year : "1994"},
        { year : "1995"},
        { year : "1996"},
        { year : "1997"},
        { year : "1998"},
        { year : "1999"},
        { year : "2001"},
        { year : "2002"},
        { year : "2003"},
        { year : "2004"},
        { year : "2005"},
        { year : "2006"},
        { year : "2007"},
        { year : "2008"},
        { year : "2009"},
        { year : "2010"},
        { year : "2011"},
        { year : "2012"},
        { year : "2013"},
        { year : "2014"},
        { year : "2015"},
        { year : "2016"},
        { year : "2017"},
        { year : "2018"},
        { year : "2019"}
      ],
      ExamTypeList : [
         { name : "Select" },
         { name : "GCSE - General Certificate of Secondary Education" },
         { name : "A-level(s) / AS-level(s)" },
         { name : "BTEC - Business and Technology Education Council)" },
         { name : "International Baccalaureate" },
         { name : "Cambridge Pre-U" },
         { name : "Eduqas" },
         { name : "Other" },

      ]
    };

    this.schoolAdd = this.schoolAdd.bind(this);

  }
  
    static navigationOptions = {
      title: 'Add School / College',
    };



    componentDidMount() {
      BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
     }
     
     componentWillUnmount() {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
     }
     
     handleBackButton = () => {
          this.props.navigation.goBack();
       return true;
     } 



    schoolAdd = ()=> {


      var school = this.state.schoolName.trim();
      var res = this.state.results.trim();
     
      console.log('from : ',this.state.from);
      console.log('to : ',this.state.to);
      console.log('exam type : ',this.state.ExamType);
      console.log('schoolname  : ',school);
      console.log('results  : ',res);

      if(this.state.from === "" || this.state.from === "Year"){
            console.log('from validate : ',this.state.from);
              this.setState({ fromError : true});
            return;
      }
      this.setState({ fromError : false});
      if(this.state.to === "" || this.state.to === "Year"){
        console.log('to validate : ',this.state.to);
        this.setState({ toError : true});
        return;
      }
      this.setState({ toError : false});
   
  //  var to = parseInt(this.state.to);
  //  var from = parseInt(this.state.from);
  //  if(from >= to){
  //   console.log('from > to  validate  to : '+this.state.to+' , from : '+this.state.from);
  //   this.setState({ toError : true});
  //   return;
  //  }
  //  this.setState({ toError : false});




   if(school == ""){
          console.log('coursetype validate : ',school);
          this.setState({ schoolError : true});   
          return;   
      }
      this.setState({ schoolError : false});   

      if(this.state.ExamType === "" || this.state.ExamType === "Select"){
       console.log('Exam type validate : ',this.state.ExamType);
         this.setState({ examTypeError : true});   
       return;
     }
       this.setState({examTypeError : false});
    
     if(res === ""){
       console.log('coursename validate : ',);
       this.setState({ resultError : true});   
       return; 
     }
     this.setState({ resultError : false});   
  
         console.log('else part');
           const { candidate_id } = this.props.navigation.state.params;
           console.log('candidate id : ',candidate_id);
           this.setState({submitDisabled: true});
    fetch(baseUrl+'/mobile/api/candidate_school_education', {
         method: 'POST',
         headers: {
                 Accept: 'application/json',
                 'Content-Type': 'application/json',
         },
       body: JSON.stringify({
             school_from_year : this.state.from,
             school_to_year : this.state.to,
             school_name : school,
             exam : this.state.ExamType,
             result : res,
             candidate_profile_id : candidate_id
         }),
     })
    .then((response) => {
        response.json().then(respond => {
          console.log(respond);
          this.setState({submitDisabled: false});
                    if(respond.status == "1"){
                      this.props.navigation.state.params.getSchoolData();
                      this.props.navigation.goBack();
                    Toast.show({
                      text: respond.message,
                      type: "success"
                    });
                  }else {
                    Toast.show({
                      text: respond.message,
                      type: "danger"
                    });
                  }
                  
        })
      }).catch(error => {
        this.setState({submitDisabled: false});
       console.error(error);
     });

     }


    
    onValueChange2(value) {
      this.setState({
        from: value
      });
    }
    onValueChange22(value) {
      this.setState({
        to: value
      });
    }
    onValueChange222(value) {
      this.setState({
        ExamType: value
      });
    }
    
    render() {
      const {navigate} = this.props.navigation;
        
      return (
        <Container style={styles.container}>
        <Image source={require('../assest/login_bg.png')} style={styles.backgroundImage} />
        <Content style={styles.loginForm}>
        <View style={{width : "95%" , alignSelf : "center", justifyContent : "center" }}>
          <Form>
          <View >
            <Label style={{ paddingTop : 10, color:'#1f6fe7'}}>From</Label>
            </View>
            <View  style={styles.labunder}>
            <Item picker>
              <Picker
                
                mode="dropdown"
                iosIcon={<Icon name="arrow-down" />}
                style={{ height: 40 , color : (this.state.fromError)?'red' : '#333' }}
                placeholder="Select Year"
                placeholderStyle={{ color: "#64a8e3" }}
                placeholderIconColor="#0275d8"
                selectedValue={this.state.from}
                onValueChange={this.onValueChange2.bind(this)}
              >
                 {this.state.fromList.map((item,index)=> {
                    return (<Picker.Item key={index} label={item.year} value={item.year} />)
                })}
                </Picker>
            </Item>
            </View>


            <View ><Label style={{ color:'#1f6fe7'}}>To</Label></View>
            <View  style={styles.labunder}>
            <Item picker>
              <Picker
                
                mode="dropdown"
                iosIcon={<Icon name="arrow-down" />}
                style={{ height: 40  , color : (this.state.toError)?'red' : '#333'}}
                placeholder="Select Year"
                placeholderStyle={{ color: "#64a8e3" }}
                placeholderIconColor="#0275d8"
                selectedValue={this.state.to}
                onValueChange={this.onValueChange22.bind(this)}
              >
               {this.state.toList.map((item,index)=> {
                    return (<Picker.Item key={index} label={item.year} value={item.year} />)
                })}
                </Picker>
                </Item>
            </View>

            {(this.state.toError)?
            <Text style={{fontSize : 12,color : 'red'}}>{(this.state.toError)?"Please select value greater than" : ""}</Text> : null
            }

            <View style={{ color:'#64a8e3'}}>
            <View floatingLabel style={styles.labunder}>
              <Label  style={{color : (this.state.schoolError)?'red' : '#1f6fe7'}}>School / College</Label>
              <Input style={ styles.input } value={this.state.schoolName} onChangeText={(schoolName)=> this.setState({schoolName})}/>
            </View>
            </View>



            <View ><Label style={{ color:'#1f6fe7'}}>Exam Type</Label></View>
            <View  style={styles.labunder}>
            <Item picker>
              <Picker
                
                mode="dropdown"
                iosIcon={<Icon name="arrow-down" />}
                style={{ height: 40 , color : (this.state.examTypeError)?'red' : '#333'}}
                placeholder="Select Exam Type"
                placeholderStyle={{ color: "#64a8e3" }}
                placeholderIconColor="#0275d8"
                selectedValue={this.state.ExamType}
                onValueChange={this.onValueChange222.bind(this)}
              >

                {this.state.ExamTypeList.map((item,index)=> {
                    return (<Picker.Item key={index} label={item.name} value={index} />)
                })}

                </Picker>
                </Item>
            </View>
            
            <View  style={styles.labunder}>
            <Text style={{color : (this.state.resultError)?'red' : '#1f6fe7', paddingTop: 10}}>Result</Text>
            <Textarea  rowSpan={4} cbordered value={this.state.results} onChangeText={(results)=> this.setState({results})} />
            </View>

            <Button disabled={this.state.submitDisabled} onPress={()=> { this.schoolAdd(); }} block
            style={{marginTop: 10 , marginBottom : 10}}>
            <Text>Save Changes</Text>
          </Button>
          </Form>
          
          </View>
      
            </Content>
           
      </Container>
      

        
      );
    }
}
const styles = StyleSheet.create({
  labunder:{flex: 1,  flexDirection: 'row',
  borderBottomWidth: 1,
  borderBottomColor: '#017bc2', marginBottom:5,},
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover', // or 'stretch'
  },
  loginForm: {
    
    position: 'absolute',
    backgroundColor: 'transparent', width: '100%', height: '100%',
  },
  container: {

    flex: 1,
    backgroundColor: 'transparent',
    alignItems: 'center', justifyContent: 'center',


  },


  loginContainer: {
    alignItems: 'center',
    flexGrow: 1,
    justifyContent: 'center'
  },
  form1:{ flex: 1, paddingLeft:0 },
  btnchose: { backgroundColor: '#1f6fe7', fontSize:14,textAlign: 'center', borderRadius: 5,height: 24,},
  input:{
    color: '#333',
    height: 40
  },
  labunder:{
    borderBottomWidth: 1,
    borderBottomColor: '#017bc2', marginBottom:5,
  },
    cusin:{ paddingTop:10,},

})
export default Addschool;
