import React from 'react';
import { Text, View } from 'react-native';
import { NavigationActions, StackActions } from 'react-navigation';
import { TouchableOpacity, Animated, ScrollView, Image, Dimensions, StyleSheet, YellowBox, Alert, ImageBackground, KeyboardAvoidingView, BackHandler, ActivityIndicator } from "react-native";
import { Container, Content, Form, Item, Input, Label, Button, Thumbnail, ListItem, CheckBox, Body, Toast, Icon } from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';
import EStyleSheet from 'react-native-extended-stylesheet';
const { baseUrl } = require('./baseurl/Url');

const entireScreenWidth = Dimensions.get('window').width;
EStyleSheet.build({ $rem: entireScreenWidth / 380 });
import { createBottomTabNavigator, createAppContainer, createMaterialTopTabNavigator } from 'react-navigation';

const { width } = Dimensions.get("window");
//const {baseUrl} = require('./baseurl/Url');


class SIGNIN extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      active: 0,
      xTabOne: 0,
      xTabTwo: 0,
      translateX: new Animated.Value(0),
      translateXTabOne: new Animated.Value(0),
      translateXTabTwo: new Animated.Value(width),
      translateY: -1000,
      rememberLoginCheck: false,
      mailListSignUpCheck: false,
      iAgreeSignUpCheck: false,
      iagreeCheck: false,
      Password: '',
      Email: '',
      emailerr: false,
      passerr: false,
      emailErrSignup: false,
      emailSignUp: '',
      passErrSignup: false,
      passwordSignUp: '',
      conPassErrSignup: false,
      conPasswordSignUp: '',
      behavior: 'position',
      submitDisabled: false,
      loading: false
    };
    this.login = this.login.bind(this);
    this._storeData = this._storeData.bind(this);
  }

  _storeData = async (id, email) => {
    try {
      await AsyncStorage.setItem('UserId', id);
      await AsyncStorage.setItem('Emailid', email);
      /*
      if(this.state.rememberLoginCheck){
        await AsyncStorage.setItem('password', this.state.Password.trim());
      }else{
        await AsyncStorage.setItem('password', '');
      }
      */
      this.props.screenProps.loginToProfileScreen();
    } catch (error) {
      console.error(error);
    }
  };

  login = () => {
    console.log('check' + this.state.rememberLoginCheck);
    var email = this.state.Email.trim();
    console.log("email : 111," + email + ",adsfasdf");
    var password = this.state.Password.trim();

    if (email == '') {
      this.setState({
        emailerr: true,
      });
      Toast.show({
        text: 'Email address is required',
        type: "danger"
      });
      return;
    }
    console.log('email ', email)
    this.setState({ emailerr: false });
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    if (!pattern.test(email)) {
      this.setState({
        emailerr: true,
      });
      Toast.show({
        text: 'Invalid Email Address',
        type: "danger"
      });
      console.log('Email Address Pattern is Invalid : ', email);
      return;
    }
    this.setState({ emailerr: false });
    if (password == '') {
      this.setState({
        passerr: true,
      });
      Toast.show({
        text: 'Invalid Password',
        type: "danger"
      });
      return;
    }
    this.setState({ passerr: false });

    /*
    if (password.length < 6 || password.length > 20) {
      this.setState({
        passerr: true,
      });
      Toast.show({
        text: 'Enter Password in 6-20 character',
        type: "danger"
      });
      return;
    }*/

    this.setState({ passerr: false, submitDisabled: true, loading: true });



    fetch(baseUrl + "/mobile/auth/login", {
      method: 'POST',
      headers: new Headers({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      }),
      body: JSON.stringify({
        identity: email,
        password: password,
      })
    })
      .then(response => {
        response.json().then(respond => {
          console.log(respond);
          if (respond.status == "1") {
            this._storeData(respond.user_id, email);
            this.setState({ Email: '', Password: '', rememberLoginCheck: false })
          }
          else
            Alert.alert('Gradhire', respond.message);
          this.setState({ submitDisabled: false, loading: false });
        })
      })
      .catch(error => {
        this.setState({ submitDisabled: false });
        console.error(error);
      });
  }

  gotoForget = () => {
    this.props.screenProps.loginToForgetScreen();
  }

  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>



        <Container style={styles.container}>

          <Content style={styles.loginForm}>

            <Text style={styles.sinin} >Sign In</Text>
            <Text style={{ textAlign: "center", paddingBottom: 10, paddingTop: 10 }}>To Continue, Please Enter Details</Text>


            <View style={styles.con}>
              <View style={styles.SectionStyle}>
                <Item floatingLabel>
                  <Label style={{ color: (this.state.emailerr) ? 'red' : '#0275d8', paddingLeft: 25, }}>Email Address</Label>
                  <Input style={styles.input} value={this.state.Email} onChangeText={(text) => this.setState({ Email: text })} />
                </Item>
                <Image style={styles.ImageStyle} source={require('../assest/mail.png')} />
              </View>


            </View>
            <View style={styles.con}>
              <View style={styles.SectionStyle}>
                <Item floatingLabel>
                  <Label style={{ color: (this.state.passerr) ? 'red' : '#0275d8', paddingLeft: 25, }}>Password</Label>
                  <Input secureTextEntry={true} style={styles.input} value={this.state.Password} onChangeText={(Password) => this.setState({
                    Password
                  })} />
                </Item>
                <Image style={styles.ImageStyle} source={require('../assest/password_icon.png')} />
              </View>


            </View>



            <Item floatingLabel style={styles.input}>

              <Thumbnail resizeMode="contain" small square style={styles.mailicon} source={require('../assest/password_icon.png')} />


            </Item>


            { /*          
                        <View style={{ flexDirection : 'row', flex : 1, paddingTop:10, marginTop : 10}}>
                          <CheckBox checked={this.state.rememberLoginCheck} onPress={()=> { console.log('Checked box'); this.setState(preState=>({rememberLoginCheck : !preState.rememberLoginCheck}))}} />
                          <Text style={{marginLeft : 15}} >Remember Me</Text>
                        </View>
                        */ }

            <View style={{ marginTop: 20 }}>
              {(!this.state.loading) ?
                <Button block disabled={this.state.submitDisabled}
                  style={{ borderRadius: 5, color: '#fff', backgroundColor: '#1f6fe7', }}
                  onPress={() => { this.login(); }}
                >

                  <Text style={{ color: '#fff', textAlign: "center", paddingTop: 10, paddingBottom: 10, fontSize: 16, textTransform: 'uppercase' }}>Sign In</Text>
                </Button>
                : <ActivityIndicator />
              }
            </View>

            <TouchableOpacity onPress={() => { this.gotoForget(); }}>
              <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ marginTop: 20, fontSize: 16, color: '#333' }}>Forgot Password?</Text>
              </View>
            </TouchableOpacity>




          </Content>
        </Container>


      </View>

    );
  }
}

class SIGNUP extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      active: 0,
      xTabOne: 0,
      xTabTwo: 0,
      translateX: new Animated.Value(0),
      translateXTabOne: new Animated.Value(0),
      translateXTabTwo: new Animated.Value(width),
      translateY: -1000,
      rememberLoginCheck: false,
      mailListSignUpCheck: false,
      iAgreeSignUpCheck: false,
      iagreeCheck: false,

      Password: '',
      Email: '',
      emailerr: false,
      passerr: false,
      emailErrSignup: false,
      emailSignUp: '',
      passErrSignup: false,
      passwordSignUp: '',
      conPassErrSignup: false,
      conPasswordSignUp: '',
      behavior: 'position',
      submitDisabled: false
    };
    this.signUp = this.signUp.bind(this);
  }

  //  // SignUp Fetch Api

  signUp = () => {

    var emailSignUp = this.state.emailSignUp.trim();
    var passwordSignUp = this.state.passwordSignUp.trim();
    var conPasswordSignUp = this.state.conPasswordSignUp.trim();



    if (emailSignUp == '') {
      this.setState({
        emailErrSignup: true,
      });
      Toast.show({
        text: 'Email address is required',
        type: "danger"
      });
      return;
    }
    this.setState({ emailErrSignup: false });
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    if (!pattern.test(emailSignUp)) {
      this.setState({
        emailErrSignup: true,
      });
      Toast.show({
        text: 'Invalid Email Address',
        type: "danger"
      });
      return;
    }

    this.setState({ emailErrSignup: false });
    if (passwordSignUp == '') {
      this.setState({
        passErrSignup: true,
      });
      Toast.show({
        text: 'Invalid Password',
        type: "danger"
      });
      return;
    }
    this.setState({ passErrSignup: false });

    /*
    if (passwordSignUp.length < 6 || passwordSignUp.length > 20) {
      this.setState({
        passErrSignup: true,
      });
      Toast.show({
        text: 'Enter Password in 6-20 character',
        type: "danger"
      });
      return;
    }
    this.setState({ passErrSignup: false });
    */

    if (conPasswordSignUp == '') {
      this.setState({
        conPassErrSignup: true,
      });
      Toast.show({
        text: 'Invalid Confirm Password',
        type: "danger"
      });
      return;
    }
    this.setState({ conPassErrSignup: false });
    if (conPasswordSignUp != passwordSignUp) {
      this.setState({
        passErrSignup: true,
        conPassErrSignup: true
      });
      Toast.show({
        text: 'Password and Confirm Password not Match',
        type: "danger"
      });
      return;
    }
    this.setState({
      passErrSignup: false,
      conPassErrSignup: false
    });

    if (!this.state.iAgreeSignUpCheck) {
      this.setState({ iagreeCheck: true });
      Toast.show({
        text: 'Please Check Terms of Use',
        type: "danger"
      });
      return;
    }
    this.setState({ iagreeCheck: false });
    var mailing_stat = (this.state.mailListSignUpCheck) ? "1" : "0";
    this.setState({ submitDisabled: true, loading: true });
    fetch(baseUrl + '/mobile/auth/create_user', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: JSON.stringify({
        register_as: '3',
        email: emailSignUp,
        password: passwordSignUp,
        confirm_pass: conPasswordSignUp,
        mailing_status: mailing_stat
      }),
    })
      .then((response) => {
        console.log('res signup : ', response);
        response.json().then(respond => {
          console.log(respond);
          if (respond.status == "1") {
            this.setState({ userNameSignUp: '', emailSignUp: '', passwordSignUp: '', conPasswordSignUp: '', iAgreeSignUpCheck: false, mailListSignUpCheck: false })
          }
          Alert.alert('Gradhire', respond.message);
          this.setState({ submitDisabled: false, loading: false });
        })
      })
      .catch(error => {
        this.setState({ submitDisabled: false });
        console.error(error);
      });

  }

  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>




        <Container style={styles.container}>


          <Content style={styles.loginForm}>

            <Text style={styles.sinin}>Sign Up</Text>
            <Text style={{ textAlign: "center", paddingTop: 10 }}>To Continue, Please Enter Details</Text>


            <View style={styles.con}>
              <View style={styles.SectionStyle}>
                <Item floatingLabel>
                  <Label style={{ color: (this.state.emailErrSignup) ? 'red' : '#0275d8', paddingLeft: 25, }}>Email Address</Label>
                  <Input style={styles.input} value={this.state.emailSignUp} onChangeText={(email) => this.setState({ emailSignUp: email })} />
                </Item>
                <Image style={styles.ImageStyle} source={require('../assest/mail.png')} />
              </View>


            </View>

            <View style={styles.con}>
              <View style={styles.SectionStyle}>
                <Item floatingLabel>
                  <Label style={{ color: (this.state.passErrSignup) ? 'red' : '#0275d8', paddingLeft: 25, }}>Password</Label>
                  <Input secureTextEntry={true} style={styles.input} value={this.state.passwordSignUp} onChangeText={(Password) => this.setState({ passwordSignUp: Password })} />
                </Item>
                <Image style={styles.ImageStyle} source={require('../assest/password_icon.png')} />
              </View>


            </View>


            <View style={styles.con}>
              <View style={styles.SectionStyle}>
                <Item floatingLabel>
                  <Label style={{ color: (this.state.conPassErrSignup) ? 'red' : '#0275d8', paddingLeft: 25, }}>Confirm Password</Label>
                  <Input secureTextEntry={true} style={styles.input} value={this.state.conPasswordSignUp} onChangeText={(ConnPassword) => this.setState({ conPasswordSignUp: ConnPassword })} />
                </Item>
                <Image style={styles.ImageStyle} source={require('../assest/password_icon.png')} />
              </View>


            </View>


            <View style={{ flexDirection: 'column', flex: 1, marginTop: 15 }} >
              <View style={{ flexDirection: 'row', flex: 1, margin: 5, marginLeft: 0 }}>
                <CheckBox checked={this.state.iAgreeSignUpCheck} onPress={() => { console.log('Checked box'); this.setState(preState => ({ iAgreeSignUpCheck: !preState.iAgreeSignUpCheck })) }} />
                <Text style={{ marginLeft: 15, color: (this.state.iagreeCheck) ? "red" : "#333" }} >I agree with the Terms of Use</Text>
              </View>

              <View style={{ flexDirection: 'row', flex: 1, margin: 5, marginLeft: 0 }}>
                <CheckBox checked={this.state.mailListSignUpCheck} onPress={() => { console.log('Checked box'); this.setState(preState => ({ mailListSignUpCheck: !preState.mailListSignUpCheck })) }} />
                <Text style={{ marginLeft: 15, color: "#333" }} >Add me to your mailing list</Text>
              </View>
            </View>

            <View style={{ marginTop: 20 }}>
              {(!this.state.loading) ?
                <Button block disabled={this.state.submitDisabled}
                  style={{ borderRadius: 5, backgroundColor: '#1f6fe7', }}
                  onPress={() => { this.signUp(); }}
                >
                  <Text style={{ color: '#fff', textAlign: "center", paddingTop: 5, paddingBottom: 5, textTransform: 'uppercase' }}>SUBMIT</Text>
                </Button>
                : <ActivityIndicator />
              }
            </View>

          </Content>
        </Container>
      </View>
    );
  }
}



export default class RootScreen extends React.Component {
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);


  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  handleBackButton = () => {
    Alert.alert(
      'Gradhire',
      'Exiting the application?', [{
        text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel'
      }, {
        text: 'OK',
        onPress: () => BackHandler.exitApp()
      },], {
        cancelable: false
      }
    )
    return true;
  }


  loginToProfileScreen = () => {
    console.log('loginToProfileScreen : call ');


    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: 'Candidate_profile1' })],
    });
    this.props.navigation.dispatch(resetAction);

    // const {navigate} = this.props.navigation;
    //              navigate('Candidate_profile1');
    //   navigate('ComingSoon');
  }
  loginToForgetScreen = () => {
    console.log('loginToForgetScreen : call ');
    const { navigate } = this.props.navigation;
    navigate('Forget');
  }

  render() {
    YellowBox.ignoreWarnings(['Warning: Async Storage has been extracted from react-native core', 'ViewPagerAndroid']);
    const TabNavigator = createMaterialTopTabNavigator({
      "SIGN IN": { screen: ({ navigation }) => <SIGNIN screenProps={{ loginToProfileScreen: this.loginToProfileScreen, loginToForgetScreen: this.loginToForgetScreen }} /> },
      "SIGN UP": SIGNUP,
    }, {
        swipeEnabled: true,
        animationEnabled: true, fontSize: 24,
        tabBarOptions: {
          activeTintColor: '#1f6fe7',
          inactiveTintColor: '#333',
          style: {
            backgroundColor: 'transparent',
          },
          labelStyle: {
            textAlign: 'center', fontSize: 20,
          },
          indicatorStyle: {
            borderBottomColor: '#1f6fe7',
            borderBottomWidth: 2,
          },
        },
      }
    );
    const RootNav = createAppContainer(TabNavigator);
    return (


      <ImageBackground source={require('../assest/login_bg.png')}
        style={{ resizeMode: 'stretch' }}>


        <View style={{
          marginBottom: 20,
          paddingBottom: 50,
          height: '100%',
          width: "100%", textAlign: 'center'
        }}>
          <Image resizeMode="contain" style={styles.logo} source={require('../assest/logo.png')} />

          <RootNav />

        </View>
      </ImageBackground>

    );
  }
}

const styles = EStyleSheet.create({
  log2: {
    height: '100%', alignItems: 'center',
  },
  loginContainer: {
    alignItems: 'center',
    flexGrow: 1,
    justifyContent: 'center', backgroundColor: '#333'
  },
  logo: {
    alignSelf: 'center',
    width: '300rem',
    height: '150rem',


  },
  loginForm: { width: '94%', backgroundColor: 'transparent', flex: 1, },
  container: { width: '100%', backgroundColor: 'transparent', alignItems: 'center', flex: 1, flexGrow: 1, justifyContent: 'center', },
  input: {
    fontSize: '14rem',
    color: '#333', paddingLeft: 15, marginLeft: 10,


  },
  texth1: { fontWeight: 'bold' },

  texth2: { color: '#1f6fe7' },
  carditn: { paddingTop: '5rem', paddingBottom: '5rem', paddingLeft: '10rem', color: "#0275d8", borderRadius: 10, width: '100%' },
  mailicon: {
    width: '22rem',
    height: '20rem', justifyContent: 'flex-end',
    marginBottom: 36, flex: 1,
  },
  sinin: { color: '#0275d8', fontSize: '20rem', textAlign: "center", paddingTop: '10rem', textTransform: 'uppercase', paddingTop: '30rem', },



  con: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10, marginLeft: 10, marginRight: 10,
  },

  SectionStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',

    borderBottomWidth: .5,
    borderBottomColor: '#1f6fe7', paddingBottom: 10


  },

  ImageStyle: {
    padding: 5,
    margin: 5,
    height: '25rem',
    width: '25rem',
    resizeMode: 'contain',
    alignItems: 'center'
  },

});

