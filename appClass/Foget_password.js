import React, { Component } from 'react';
import { Image,TouchableOpacity,StyleSheet,BackHandler,Alert,Linking,Dimensions } from 'react-native';
import { Container,Toast,View, Header, Content, Form, Item, Input, Label,Button,Text, Card, CardItem, Thumbnail, Icon, Left, Body, Right} from 'native-base';
import EStyleSheet from 'react-native-extended-stylesheet';
const {baseUrl} = require('./baseurl/Url');
const entireScreenWidth = Dimensions.get('window').width;
EStyleSheet.build({$rem: entireScreenWidth / 380});

class Forget_password extends React.Component {
  static navigationOptions = {
    header : null
    };

    constructor(props){
      super(props);
      this.state = {
          email : '',
          error : false,
          errormsg : ''
      }
      this.changePassword = this.changePassword.bind(this);
    }

   


    componentDidMount() {
      BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
     }
     
     componentWillUnmount() {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
     }
     
     handleBackButton = () => {
          this.props.navigation.goBack();
       return true;
     } 




    changePassword = ()=> {
      var email = this.state.email.trim();
      if (email == '') {
        this.setState({
          error : true,
          errormsg : "*Invalid Email Address."
          });
        return;
        }
        var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
        if (!pattern.test(email)) {
          this.setState({
                error : true,
                errormsg : "*Please enter Email Address."
          });
          return;
        }  

        this.setState({ error : false,
          errormsg : "" });

         console.log('change password ',email);
        fetch(baseUrl+'/mobile/auth/forgot_password', {
              method: 'POST',
              headers: {
                      Accept: 'application/json',
                      'Content-Type': 'application/json',
              },
            body: JSON.stringify({
                  identity: email
              }),
          })
         .then((response) => {
             response.json().then(respond => {
               console.log(respond);
               Alert.alert(
                'Gradhire',
                respond.message,
                [
                  {text: 'OK', onPress: () => { 
                  //  if(respond.status == 1){ this.props.navigation.navigate('Resetpassword'); }
                   }}
                ],
                { cancelable: false }
              )
             })
        
           }).catch(error => {
            console.error(error);
          });

    }
   
    render() {
      console.log('props of Forget password  id : ',this.props)
      return (

        <Container style={styles.container}>
          <Image source={require('../assest/login_bg.png')} style={styles.backgroundImage} />
          <Content style={ styles.loginForm }>
          <Form>
          <Image resizeMode="contain" style={styles.logo} source={require('../assest/logo.png')} />
          
          <Text style={styles.textForgetPassword}>Forget Password</Text>
          <Text style={styles.continueText}>To Continue, Please Enter Details</Text>
      
      
          <Item floatingLabel style={styles.input}>
          <Label style={{color : (this.state.error)? 'red':'#0275d8' }}>Email Address</Label>
          <Thumbnail resizeMode="contain" small square style={styles.mailicon} source={require('../assest/mail.png')} />
          <Input value={this.state.email} onChangeText={(text)=> {this.setState({email : text})}} />
          </Item>
            <Text style={styles.errorMsg}>{this.state.errormsg}</Text> 
            <Button
              block
              style={{marginTop: 20,marginLeft:20,marginRight:20,borderRadius:5}} 
              onPress={()=> this.changePassword()}
            >
            <Text>Submit</Text>
            </Button>
            <Button
              block
              style={{marginTop: 20,marginLeft:20,marginRight:20,borderRadius:5}} 
              onPress={()=> this.props.navigation.navigate('RootScreen')}
            >
            <Text>Back </Text>
            </Button>
            </Form>
            </Content>
          </Container>
      );
    }
}




const styles = EStyleSheet.create({

  backgroundImage: {
    flex: 1,
    resizeMode: 'cover', // or 'stretch'
},
loginForm: {
  flex: 1,
    position: 'absolute',
    backgroundColor: 'transparent', paddingTop:40, width:'90%', height:null, 
},
  container: {
      flex: 1,
      backgroundColor: 'transparent',
      alignItems: 'center',justifyContent: 'center', 
  },
  mailicon: {
    height: '10rem',
    width: "20rem",
},textForgetPassword : {
   fontSize : '20rem',
  color: '#0275d8',
   textAlign: "center",
    paddingTop: 20,
    textTransform: 'uppercase'
},
sinin:{color: '#0275d8',fontSize:'20rem', textAlign: "center", paddingTop: '10rem',textTransform: 'uppercase', paddingTop:'30rem',},
continueText : {
  fontSize : '14rem',
  textAlign: "center",
   paddingTop: 10,
   color: '#5c5c5c',
}, 
  loginContainer:{
      alignItems: 'center',
      flexGrow: 1,
      justifyContent: 'center'
  },errorMsg : {
    color : 'red',
    fontSize: "10rem" ,
     marginLeft : 12
  },
  logo: {
    flex: 1,
    alignItems: 'center',
      width: '250rem',
      height: '80rem',
     justifyContent: 'center',
     marginLeft:50,
     marginBottom : '8rem'
      
  },
  input:{
    fontSize: '16rem',
    
    color: 'black',
    marginBottom:10, paddingTop:20,
    borderBottomColor: '#1f6fe7',
    borderBottomWidth: 1
    
  }

})



export default Forget_password;