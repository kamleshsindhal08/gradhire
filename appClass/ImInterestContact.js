import React, { Component } from 'react';
import { Image, TouchableOpacity, StyleSheet, BackHandler, ActivityIndicator, Dimensions, Alert } from 'react-native';
import { Container, Header, Content, View, Form, Item, Input, Label, Button, Text, Icon, Card, Spinner, CardItem, Thumbnail, Left, Body, Right, Toast } from 'native-base';

import EStyleSheet from 'react-native-extended-stylesheet';
const { baseUrl } = require('./baseurl/Url');

const entireScreenWidth = Dimensions.get('window').width;
EStyleSheet.build({ $rem: entireScreenWidth / 380 });


class ImInterestContact extends React.Component {

  constructor(props) {
    super(props);

    this.state = {

      Email: '',
      MobileNumber: '',
      PhoneNumber: '',
      emailerr: false,
      MobileNumbererr: false,
      PhoneNumbererr: false,
      isRefresh: true
    };
  }


  static navigationOptions = {
    title: 'Contact',
  };


  componentDidMount() {
    const { id } = this.props.navigation.state.params;
    fetch(baseUrl + '/mobile/api/single_personal_contact_detail', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        user_id: id
      }),
    })
      .then((response) => {
        response.json().then(respond => {
          console.log(respond);
          if (respond.status == "1") {

            this.setState({
              Email: respond.data.email,
              MobileNumber: respond.data.mobile_number,
              PhoneNumber: respond.data.phone_number, isRefresh: false
            });

          } else {
            this.setState({ isRefresh: false });
          }

        })
      }).catch(error => {
        console.error(error);
      });
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }



  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  handleBackButton = () => {
    this.props.navigation.goBack();
    return true;
  }


  addContact = () => {
    var job_id  = this.props.navigation.state.params.job_id ;
    var company_user_id = this.props.navigation.state.params.company_user_id ;
    var candidate_id = this.props.navigation.state.params.candidate_id ;

    var email = this.state.Email.trim();
    var mobile_number = this.state.MobileNumber.trim();
    var phone_number = this.state.PhoneNumber.trim();


  

    
    if (email == "") {
      console.log('Title : ');
      this.setState({ emailerr: true });
      return;
    }

    this.setState({ emailerr: false });

    if (email != "") {
      var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
      if (!pattern.test(email)) {
        this.setState({
          emailerr: true,
        });
        console.log('email pattern invalid : ', email);
        return;
      }
    }
    this.setState({ emailerr: false });


    if (mobile_number == "" && phone_number == "" ) {
      Alert.alert(
        'Gradhire',
        'Please enter at least mobile number or phone number to continue', [{
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel'
        }, {
          text: 'OK',
        },], {
          cancelable: false
        }
      )
      return;
    }

     if (mobile_number != "" && mobile_number.length < 10) {
        console.log('Title : ');
        this.setState({ MobileNumbererr: true });
        return;
      }
      this.setState({ MobileNumbererr: false });
      if (phone_number != "" && phone_number.length < 10) {
        console.log('Title : ');
        this.setState({ PhoneNumbererr: true });
        return;
      }
      this.setState({ PhoneNumbererr: false });

    const { id } = this.props.navigation.state.params;

    fetch(baseUrl + '/mobile/api/interested_invite_dob_detail', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        // 
        job_id : job_id,
        company_user_id : company_user_id ,
        candidate_id : candidate_id,
        email : email,
        mobile : mobile_number,
        phone : phone_number
      }),
    })
      .then((response) => {
        response.json().then(respond => {
          console.log('update contact details ', respond);
          if (respond.status == "1") {
            this.props.navigation.state.params.getJobAlertData();
            this.props.navigation.goBack();
            Toast.show({
              text: respond.message,
              type: "success"
            });
          } else {
            Toast.show({
              text: respond.message,
              type: "danger"
            });
          }
        })
      }).catch(error => {
        console.error(error);
      });

      


  }




  render() {
    if (this.state.isRefresh) {
      return <View style={{ flex: 1, justifyContent: 'center' }}><ActivityIndicator size="large" color="#0000ff" /></View>;
    } else {
      return (

        <Container style={styles.container}>
          <Image source={require('../assest/login_bg.png')} style={styles.backgroundImage} />
          <Content style={styles.loginForm}>
            <Form>
              {/* <Image resizeMode="contain" style={styles.logo} source={require('../assest/logo.png')} /> */}


              {/* 
          <Text style={{color: '#0275d8', textAlign: "center", paddingTop: 10,textTransform: 'uppercase'}}>Contact Details</Text>
          <Text style={{textAlign: "center", paddingTop: 10}}>Please Enter Details</Text>
          */}



                <Text style={{textAlign: "center", paddingTop: 10,marginTop : 15,marginLeft : 25,marginRight : 25 ,marginBottom : 25}}>Thank you for confirming your interest in this role. We will make the following contact details available to the company so that they can make contact with you. Please confirm these details</Text>




              <View style={styles.con}>
                <View style={styles.SectionStyle}>
                  <Item floatingLabel>
                    <Label style={{ color: (this.state.emailerr) ? 'red' : '#0275d8', paddingLeft: 25 }}>Email</Label>

                    {/* <Thumbnail resizeMode="contain" small square style={styles.mailicon} source={require('../assest/mail.png')} /> */}
                    <Input style={styles.input} underlineColorAndroid="transparent" value={this.state.Email} onChangeText={(text) => this.setState({ Email: text })} />
                  </Item>
                  <Image style={styles.ImageStyle} source={require('../assest/mail.png')} />
                </View>
              </View>


              {/* <Item floatingLabel style={styles.input}>
            <Label style={{color : (this.state.emailerr)? 'red':'#0275d8' }}>Email</Label>
              
              <Thumbnail resizeMode="contain" small square style={styles.mailicon} source={require('../assest/mail.png')} />
              <Input  style={styles.input} value={this.state.Email} onChangeText={(text)=> this.setState({Email : text })}/>
            </Item> */}





              {/* <Text style={{ fontSize: 12,paddingLeft: 14,color : 'red'}}> {(this.state.emailerr)? "Invalid Username": ''}</Text> */}



              <View style={styles.con}>
                <View style={styles.SectionStyle}>
                  <Item floatingLabel>
                    <Label style={{ color: (this.state.MobileNumbererr) ? 'red' : '#0275d8', paddingLeft: 25 }}>Mobile Number</Label>

                    {/* <Thumbnail resizeMode="contain" small square style={styles.mailicon} source={require('../assest/mail.png')} /> */}
                    <Input keyboardType="numeric" maxLength={15} style={styles.input} value={this.state.MobileNumber} onChangeText={(text) => this.setState({ MobileNumber: text })} />
                  </Item>
                  <Image style={styles.ImageStyle} source={require('../assest/mobile_no.png')} />
                </View>
              </View>

              {/* <Item floatingLabel style={styles.input}>
                <Label style={{ color: (this.state.MobileNumbererr) ? 'red' : '#0275d8' }}>Mobile Number</Label>
                <Thumbnail resizeMode="contain" small square style={styles.mailicon} source={require('../assest/mobile_no.png')} />

                <Input style={styles.input} value={this.state.MobileNumber} onChangeText={(text) => this.setState({ MobileNumber: text })} />
              </Item> */}
              {/* <Text style={{ fontSize: 12,paddingLeft: 14,color : 'red'}}> {(this.state.MobileNumbererr)? "Invalid Password": ''}</Text> */}



              <View style={styles.con}>
                <View style={styles.SectionStyle}>
                  <Item floatingLabel>
                    <Label style={{ color: (this.state.PhoneNumbererr) ? 'red' : '#0275d8', paddingLeft: 25 }}>Phone Number</Label>
                    <Input keyboardType="numeric" maxLength={15} style={styles.input} value={this.state.PhoneNumber} onChangeText={(text) => this.setState({ PhoneNumber: text })} />
                     </Item>
                  <Image style={styles.ImageStyle} source={require('../assest/telephone_icon.png')} />
                </View>
              </View>



              {/* <Item floatingLabel style={styles.input}>
                <Label style={{ color: (this.state.PhoneNumbererr) ? 'red' : '#0275d8' }}>Phone Number</Label>
                <Thumbnail resizeMode="contain" small square style={styles.mailicon} source={require('../assest/telephone_icon.png')} />
                <Input style={styles.input} value={this.state.PhoneNumber} onChangeText={(text) => this.setState({ PhoneNumber: text })} />
              </Item> */}
              {/* <Text style={{ fontSize: 12,paddingLeft: 14,color : 'red'}}> {(this.state.PhoneNumbererr)? "Invalid Username": ''}</Text>
             */}

              <Button block
                style={{ marginTop: 10, marginLeft: 20, marginRight: 20, marginBottom: 40  , borderRadius: 5 }}
                onPress={() => {
                  console.log('hi');
                  // let ty =
                  this.addContact();

                }}
              >
                <Text>Submit</Text>
              </Button>
            </Form>



          </Content>

        </Container>
      );
    }
  }
}

const styles = EStyleSheet.create({
  con: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 15,
    marginLeft: 15,
    marginRight: 15,
    marginBottom : 15
  },

  SectionStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomWidth: .5,
    borderBottomColor: '#1f6fe7',
    paddingBottom: 8
  },

  ImageStyle: {
    padding: 5,
    marginTop: 5,
    marginLeft: 5,
    marginRight: 5,
    height: '25rem',
    width: '25rem',
    resizeMode: 'contain',
    alignItems: 'center'
  },
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover', // or 'stretch'
  },
  loginForm: {
    paddingTop: 25,
      
    position: 'absolute',
    backgroundColor: 'transparent', width: '94%', height: '100%',
  },
  container: {

    flex: 1,
    backgroundColor: 'transparent',
    alignItems: 'center', justifyContent: 'center',



  },
  mailicon: {
    height: 10,
    width: 20,

  },

  loginContainer: {
    alignItems: 'center',
    flexGrow: 1,
    justifyContent: 'center'
  },
  logo: {
    flex: 1,
    alignItems: 'center',
    width: 300,
    height: 100,
    justifyContent: 'center', marginLeft: 50,

  },
  input: {
    fontSize: '14rem',
    color: '#333',
    paddingLeft: 15,
    marginLeft: 15,
    
  }

})

export default ImInterestContact;