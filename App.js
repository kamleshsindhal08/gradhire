import React, { Component } from 'react';
import { Platform, StyleSheet, style, Text, View, SafeAreaView, ScrollView, Dimensions, Image } from 'react-native';

import { Root } from 'native-base';
import { createStackNavigator, createAppContainer,createBottomTabNavigator, createDrawerNavigator, DrawerItems } from "react-navigation";
import Forget_password from './appClass/Foget_password';
import Resetpassword from './appClass/Resetpassword';
import AddUniversity from './appClass/AddUniversity';
import EditUniversity from './appClass/EditUniversity';
import SplashScreen from './appClass/SplashScreen';
import Candidate_profile1 from './appClass/Candidate_profile1';
import AddSchool from './appClass/Addschool';
import AddWorkExp from './appClass/AddWorkExp';
import EditWorkExp from './appClass/EditWorkExp';
import AddContact from './appClass/AddContact';
import EditSchool from './appClass/EditSchool';
import EditProfile from './appClass/Editprofile';
import Changepassword from './appClass/Changepassword';
import RootScreen from './appClass/LoginSignupNew';
import ComingSoon from './appClass/ComingSoon';
import OneButton from './appClass/OneButton';
import VideoProfile from './appClass/VideoProfile';
import VideoProfileCreate from './appClass/VideoProfileCreate';
import OpenCamera from './appClass/OpenCamera';
import JobAlert from './appClass/JobAlert';
import ImInterestContact from './appClass/ImInterestContact';
import Companyjobdetails from './appClass/Companyjobdetails';
import Map1 from './appClass/Map1';
import VideoPlayer from './appClass/VideoPlayer';
import VideoTips from './appClass/VideoTips';



const { width } = Dimensions.get('window')

const Tab = createBottomTabNavigator({
  Profile :  createStackNavigator({
    Candidate_profile1 : { screen : Candidate_profile1,
    navigationOptions: {        
      headerLeft: null,        
      headerTitle: "Profile"      
    } 
  },
  },
  {
    navigationOptions: ({ navigation }) => ({

      tabBarIcon: ({ focused, horizontal, tintColor }) => {
        return (<Image  source={require('./assest/user.png')} style={{width : 25 ,height: 25 ,tintColor : tintColor}} />);
      },
    }),
  }
  ),
  'Job Alert' :  createStackNavigator({ JobAlert : {screen : JobAlert,
              navigationOptions : {
                headerLeft: null,        
                headerTitle: "Job Alert",
                headerStyle: { backgroundColor: '#2e2b48' },
                headerTintColor: 'white', 
              }  
            },
     },
     {
       navigationOptions: ({ navigation }) => ({
         tabBarIcon: ({ focused, horizontal, tintColor }) => {
           return (<Image source={require('./assest/job_alert.png')} style={{width : 25 ,height: 25,tintColor  : tintColor}} />);
         },
       })
     })

    });



const AppNavigator = createStackNavigator({

          SplashScreen: { screen: SplashScreen },
          RootScreen: { screen: RootScreen },
          Forget: { screen: Forget_password },
           
           Candidate_profile1: { screen: Tab,
            navigationOptions: {
              header: null,
              headerLeft: null
            }
          },
          AddUniversity: { 
            screen: AddUniversity,
            navigationOptions : {
              headerStyle: { backgroundColor: '#2e2b48' },
              headerTintColor: 'white', 
            } 
           },
            EditUniversity: { 
              screen: EditUniversity,
              navigationOptions : {
                headerStyle: { backgroundColor: '#2e2b48' },
                headerTintColor: 'white', 
              } 
             },
            AddSchool: { 
              screen: AddSchool,
              navigationOptions : {
                headerStyle: { backgroundColor: '#2e2b48' },
                headerTintColor: 'white', 
              } 
             },
            EditSchool: { 
              screen: EditSchool,
              navigationOptions : {
                headerStyle: { backgroundColor: '#2e2b48' },
                headerTintColor: 'white', 
              } 
             },
          
            AddWorkExp: { 
              screen: AddWorkExp,
              navigationOptions : {
                headerStyle: { backgroundColor: '#2e2b48' },
                headerTintColor: 'white', 
              } 
             },
            EditWorkExp: { 
              screen: EditWorkExp,
              navigationOptions : {
                headerStyle: { backgroundColor: '#2e2b48' },
                headerTintColor: 'white', 
              } 
             },
            AddContact: { 
              screen: AddContact,
              navigationOptions : {
                headerStyle: { backgroundColor: '#2e2b48' },
                headerTintColor: 'white', 
              } 
             },
                          // //  create video RNCamera 
            VideoProfile: { 
              screen: VideoProfile,
              navigationOptions : {
                headerStyle: { backgroundColor: '#2e2b48' },
                headerTintColor: 'white', 
              } 
             },
             VideoProfileCreate: { 
              screen: VideoProfileCreate,
              navigationOptions : {
                headerStyle: { backgroundColor: '#2e2b48' },
                headerTintColor: 'white', 
              } 
             },
             
              OpenCamera: { screen: OpenCamera },
          //   ComingSoon : {screen : ComingSoon},
            EditProfile: { 
              screen: EditProfile,
              navigationOptions: {
                header: null,
                //headerLeft: null
              }
             },
            Changepassword: { 
              screen: Changepassword,
              navigationOptions : {
                headerStyle: { backgroundColor: '#2e2b48' },
                headerTintColor: 'white', 
              } 
             },
          

        // JobAlert: { screen: JobAlert },
          Companyjobdetails: { screen: Companyjobdetails },
          ImInterestContact: { 
            screen: ImInterestContact,
            navigationOptions : {
              headerStyle: { backgroundColor: '#2e2b48' },
              headerTintColor: 'white', 
            } 
           },
            VideoPlayer : {
              screen : VideoPlayer,
              navigationOptions : {
                headerStyle: { backgroundColor: '#2e2b48' },
                headerTintColor: 'white', 
              } 
            },
            VideoTips : {
              screen : VideoTips,
              navigationOptions : {
                headerStyle: { backgroundColor: '#2e2b48' },
                headerTintColor: 'white', 
              } 
            },
           Map1 : {
             screen : Map1,
             navigationOptions : {
              headerStyle: { backgroundColor: '#2e2b48' },
              headerTintColor: 'white', 
            } 
            }
    

});


const AppContainer = createAppContainer(AppNavigator);
const prefix = "https://www.mailinator.com/key/url?url=http%3A//localhost/gradehire/home/resetpassword";


// const MainApp = () => <AppContainer uriPrefix={url} />;
// export default MainApp;


export default class App extends Component {





  render() {
    return (
      <Root>
        
        <AppContainer uriPrefix={prefix} />
       
      </Root>
    );
  }
}

